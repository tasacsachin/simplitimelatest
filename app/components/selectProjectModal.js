
import React, { Component } from 'react';
import {
    StyleSheet, SafeAreaView,
    Text, Modal, Picker, TouchableWithoutFeedback, FlatList,
    View, Image, TouchableOpacity, TextInput, ScrollView, Platform
} from 'react-native';
import { height, width, fontSizes } from '../utils/utils'
const data = [
    "Project X", "Project Y", "Project Z", "Project A", "Project B", "Project C", "Project D",
]
const SelectProjectModal = ({ type, pickerDropdownModalVisible, totalSelectedCount, selectedCountShow, extraData, pickerDropdownModalClose, titleText, pickerDropdownModalListData, pickerDropdownModalRow, pickerDropdownSelectClicked, onChangeOfSearchTextInput }) => {

    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={pickerDropdownModalVisible}
            onRequestClose={pickerDropdownModalClose}>

            <View style={styles.modalContainer}>

                <View style={styles.bottomContainer}>
                    <View style={{ flexDirection: 'row', justifyContent: selectedCountShow ? 'space-between' : 'center', alignItems: 'center', marginHorizontal: 20 }}>
                        <Text style={{ fontSize: fontSizes.font20, color: "#2A56C6", fontWeight: "bold", marginTop: 15 }}>
                            {titleText}

                        </Text>
                        {selectedCountShow ?
                            <Text style={{ fontSize: fontSizes.font14, color: "#2A56C6", fontWeight: "bold", marginTop: 15 }}>
                                Total Selected:
                            <Text style={{ fontSize: fontSizes.font14, color: "#000", fontWeight: "bold", marginTop: 15 }}>
                                    {' ' + totalSelectedCount}

                                </Text>
                            </Text>
                            : null}
                    </View>
                    {/* <TouchableOpacity onPress={pickerDropdownSelectClicked}
                            style={{ alignItems: "center",flexDirection:"row", justifyContent: "center", backgroundColor: "#2A56C6", borderRadius: 4,paddingHorizontal:10,paddingVertical:5 }}>
                           <Image source={require('../images/headerIcons/tickIcon.png')}/>
                            <Text style={{ fontSize: fontSizes.font18, color: "white", marginLeft:5 }}>
                                Select
    
                    </Text>
                        </TouchableOpacity> */}

                    {/* <TextInput
                        style={{ height: 50, width: width - 60, color: '#000', alignSelf: "center", paddingLeft: 10, borderBottomColor: "#E1E2E4", marginTop: 10, borderBottomWidth: 1.5 }}
                        placeholder="Search"
                        //value={value}
                        placeholderTextColor="#757575"
                        underlineColorAndroid="transparent"
                        autoCapitalize={false}
                        autoCorrect={false}
                        //maxLength={placeholder == "Company ID" ? 5 : 100}
                        //secureTextEntry={placeholder == "Password" ? true : false}
                        onChangeText={onChangeOfSearchTextInput}
                    //value={ProjectCodeValue}
                    /> */}
                    <View style={{ flexDirection: 'row', borderBottomColor: "#E1E2E4", marginTop: 5, borderBottomWidth: 1.5, marginHorizontal: 20 }}>
                        <Image source={require('../images/searchIcon.png')} style={{ alignSelf: 'center', marginRight: 5 }} />
                        <TextInput
                            style={{ height: 40, flex: 1, color: '#000', alignSelf: "center", paddingLeft: 5, }}
                            placeholder="Search"
                            //value={value}
                            placeholderTextColor="#757575"
                            underlineColorAndroid="transparent"
                            autoCapitalize={false}
                            autoCorrect={false}
                            returnKeyType='search'
                            //maxLength={placeholder == "Company ID" ? 5 : 100}
                            //secureTextEntry={placeholder == "Password" ? true : false}
                            onChangeText={onChangeOfSearchTextInput}
                        //value={ProjectCodeValue}
                        />
                    </View>


                    <FlatList
                        data={pickerDropdownModalListData}
                        renderItem={pickerDropdownModalRow
                        }
                        showsVerticalScrollIndicator={false}
                        keyExtractor={key => key.index}
                        keyboardShouldPersistTaps="always"
                        extraData={extraData}
                    />



                </View>
                {/* <View style={{ width: width - 20, marginTop:-30,height: 60, padding: 10, flexDirection: "row", justifyContent: "space-between" }} >
                    <TouchableOpacity onPress={pickerDropdownModalClose}
                        style={{ flex: 1, alignItems: "center", justifyContent: "center", backgroundColor: "#2A56C6", borderRadius: 4 }}>
                        <Text style={{ fontSize: fontSizes.font18, color: "white" }}>
                            Cancel

                    </Text>
                    </TouchableOpacity>
                    

                </View> */}
                {type == 'multiSelect' ?
                    <View style={{ flexDirection: "row", justifyContent: "space-between", width: width - 100, alignItems: 'center', }}>
                        <TouchableWithoutFeedback onPress={pickerDropdownModalClose}>
                            <View style={{ height: 60, width: 60, backgroundColor: "red", borderRadius: 30, alignItems: "center", justifyContent: "center", marginTop: -25 }}>
                                <Image style={{ height: 30, width: 30 }}
                                    source={require('../images/crossIcon.png')} />
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress={pickerDropdownSelectClicked}>
                            <View style={{ height: 60, width: 60, backgroundColor: "#2A56C6", borderRadius: 30, alignItems: "center", justifyContent: "center", marginTop: -25 }}>
                                <Image style={{ height: 20, width: 25 }}
                                    source={require('../images/headerIcons/tickIcon.png')} />
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                    :
                    <TouchableWithoutFeedback onPress={pickerDropdownModalClose}>
                        <View style={{ height: 60, width: 60, backgroundColor: "red", borderRadius: 30, alignItems: "center", justifyContent: "center", marginTop: -25, }}>
                            <Image style={{ height: 30, width: 30 }}
                                source={require('../images/crossIcon.png')} />
                        </View>
                    </TouchableWithoutFeedback>}
            </View>

        </Modal>
    )
}
const styles = StyleSheet.create({
    modalContainer: { height: height, width: width, backgroundColor: "#00000098", alignItems: "center", justifyContent: "center", padding: 10, paddingTop: 70, paddingBottom: 50 },
    bottomContainer: { width: width - 20, backgroundColor: "white", borderRadius: 10, height: height - 120, paddingBottom: 20 },

});






export default SelectProjectModal;