
import React, { Component } from 'react';
import {
    StyleSheet,
    Text, Modal, Picker, TouchableWithoutFeedback,
    View, Image, TouchableOpacity, TextInput, ScrollView, Platform
} from 'react-native';
import { height, width, fontSizes } from '../utils/utils'

const AddEmployeeModal = ({ modalVisible, ActiveStatusPassedOnModal, onSwitchClickOfModal, modalType, addEmployeeModalTitle, activityValue, activityChangeText, modalClose, onEmployeeDetailSavePress, employeeNameMandatory, employeeNameMandatoryText, fullNameChangeText, fullNameValue, emailChangeText, emailValue }) => {

    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={modalClose}>
            <ScrollView keyboardShouldPersistTaps="always">
                <View style={styles.modalContainer}>
                    <TouchableOpacity onPress={modalClose} style={{ flex: 1, width: width }} />

                    <View style={styles.centerContainer}>
                        <View style={{ flexDirection: "row", justifyContent: 'space-between' }}>
                            <Text style={[styles.textStyle, { fontSize: fontSizes.font18, color: "#000", fontWeight: "bold" }]}>{addEmployeeModalTitle}</Text>

                            {global.userType == 'Admin' ?
                                <TouchableWithoutFeedback onPress={onSwitchClickOfModal}>
                                    <View style={{ flexDirection: "row", justifyContent: 'space-between', alignItems: 'center' }}>
                                        <Text style={{ marginRight: 10, color: ActiveStatusPassedOnModal == 'Active' ? '#06B15A' : 'red' }}>{ActiveStatusPassedOnModal}</Text>
                                        <Image source={ActiveStatusPassedOnModal == 'Active' ? require('../images/switchGreen.png') : require('../images/switchRed.png')} />
                                    </View>
                                </TouchableWithoutFeedback>
                                : null}
                        </View>
                        {employeeNameMandatory ? <Text style={{ fontSize: 12, color: "red" }}>
                            {employeeNameMandatoryText}
                        </Text> : null}
                        {modalType == 'Add Employee' ?
                            <TextInput
                                style={styles.textInput}
                                placeholder="Full Name*"
                                placeholderTextColor="#979797"
                                underlineColorAndroid="transparent"
                                onChangeText={fullNameChangeText}
                                value={fullNameValue}
                            />
                            : null}

                        {modalType == 'Add Employee' ?
                            <TextInput
                                style={styles.textInput}
                                placeholder="Email Id*"
                                placeholderTextColor="#979797"
                                underlineColorAndroid="transparent"
                                onChangeText={emailChangeText}
                                value={emailValue}
                            />
                            : null}
                        {modalType == 'Add Employee' ? null :
                            <TextInput
                                style={styles.textInput}
                                placeholder="Activity Name*"
                                placeholderTextColor="#979797"
                                underlineColorAndroid="transparent"
                                onChangeText={activityChangeText}
                                value={activityValue}
                            />
                        }
                        <TouchableOpacity style={styles.saveButton} onPress={onEmployeeDetailSavePress}>
                            <Text style={styles.daysText}>
                                Save
                    </Text>
                        </TouchableOpacity>

                    </View>

                    <TouchableOpacity onPress={modalClose} style={{ flex: 1, width: width }} />
                </View>
            </ScrollView>
        </Modal>
    )
}
const styles = StyleSheet.create({
    modalContainer: { height: height, width: width, backgroundColor: "#00000090", alignItems: "center", justifyContent: "center" },
    centerContainer: { width: "80%", padding: 15, backgroundColor: "white", paddingRight: Platform.OS == 'ios' ? 15 : 25 },
    textStyle: { fontSize: fontSizes.font14, color: "#979797", textAlign: 'left', marginTop: 10, marginBottom: 10 },
    textInput: { borderBottomColor: "#D8D8D8", borderBottomWidth: 1, height: 40, marginTop: 10, color: '#979797', fontSize: fontSizes.font14 },
    saveButton: { alignSelf: "flex-end", marginBottom: 10, marginTop: 20 },
    daysText: { color: "#2A56C6", margin: 5, fontSize: fontSizes.font14 },


});






export default AddEmployeeModal;