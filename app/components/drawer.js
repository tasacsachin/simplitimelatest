
import React, { Component } from 'react';
import {

    StyleSheet, Alert,AsyncStorage,
    Text, TouchableWithoutFeedback,
    View, ScrollView, Image, TouchableOpacity
} from 'react-native';
import { height, width, fontSizes,getLog } from '../utils/utils'
import { DrawerNavigator, DrawerItems, StackNavigator } from 'react-navigation';
export default class customDrawer extends Component {
    logoutPress(){
        Alert.alert(
            'Logout',
            'Are you sure you want to logout?',
            [
                { text: 'Cancel', onPress: () => getLog('Cancel Pressed'), style: 'cancel' },
                { text: 'OK', onPress: () => getLog('OK Pressed') },
            ],
            { cancelable: false }
        )
    }
    render(){
        return(
            <View style={styles.container} >
                <ScrollView>
                    <View style={styles.topView}>
                        <View style={styles.userDataView}>
                            <Image source={require('../images/userImage.png')}
                                style={styles.userImage} />
                            <View style={styles.emailView}>
                                <Text style={styles.emailText}>
                                    {email}
                                    </Text>
                                <TouchableOpacity onPress={() => alert("edit")}>
                                    <Image source={require('../images/drawerIcons/editIcon.png')} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <DrawerItems
                        {...props}
                    />
                    <TouchableWithoutFeedback onPress={() => logoutPress()}>
                        <View style={{ flexDirection: "row", paddingLeft: 20, padding: 8 }}>
                            <Image source={require('../images/drawerIcons/logoutIcon.png')} />
                            <Text style={{ fontSize: 14, color: '#333333', marginLeft: 38, fontWeight: "bold" }}>
                                Logout
                            </Text>
                        </View>
                    </TouchableWithoutFeedback>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    topView: {
        height: 150,
        width: width - (width / 5),
        backgroundColor: "#2A56C6",
        alignItems: "center",
        justifyContent: 'center'
    },
    userDataView: {
        width: (width - (width / 5)) - 20
    },
    userImage: { height: 60, width: 60, borderRadius: 30, marginTop: 25, },
    emailText: {
        textAlign: 'left',
        color: '#fff',

    },
    emailView: { flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginTop: 20, }

});