/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet, StatusBar, Keyboard, TouchableWithoutFeedback,
    Text, SafeAreaView, TouchableOpacity, Animated, Easing,
    View, Image, TextInput, ScrollView, AsyncStorage
} from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import { getLog, height, width } from '../utils/utils';
import webservice from '../components/webService'



export default class splash extends Component {

    constructor() {
        super()
        //this.animatedValue = new Animated.Value(0)
        this.springValue = new Animated.Value(0.5)
        this.spinValue = new Animated.Value(0)
        this.spin()
    }
    componentDidMount() {
        // this.animate()
        this.spring()  
        this.spring.bind(this)      
    }
    spring () {
        this.springValue.setValue(0.3)
        Animated.spring(
          this.springValue,
          {
            toValue: 1,
            friction: 1
          }
        ).start()
      }
      spin () {
        this.spinValue.setValue(0)
        Animated.timing(
          this.spinValue,
          {
            toValue: 1,
            duration: 2050,
            easing: Easing.linear
          }
        ).start(() => this.spin())
      }
    animate() {
        this.animatedValue.setValue(0)
        Animated.timing(
            this.animatedValue,
            {
                toValue: 1,
                duration: 4000,
                easing: Easing.linear
            }
        ).start(() => this.animate())
    }

    componentWillMount() {
        var userToken = ''
        AsyncStorage.getItem('userToken').then((token) => {

            userToken = token == null ? '' : token;
            
            
        })
        setTimeout(() => {
            if (userToken.length > 0) {
                AsyncStorage.getItem('userType').then((data) => {
                    global.userType = data
                    AsyncStorage.getItem('LoginresponseData').then((data) => {
                        var d=JSON.parse(data)
                        global.uType = d.userType;
                        //alert(global.uType)
                        this.hitGetUserObjectAPI(userToken,global.userType)
                        getLog("userToken===" + userToken)
                        getLog("LoginresponseData===" + JSON.stringify(data))
                    })
                    
                    
                    

                })

                

            } else {
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'Login' })],
                });
                this.props.navigation.dispatch(resetAction);
            }
        }, 2000)
    }
    hitGetUserObjectAPI(token,data) {
        return webservice('', "user/getprofile", 'Get', token)
          .then(response => {
            getLog("Get User Object API called on splash screen"+JSON.stringify(response))
            if (response != "error") {
                AsyncStorage.setItem("LoginresponseData",JSON.stringify(response), () => {
                    if(data=='Admin'){
                        const resetAction = StackActions.reset({
                            index: 0,
                            actions: [NavigationActions.navigate({ routeName: 'DrawerOfAdmin' })],
                        });
                        this.props.navigation.dispatch(resetAction);
                    }else{
                        const resetAction = StackActions.reset({
                            index: 0,
                            actions: [NavigationActions.navigate({ routeName: 'Drawer' })],
                        });
                        this.props.navigation.dispatch(resetAction);
                    }
                
                })


                

            }
    
          })
      }
    render() {
        // const opacity = this.animatedValue.interpolate({
        //     inputRange: [0, 0.5, 1],
        //     outputRange: [0, 1, 0]
        // })
        // const Size = this.animatedValue.interpolate({
        //     inputRange: [0, 0.5, 1],
        //     outputRange: [width, width, width]
        //   })
        const spin = this.spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['180deg', '0deg']
          })
        return (
            <SafeAreaView
                style={styles.safeArea}>
                <StatusBar
                translucent
                backgroundColor="#2A56C6"
                barStyle="light-content"
            />
                <View style={styles.container}>
                    <Animated.Image style={{  width:width, height:width,transform: [{scale: this.springValue}
                        // ,{rotate: spin}
                        ] }} source={require('../images/logo.png')} />
                </View>



            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2A56C6',
    },
    iOSTopBar: {
        backgroundColor: '#21459E',
    },
    safeArea: {
        flex: 1,
        backgroundColor: '#2A56C6'
    },

});
