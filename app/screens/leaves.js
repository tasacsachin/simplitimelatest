/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Image
} from 'react-native';
import TimeSheet from './timeSheet'
export default class leaves extends Component {

    
    static navigationOptions = {
        // drawerLabel: 'Leaves',
        // drawerIcon: ({ tintColor }) => (
        //     <Image style={{tintColor: tintColor}}
        //     source={require('../images/drawerIcons/leavesIcon.png')} />
        // ),
        drawerLabel: () => global.userType == 'Admin' ? null : 'Leaves',
        
        drawerIcon: ({ tintColor }) => global.userType == 'Admin' ? null : (
            <Image style={{ tintColor: tintColor }}
                source={require('../images/drawerIcons/leavesIcon.png')}
            />
        ),
    };
    
    render() {
        return (
            <TimeSheet id="leaves" navigation={this.props.navigation}/>
        );
    }
}


