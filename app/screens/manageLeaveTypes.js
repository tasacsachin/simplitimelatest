/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Image
} from 'react-native';
import Setting from './setting'
import { height, width, fontSizes } from '../utils/utils'
export default class manageLeaveTypes extends Component {
    static navigationOptions = {
        // drawerLabel: 'Manage Leave Types',
        // drawerIcon: ({ tintColor }) => (
        //     <Image style={{tintColor: tintColor}}
        //     source={require('../images/drawerIcons/manageLeaves.png')}
        //      />
        // ),
        drawerLabel: () => global.userType == 'Admin' ? null : 'Manage Leave Types',
        
        drawerIcon: ({ tintColor }) => global.userType == 'Admin' ? null : (
            <Image style={{ tintColor: tintColor }}
                source={require('../images/drawerIcons/manageLeaves.png')}
            />
        ),
    };
    render() {
        return (
            <Setting id="manageLeaveTypes" navigation={this.props.navigation}/>
        );
    }
}


