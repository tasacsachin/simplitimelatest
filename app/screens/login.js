/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  StatusBar,
  Keyboard,
  TouchableWithoutFeedback,
  Text,
  SafeAreaView,
  TouchableOpacity,
  LayoutAnimation,
  View,
  Image,
  TextInput,
  ScrollView,
  AsyncStorage
} from "react-native";

import { height, width, fontSizes, getLog } from "../utils/utils";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { StackActions, NavigationActions } from "react-navigation";
import webservice from "../components/webService";
import Spinner from "../components/spinner";
const emailRegex = /^[A-Z0-9_]+([\.][A-Z0-9_]+)*@[A-Z0-9-]+(\.[a-zA-Z]{2,4})+$/i;
export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      companyId: "",
      keyboardOpen: false,
      check: false,
      spinnerVisible: false,
      zeroPlusFullWidth: 0
    };
  }
  componentDidMount() {
    if (Platform.OS == "ios") {
      this.keyboardDidShowListener = Keyboard.addListener(
        "keyboardWillShow",
        () => this.setState({ keyboardOpen: true })
      );
      this.keyboardDidHideListener = Keyboard.addListener(
        "keyboardWillHide",
        () => this.setState({ keyboardOpen: false })
      );
    } else {
      this.keyboardDidShowListener = Keyboard.addListener(
        "keyboardDidShow",
        () =>
          this.setState({
            keyboardOpen: true,
            marginFromBottom: this.state.company ? 55 : 10
          })
      );
      this.keyboardDidHideListener = Keyboard.addListener(
        "keyboardDidHide",
        () => this.setState({ keyboardOpen: false, marginFromBottom: 10 })
      );
    }
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }
  onChange(text, type) {
    //alert(text + "==" + type)
    switch (type) {
      case "Email":
        this.setState({ email: text.trim() });
        break;
      case "Password":
        this.setState({ password: text.trim() });
        break;
      case "Company ID":
        this.setState({ companyId: text });
        break;
    }
  }

  titleNamefunction(text) {
    return <Text style={styles.titleText}>{text}</Text>;
  }
  textField(placeholder, value) {
    return (
      <TextInput
        style={styles.textInput}
        placeholder={placeholder}
        value={value}
        placeholderTextColor="#333333"
        underlineColorAndroid="transparent"
        maxLength={placeholder == "Company ID" ? 5 : 100}
        secureTextEntry={placeholder == "Password" ? true : false}
        onChangeText={text => this.onChange(text, placeholder)}
        //value={ProjectCodeValue}
      />
    );
  }
  popUp(msg) {
    alert(msg);
  }
  onLogin() {
    if (this.state.email == "") {
      this.popUp("Please enter email.");
    } else if (!emailRegex.test(this.state.email)) {
      this.popUp("Please enter valid email.");
    } else if (this.state.password == "") {
      this.popUp("Please enter password.");
    } else {
      this.setState({ spinnerVisible: true });
      var deviceType = Platform.OS == "ios" ? 1 : 2;
      let variables = new FormData();

      variables.append("email", this.state.email);
      variables.append("password", this.state.password);
      variables.append("deviceType", deviceType);
      if (this.state.companyId.length > 0) {
        variables.append("companyCode", this.state.companyId);
      }
      if (this.state.check) {
        variables.append("company", "1");
      }

      return webservice(variables, "login", "POST").then(response => {
        this.setState({ spinnerVisible: false });
        if (response != "error") {
          getLog("UserToken" + response.userToken);
          global.uType = response.userType;
          AsyncStorage.setItem("userToken", response.userToken, () => {
            AsyncStorage.setItem(
              "LoginresponseData",
              JSON.stringify(response),
              () => {
                
                //global.userType = 'company'
                //alert(response.userType)
                //var typeOfUser = this.state.check ? "Admin" : "Employee";
                if(response.userType=='company'){
                    typeOfUser="Admin"
                }else if(response.userType=='employee'){
                    typeOfUser="Employee"
                }else{
                    typeOfUser="Individual"
                }
                
                AsyncStorage.setItem("userType", typeOfUser, () => {
                  global.userType = typeOfUser;

                  if (typeOfUser == "Admin") {
                    const resetAction = StackActions.reset({
                      index: 0,
                      actions: [
                        NavigationActions.navigate({
                          routeName: "DrawerOfAdmin"
                        })
                      ]
                    });
                    this.props.navigation.dispatch(resetAction);
                  } else {
                    const resetAction = StackActions.reset({
                      index: 0,
                      actions: [
                        NavigationActions.navigate({ routeName: "Drawer" })
                      ]
                    });
                    this.props.navigation.dispatch(resetAction);
                  }
                });
              }
            );
          });
        }
      });
    }
  }
  // onLogin(){
  //     this.expandElement()
  //     const resetAction = StackActions.reset({
  //         index: 0,
  //         actions: [NavigationActions.navigate({ routeName: 'Drawer' })],
  //     });
  //     this.props.navigation.dispatch(resetAction);
  // }

  render() {
    return (
      <SafeAreaView style={styles.safeArea}>
        <Spinner visible={this.state.spinnerVisible} />

        {Platform.OS == "ios" ? (
          <View style={styles.iOSTopBar} />
        ) : (
          <StatusBar backgroundColor="#21459E" barStyle="light-content" />
        )}

        {/* <KeyboardAwareScrollView keyboardShouldPersistTaps="always"> */}
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <View style={styles.container}>
            <View
              style={{
                height: height / 2 - 50,
                backgroundColor: "transparent",
                alignItems: "center"
              }}
            >
              <Image
                source={require("../images/loginSignup/Simplitime.png")}
                style={{ marginTop: height / 8 }}
              />
            </View>
            <View
              style={{
                height:
                  Platform.OS === "ios" && height === 812
                    ? height / 2 - 25
                    : height / 2 + 50,
                backgroundColor: "#E0E0E0",
                justifyContent: "flex-end",
                alignItems: "center"
              }}
            >
              <View style={{ flexDirection: "row", paddingBottom: 50 }}>
                <Text
                  style={[
                    styles.titleText,
                    {
                      color: "#898989",
                      textAlign: "right",
                      fontSize: fontSizes.font16,
                      marginTop: 20
                    }
                  ]}
                >
                  Don't have an account?
                </Text>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate("Signup")}
                >
                  <Text
                    style={[
                      styles.titleText,
                      {
                        color: "#2A56C6",
                        textAlign: "right",
                        fontSize: fontSizes.font16,
                        marginTop: 20
                      }
                    ]}
                  >
                    {" Sign up"}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View
              style={[
                styles.card,
                {
                  top: this.state.keyboardOpen
                    ? 10
                    : Platform.OS === "ios" && height === 812
                      ? height / 3.5
                      : height / 3.5 - 30,
                  maxHeight:
                    Platform.OS === "ios" && height === 812
                      ? height / 2
                      : height / 2 + 50 + 30
                }
              ]}
            >
              <ScrollView keyboardShouldPersistTaps="always">
                {this.titleNamefunction("Email")}
                {this.textField("Email", this.state.email)}

                {this.titleNamefunction("Password")}
                {this.textField("Password", this.state.password)}

                {this.titleNamefunction("Company ID (If Applicable)")}
                {this.textField("Company ID", this.state.companyId)}
              </ScrollView>
              <View flexDirection="row" justifyContent="flex-end">
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate("ForgotPassword")
                  }
                >
                  <Text
                    style={[
                      styles.titleText,
                      {
                        color: "#2A56C6",
                        textAlign: "right",
                        fontSize: fontSizes.font16,
                        marginTop: 20
                      }
                    ]}
                  >
                    Forgot Password?
                  </Text>
                </TouchableOpacity>
              </View>
              <View flexDirection="row" justifyContent="flex-start">
                <TouchableOpacity
                  onPress={() => this.setState({ check: !this.state.check })}
                >
                  <Image
                    source={
                      this.state.check
                        ? require("../images/createTimesheet/tickIcon.png")
                        : require("../images/createTimesheet/uncheckIcon.png")
                    }
                  />
                </TouchableOpacity>
                <Text
                  style={[
                    styles.titleText,
                    { fontSize: fontSizes.font12, marginTop: 0 }
                  ]}
                >
                  I am Company Admin
                </Text>
              </View>
              <TouchableWithoutFeedback onPress={() => this.onLogin()}>
                <View style={styles.buttonStyle}>
                  <Text
                    style={[
                      styles.titleText,
                      {
                        color: "white",
                        textAlign: "center",
                        fontSize: fontSizes.font16,
                        marginTop: 0
                      }
                    ]}
                  >
                    LOG IN
                  </Text>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </View>
        </TouchableWithoutFeedback>
        {/* </KeyboardAwareScrollView> */}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //justifyContent: 'center',
    //alignItems: 'flex-start',
    backgroundColor: "#2A56C6"
  },
  iOSTopBar: {
    backgroundColor: "#21459E"
  },
  safeArea: {
    flex: 1,
    backgroundColor: "#21459E"
  },
  card: {
    position: "absolute",
    width: width - 20,
    left: 10,
    right: 10,
    backgroundColor: "#fff",

    elevation: 5,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    padding: 15,
    borderRadius: 3
  },

  titleText: {
    fontSize: fontSizes.font14,
    color: "#898989",
    marginTop: 15,
    marginLeft: 5
  },
  textInput: {
    height: 45,
    borderBottomColor: "#D8D8D8",
    borderBottomWidth: 1,
    color: "#333333",
    fontSize: fontSizes.font14,
    marginHorizontal: 5,
    marginTop: 5
  },
  buttonStyle: {
    backgroundColor: "#2A56C6",
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
    borderRadius: 3
  }
});
