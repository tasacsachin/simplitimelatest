/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform, Alert,
    StyleSheet, TextInput, ScrollView,
    Text, Keyboard, AsyncStorage, Linking,
    View, Image, SafeAreaView, TouchableWithoutFeedback
} from 'react-native';
//import { GoogleSignin, GoogleSigninButton } from 'react-native-google-signin';
import { StackActions, NavigationActions } from 'react-navigation';
import { height, width, fontSizes, getLog } from '../utils/utils'
import BackgroundTimer from 'react-native-background-timer';
import RNAndroidBackgroundTimer from 'react-native-android-background-timer';
import { dateConverter, week_Date_Array, year_Array, dateConverterUseInWeekTab, dateConverterUseInHeader, hourMinuteConverter } from '../components/dateConverter'
import Header from '../components/commonHeader'
var ImagePicker = require('react-native-image-picker');
import webservice from '../components/webService'
import Spinner from '../components/spinner'
import Mailer from 'react-native-mail';
var MaileriOS = require('NativeModules').RNMail;
var PushNotification = require('react-native-push-notification');
export default class activityList extends Component {
    static navigationOptions = {
        drawerLabel: 'Settings',
        drawerIcon: ({ tintColor }) => (
            <Image style={{ tintColor: tintColor }}
                source={require('../images/drawerIcons/settingIcon.png')}
            />
        ),
    };
    constructor(props) {

        super(props);
        //alert(JSON.stringify(this.props.navigation.state.params.props.navigation.openDrawer()))
        // alert(JSON.stringify(this.props.navigation.state.params.props.state))
        this.state = {
            //userImage: require('../images/userImage.png'),
            userImage: { uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLb5mOUtzV0ObqBVuAURSvPAsC27148aFdKGc6e6Z_Z78vmMWf' },
            image64: '',
            name: '',
            email: '',
            nameEdit: false,
            emailEdit: false,
            spinnerVisible: false,
            userToken: '',
            userId: ''
        }
    }
    componentDidMount() {
        AsyncStorage.getItem('userToken').then((token) => {
            this.setState({ userToken: token })
            this.hitGetUserObjectAPI(token)
        })
    }
    componentWillMount() {
        AsyncStorage.getItem('LoginresponseData').then((response) => {
            var data = JSON.parse(response)
            this.setState({ email: data.email })
            this.setState({ name: data.fullName })
            getLog(data.thumbPhoto)
            if (data.thumbPhoto) {
                data.thumbPhoto.uri ?
                    this.setState({ userImage: data.thumbPhoto })
                    :
                    this.setState({ userImage: { uri: data.thumbPhoto } })
            }

        })
    }
    Callback;
    static settingCallback(callback) {
        Callback = callback
    }
    openImagePicker() {
        var options = {

        };

        ImagePicker.showImagePicker(options, (response) => {

            if (response.didCancel) {
                console.log('User cancelled image picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {

                this.setState({ spinnerVisible: true })
                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };


                //let source = { uri: response.uri };
                this.hitEditProfileAPI('image', response.data)
                // this.setState({
                //     userImage: source, image64: response.data,
                // })
                // AsyncStorage.getItem('LoginresponseData').then((response) => {
                //     response = JSON.parse(response)
                //     response.image = source
                //     AsyncStorage.setItem('LoginresponseData', JSON.stringify(response), () => {
                //         Callback("callback called image changed" + source)
                //     });
                // });
                // setTimeout(() => {
                //     console.log(this.state.image64)
                // }, 300)

            }
            // You can also display the image using data:
            // let source = { uri: 'data:image/jpeg;base64,' + response.data };

            //   this.setState({
            //     avatarSource: source
            //   });

        });
    }
    onChange(text, type) {
        if (type === 'Name') {
            this.setState({ name: text })
        }
        if (type === 'Email') {
            this.setState({ email: text })
        }
    }
    hitEditProfileAPI(hitFrom, imageBase64) {
        let variables = new FormData();
        //this.setState({ spinnerVisible: true, projectNameMandatory: false })
        variables.append("fullName", this.state.name)
        variables.append("email", this.state.email)
        if (hitFrom == 'image') {
            variables.append("photo", imageBase64)
        }



        this.setState({ spinnerVisible: true })
        return webservice(variables, "user/updateprofile", 'POST', this.state.userToken)
            .then(response => {
                this.setState({ spinnerVisible: false })

                if (response != "error") {
                    this.setState({ emailEdit: false })
                    this.setState({ nameEdit: false })

                    setTimeout(() => {

                        AsyncStorage.getItem('LoginresponseData').then((res) => {
                            res = JSON.parse(res)
                            res.fullName = this.state.name
                            res.email = this.state.email
                            res.thumbPhoto = { uri: response.userData.thumbPhoto }
                            this.setState({
                                userImage: { uri: response.userData.thumbPhoto }
                            })
                            this.hitGetUserObjectAPI(this.state.userToken)
                            AsyncStorage.setItem('LoginresponseData', JSON.stringify(res), () => {
                                Callback("callback called name changed" + this.state.name)
                            });
                        });
                        //this.props.navigation.state.params.employeeDetailCallBack.callBack("data")
                        //this.setState({ addEmployeeModalVisible: false })
                    }, 300)
                } else {


                }
                getLog("response of edit user detail " + response)

            })
    }

    hitGetUserObjectAPI(token) {
        return webservice('', "user/getprofile", 'Get', token)
          .then(response => {
            getLog("Get User Object API called on setting screen"+JSON.stringify(response))
            if (response != "error") {
                AsyncStorage.setItem("LoginresponseData",JSON.stringify(response), () => {
                    Callback("callback called name changed" + this.state.name)
                
                })


                

            }
    
          })
      }
    nameEdit() {

        if (this.state.nameEdit) {
            if (this.state.name != '') {
                this.hitEditProfileAPI('detail')
            } else {
                alert('Please enter name.')
            }



        } else {
            if (this.state.emailEdit) {
                alert("Please save email id first.")
            } else {
                this.setState({ nameEdit: true })
            }

        }
    }
    emailEdit() {
        var emailRegex = (/^[A-Z0-9_]+([\.][A-Z0-9_]+)*@[A-Z0-9-]+(\.[a-zA-Z]{2,4})+$/i)

        if (this.state.emailEdit) {
            if (this.state.email == '') {
                alert('Please enter email.')


            } else if (!emailRegex.test(this.state.email)) {
                alert('Please enter valid email.')

            } else {
                this.hitEditProfileAPI('detail')
            }

        } else {
            if (this.state.nameEdit) {
                alert("Please save fullname first.")
            } else {
                this.setState({ emailEdit: true })
            }

        }


    }
    googleSignOut = async () => {
        try {
            await GoogleSignin.revokeAccess();
            await GoogleSignin.signOut();
            this.setState({ user: null }); // Remember to remove the user from your app's state as well
        } catch (error) {
            console.error(error)
        }
    };
    signOut() {
        this.setState({ spinnerVisible: true })
        AsyncStorage.getItem('userToken').then((token) => {

            return webservice('', "logout", 'GET', token)
                .then(response => {
                    this.setState({ spinnerVisible: false })
                    //if (response != "error") {
                        PushNotification.cancelAllLocalNotifications();
                        global.flag = false;
                        global.selectedProject = '';
                        global.play = false
                        backgroundSecond = 0
                        clearInterval(global.interval);
                        clearInterval(global.intervalId);
                        BackgroundTimer.clearInterval(global.intervalId);
                        RNAndroidBackgroundTimer.clearInterval(global.intervalId);
                        //this.googleSignOut()
                        AsyncStorage.removeItem('userToken').then((token) => {
                            getLog("tokenRemoved")
                        })

                        AsyncStorage.removeItem('LoginresponseData').then((token) => {
                            getLog("LoginresponseDataRemoved")
                        })
                        AsyncStorage.removeItem('userType').then((token) => {
                            getLog("LoginresponseDataRemoved")
                        })

                        const resetAction = StackActions.reset({
                            index: 0,
                            actions: [NavigationActions.navigate({ routeName: 'Login' })],
                        });
                        this.props.navigation.dispatch(resetAction);

                    //}

                })
        })

    }
    signOutPress() {
        //navigation.navigate('Timesheet')
        Alert.alert(
            'Logout',
            'Are you sure you want to logout?',
            [
                { text: 'Cancel', onPress: () => getLog('Cancel Pressed'), style: 'cancel' },
                { text: 'OK', onPress: () => this.signOut() },
            ],
            { cancelable: false }
        )
    }
    handleEmail = () => {
        if (Platform.OS == 'ios') {
            // MaileriOS.mail({
            //     subject: 'need help',
            //     recipients: ['tasacsachin@gmail.com'],
            //     ccRecipients: ['sachin@greychaindesign.com'],
            //     bccRecipients: ['13cs005@bit.ac.in'],
            //     body: '<b>A Bold Body</b>',
            //     isHTML: true,
            //     attachment: {
            //         path: '',  // The absolute path of the file from which to read data.
            //         type: '',   // Mime Type: jpg, png, doc, ppt, html, pdf
            //         name: '',   // Optional: Custom filename for attachment
            //     }
            // }, (error, event) => {
            //     Alert.alert(
            //         error,
            //         event,
            //         [
            //             { text: 'Ok', onPress: () => console.log('OK: Email Error Response') },
            //             { text: 'Cancel', onPress: () => console.log('CANCEL: Email Error Response') }
            //         ],
            //         { cancelable: true }
            //     )
            // });
            // const inbox =  Linking.canOpenURL(`mail`);

            // // if inbox is installed, open that
            // if (inbox) {
            //     Linking.openURL(`mail`);
            // } else {
            //     // else default to iOS Mail app
            //     Linking.openURL(`https://facebook.github.io/react-native/docs/linking`);
            // }
            var url = 'https://accounts.google.com/signin/v2/identifier?hl=en&passive=true&continue=https%3A%2F%2Fwww.google.co.in%2Fsearch%3Fq%3Dgmail%26oq%3Dgmail%26aqs%3Dchrome..69i57j69i61l3.935j0j1%26sourceid%3Dchrome%26ie%3DUTF-8&flowName=GlifWebSignIn&flowEntry=ServiceLogin'
            //     Linking.canOpenURL(url).then(supported => {
            //         if (!supported) {
            //           console.log('Can\'t handle url: ' + url);
            //         } else {
            //           return Linking.openURL(url);
            //         }
            //       }).catch(err => console.error('An error occurred', err));
            Linking.canOpenURL('mailto:tasacsachin@gmail.com').then(supported => {
                if (!supported) {
                    Linking.openURL(url)
                } else {
                    return Linking.openURL('mailto:sales@greychaindesign.com');
                }
            }).catch(err => console.error('An error occurred', err));
            //Linking.openURL('mailto:tasacsachin@gmail.com')
        } else {
            Mailer.mail({
                subject: 'I need help',
                recipients: ['sales@greychaindesign.com'],
                //ccRecipients: ['sachin@greychaindesign.com'],
                //bccRecipients: ['13cs005@bit.ac.in'],
                // body: '<b>A Bold Body</b>',
                // isHTML: true,
                // attachment: {
                //     path: '',  // The absolute path of the file from which to read data.
                //     type: '',   // Mime Type: jpg, png, doc, ppt, html, pdf
                //     name: '',   // Optional: Custom filename for attachment
                // }
            }, (error, event) => {
                Alert.alert(
                    error,
                    event,
                    [
                        { text: 'Ok', onPress: () => console.log('OK: Email Error Response') },
                        { text: 'Cancel', onPress: () => console.log('CANCEL: Email Error Response') }
                    ],
                    { cancelable: true }
                )
            });
        }

    }
    render() {
        return (
            <SafeAreaView
                style={styles.safeArea}>
                <Spinner visible={this.state.spinnerVisible} />
                <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>

                    <View style={styles.container}>
                        <Header HeaderLeftText="   Settings"
                            //HeaderRightText={this.state.datePassedToHeader != '' ? this.state.datePassedToHeader : today_To_day_AfterEighth_Day()}
                            //type="date"
                            //this.props.navigation.state.params
                            //alert(JSON.stringify(this.props.navigation.state.params.props.navigation.state.openDrawer()))
                            //toggleDrawer={() => this.props.navigation.state.params.props.state.routes.navigate('Drawer')}
                            toggleDrawer={() => this.props.navigation.openDrawer()}
                        //headerLeftIcon={require('../images/headerIcons/downArrow.png')}
                        // headerRightIcon={require('../images/analysisExportIcons/dateIcon.png')}
                        //headerRightIconOnPress={() => this.showStartDatePicker()} 
                        />
                        <View style={styles.imageRelativeView} />
                        <View style={styles.imageViewExternal}>
                            <View style={styles.imageViewInternal}>
                                <Image source={this.state.userImage} resizeMode="cover"
                                    style={styles.userImage} />
                                <TouchableWithoutFeedback onPress={() => this.openImagePicker()}>
                                    <Image source={require('../images/appSettingIcons/FAB.png')}
                                        style={styles.plusView} />
                                </TouchableWithoutFeedback>


                            </View>

                        </View>

                        <ScrollView keyboardShouldPersistTaps="always"
                            style={styles.detailContainer}>
                            <View style={styles.rowView}>
                                <View style={{ flex: 1, justifyContent: "center", marginLeft: 15 }}>
                                    <Text style={styles.textInputText}>Name</Text>
                                </View>
                                <View style={styles.textInputView}>
                                    <TextInput
                                        style={[styles.textInput, { color: this.state.nameEdit ? '#000' : '#979797' }]}
                                        placeholder="Name"
                                        placeholderTextColor="#979797"
                                        underlineColorAndroid="transparent"
                                        onChangeText={(text) => this.onChange(text, 'Name')}
                                        value={this.state.name}
                                        editable={this.state.nameEdit ? true : false}
                                        autoFocus={this.state.nameEdit ? true : false}

                                        autoCapitalize='none'
                                        autoCorrect={false}
                                    />
                                </View>
                                <View style={styles.editView}>
                                    <TouchableWithoutFeedback onPress={() => this.nameEdit()}>
                                        <Image source={this.state.nameEdit ? require('../images/appSettingIcons/checkIconGrey.png') : require('../images/appSettingIcons/editIconGrey.png')} resizeMode="cover"
                                        />
                                    </TouchableWithoutFeedback>
                                </View>
                            </View>

                            <View style={styles.rowView}>
                                <View style={{ flex: 1, justifyContent: "center", marginLeft: 15 }}>
                                    <Text style={styles.textInputText}>Email</Text>
                                </View>
                                <View style={styles.textInputView}>
                                    <TextInput
                                        style={[styles.textInput, { color: this.state.emailEdit ? '#000' : '#979797' }]}
                                        placeholder="Email"
                                        placeholderTextColor="#979797"
                                        underlineColorAndroid="transparent"
                                        onChangeText={(text) => this.onChange(text, 'Email')}
                                        value={this.state.email}
                                        editable={this.state.emailEdit ? true : false}
                                        autoFocus={this.state.emailEdit ? true : false}
                                        autoCapitalize='none'
                                        autoCorrect={false}
                                    />
                                </View>
                                <View style={styles.editView}>
                                    <TouchableWithoutFeedback onPress={() => this.emailEdit()}>
                                        <Image source={this.state.emailEdit ? require('../images/appSettingIcons/checkIconGrey.png') : require('../images/appSettingIcons/editIconGrey.png')} resizeMode="cover"
                                        />
                                    </TouchableWithoutFeedback>
                                </View>
                            </View>
                            <TouchableWithoutFeedback onPress={() => this.handleEmail()} >
                                <View style={[styles.rowView, { justifyContent: 'space-between' }]}>
                                    <View style={{ justifyContent: "center", marginLeft: 15 }}>
                                        <Text style={styles.textInputText}>Contact Us</Text>
                                    </View>



                                    <View style={styles.rightArrowView}>
                                        <Image source={require('../images/appSettingIcons/rightArrow.png')} resizeMode="cover"
                                        />
                                    </View>


                                </View>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('TermsCondition')}>
                                <View style={[styles.rowView, { justifyContent: 'space-between' }]}>
                                    <View style={{ justifyContent: "center", marginLeft: 15 }}>
                                        <Text style={styles.textInputText}>Terms & Conditions</Text>
                                    </View>



                                    <View style={styles.rightArrowView}>
                                        <Image source={require('../images/appSettingIcons/rightArrow.png')} resizeMode="cover"
                                        />
                                    </View>


                                </View>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('ResetPassword', { data: 'fromSetting' })}>
                                <View style={[styles.rowView, { justifyContent: 'space-between' }]}>
                                    <View style={{ justifyContent: "center", marginLeft: 15 }}>
                                        <Text style={styles.textInputText}>Change Password</Text>
                                    </View>



                                    <View style={styles.rightArrowView}>
                                        <Image source={require('../images/appSettingIcons/rightArrow.png')} resizeMode="cover"
                                        />
                                    </View>


                                </View>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback onPress={() => this.signOutPress()}>
                                <View style={[styles.rowView, { justifyContent: 'space-between' }]}>
                                    <View style={{ justifyContent: "center", marginLeft: 15 }}>
                                        <Text style={[styles.textInputText, { color: "red" }]}>Sign Out</Text>
                                    </View>


                                    <TouchableWithoutFeedback >
                                        <View style={styles.rightArrowView}>
                                            <Image source={require('../images/appSettingIcons/rightArrow.png')} resizeMode="cover"
                                            />
                                        </View>
                                    </TouchableWithoutFeedback>

                                </View>
                            </TouchableWithoutFeedback>
                        </ScrollView>

                    </View>
                </TouchableWithoutFeedback>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    safeArea: {
        flex: 1,
        backgroundColor: '#21459E'
    },
    container: { flex: 1, backgroundColor: '#fff', },
    imageRelativeView: { width: width, height: 50, backgroundColor: "#2A56C6", },
    imageViewExternal: { width: width / 3, height: width / 3, borderRadius: (width / 3) / 2, backgroundColor: "white", alignSelf: "center", marginTop: -60, borderColor: "#fff", borderWidth: 1 },
    imageViewInternal: { width: (width / 3) - 8, height: (width / 3) - 8, borderRadius: ((width / 3) - 8) / 2, backgroundColor: "#21459E", alignSelf: "center", borderColor: "#21459E", marginTop: 2, borderWidth: 2 },
    userImage: { height: (width / 3) - 12, width: (width / 3) - 12, alignSelf: 'center', borderRadius: ((width / 3) - 12) / 2 },
    plusView: { height: 30, width: 30, borderRadius: 15, alignSelf: "flex-end", marginTop: Platform.OS == 'ios' ? -25 : -30 },
    detailContainer: { width: width, marginTop: 10 },
    rowView: { height: 50, width: width, flexDirection: "row", borderBottomColor: '#E1E2E4', borderBottomWidth: 1 },
    textInputText: { fontSize: fontSizes.font14, color: "#000" },
    textInputView: { flex: 3, marginHorizontal: 10 },
    textInput: { height: 50, textAlign: 'right' },
    editView: { flex: .5, marginRight: 15, alignItems: "flex-end", justifyContent: "center" },
    rightArrowView: { marginRight: 15, alignItems: "flex-end", justifyContent: "center" }

});
