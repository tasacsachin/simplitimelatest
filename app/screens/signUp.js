/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet, StatusBar, Keyboard, TouchableWithoutFeedback,
    Text, SafeAreaView, TouchableOpacity,
    View, Image, TextInput, ScrollView, Alert,
} from 'react-native';
import { GoogleSignin, GoogleSigninButton } from 'react-native-google-signin';
import Spinner from '../components/spinner'
import { height, width, fontSizes } from '../utils/utils'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import webservice from '../components/webService'
const FBSDK = require('react-native-fbsdk');

import GoogleLoginAndroid from './GoogleLoginButton'
const {
    LoginManager,
    AccessToken,
    LoginButton
} = FBSDK;
//import { LoginManager,LoginButton,AccessToken,GraphRequest,GraphRequestManager} from 'react-native-fbsdk';
import { StackActions, NavigationActions } from 'react-navigation';
const emailRegex = (/^[A-Z0-9_]+([\.][A-Z0-9_]+)*@[A-Z0-9-]+(\.[a-zA-Z]{2,4})+$/i)
export default class Signup extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            companyId: '',
            individual: true,
            company: false,
            fullName: '',
            confirmPassword: '',
            companyName: '',
            marginFromBottom: 10,
            keyboardOpen: false,
            spinnerVisible: false

        }
    }
    componentDidMount() {
        if (Platform.OS == 'ios') {
            GoogleSignin.configure({
                iosClientId: '402046120924-1k2da9aq2ijig3cc44nc1hdk78ua88c4.apps.googleusercontent.com', // only for iOS
            })
        }

        if (Platform.OS == 'ios') {
            this.keyboardDidShowListener = Keyboard.addListener(
                "keyboardWillShow",
                () => this.setState({ keyboardOpen: true })
            );
            this.keyboardDidHideListener = Keyboard.addListener(
                "keyboardWillHide",
                () => this.setState({ keyboardOpen: false })
            );
        } else {
            this.keyboardDidShowListener = Keyboard.addListener(
                "keyboardDidShow",
                () => this.setState({ keyboardOpen: true, marginFromBottom: this.state.company ? 55 : 10 })
            );
            this.keyboardDidHideListener = Keyboard.addListener(
                "keyboardDidHide",
                () => this.setState({ keyboardOpen: false, marginFromBottom: 10 })
            );
        }
    }
    iOSGoogleSignIn = async () => {
        try {
            const userInfo = await GoogleSignin.signIn();
            alert(JSON.stringify(userInfo))
            this.signOut()
            //this.setState({ userInfo });
        } catch (error) {
            alert("Login Cancelled")
            // if (error.code === statusCodes.SIGN_IN_CANCELLED) {
            //     // user cancelled the login flow
            //     alert("Login Cancelled")
            // } else if (error.code === statusCodes.IN_PROGRESS) {
            //     alert("Already Login")
            //     // operation (f.e. sign in) is in progress already
            // } else {
            //     alert("Something went wrong")
            //     // some other error happened
            // }
        }
    };
    signOut = async () => {
        try {
            await GoogleSignin.revokeAccess();
            await GoogleSignin.signOut();
            this.setState({ user: null }); // Remember to remove the user from your app's state as well
        } catch (error) {
            console.error(error)
        }
    };
    googlePlus() {
        if (Platform.OS == 'ios') {
            this.iOSGoogleSignIn()
        } else {

        }
    }
    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }
    onChange(text, type) {
        //alert(text + "==" + type)
        switch (type) {
            case 'Email': this.setState({ email: text }); break;
            case 'Password': this.setState({ password: text }); break;
            case 'Company ID': this.setState({ companyId: text }); break;
            case 'Full Name': this.setState({ fullName: text }); break;
            case 'Confirm Password': this.setState({ confirmPassword: text }); break;
            case 'Company Name': this.setState({ companyName: text }); break;

        }
    }

    titleNamefunction(text) {
        return (
            <Text style={[styles.titleText, { marginHorizontal: 15, marginLeft: 20 }]}>
                {text}
            </Text>
        )
    }
    focusOnNExtTextInput(next, placeholder) {
        if (this.state.company) {
            next != null ?
                this.refs[next].focus()
                : Keyboard.dismiss()
        } else {
            if (next == 'Company Name') {
                this.refs["Password"].focus()
            } else {
                next != null ?
                    this.refs[next].focus()
                    : Keyboard.dismiss()
            }

        }
        //this.refs[next].focus()
        //alert(next)
    }
    textField(placeholder, value, next) {
        return (
            <View style={{ borderBottomColor: "#D8D8D8", borderBottomWidth: 1, marginHorizontal: 15 }}>
                <TextInput
                    style={styles.textInput}
                    placeholder={placeholder}
                    value={value}
                    ref={placeholder}
                    returnKeyType={placeholder != "Confirm Password" ? 'next' : "done"}
                    onSubmitEditing={() => this.focusOnNExtTextInput(next, placeholder)}
                    placeholderTextColor="#333333"
                    underlineColorAndroid="transparent"
                    maxLength={placeholder == "Company ID" ? 5 : 100}
                    secureTextEntry={placeholder == "Password" || placeholder == "Confirm Password" ? true : false}
                    onChangeText={(text) => this.onChange(text, placeholder)}
                //value={ProjectCodeValue}
                />
            </View>
        )
    }
    socialLoginView() {
        return (
            <View style={{ marginTop: 5, justifyContent: "center" }}>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <View style={{ width: (width / 2) - 20, height: 1, backgroundColor: "#C6C6C6" }} />
                    <Text style={{ color: '#898989', fontSize: fontSizes.font14, marginHorizontal: 5 }}>
                        Or
                    </Text>
                    <View style={{ width: (width / 2) - 20, height: 1, backgroundColor: "#C6C6C6" }} />
                </View>
                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "center", }}>
                    <TouchableOpacity onPress={() => this.fabebook()}>
                        <Image source={require('../images/loginSignup/fbIcon.png')} style={{ margin: 15 }} />
                    </TouchableOpacity>
                    {
                        Platform.OS == 'ios' ?
                            <TouchableOpacity onPress={() => this.googlePlus()}>
                                <Image source={require('../images/loginSignup/googleIcon.png')} style={{ margin: 15 }} />
                            </TouchableOpacity>
                            :
                            <GoogleLoginAndroid
                                onLogin={
                                    (result) => {
                                        console.log('Google onLogin')
                                        if (result.message) {
                                            alert('error:  ' + result.message)
                                        } else {
                                            alert(JSON.stringify(result))
                                            //alert('Login was successful' + result.name + ' — ' + result.email)
                                        }
                                    }
                                }
                                onLogout={() => alert('logged out')}
                                onError={
                                    (result) => {
                                        if (result.error) {
                                            alert('error: ' + result.error)
                                        } else {
                                            alert('error')
                                        }
                                    }
                                }
                            />
                    }


                </View>
            </View>
        )
    }
    popUp(msg) {
        alert(msg)
    }
    onSignup() {

        if (this.state.individual ? this.state.fullName == '' : false) {
            this.popUp("Please enter full name.")
        } else if (this.state.email == "") {
            this.popUp("Please enter email.")
        }
        else if (!emailRegex.test(this.state.email)) {
            this.popUp("Please enter valid email.")
        }
        else if (this.state.company ? this.state.companyName == '' : false) {
            this.popUp("Please enter company name.")
        }
        else if (this.state.company ? this.state.companyId == '' : false) {
            this.popUp("Please enter company id.")
        }
        else if (this.state.company ? this.state.companyId.length != 5 : false) {
            this.popUp("Company id should be of 5 characters.")
        }
        else if (this.state.password == "") {
            this.popUp("Please enter password.")

        } else if (this.state.confirmPassword == "") {
            this.popUp("Please enter confirm password.")
        }
        else if (this.state.password != this.state.confirmPassword) {
            this.popUp("Both password should be same.")
        } else {
            this.setState({ spinnerVisible: true })
            var usertype = this.state.company ? "company" : "individual"
            let variables = new FormData();
            if (this.state.individual) {
                variables.append("fullName", this.state.fullName)
                variables.append("email", this.state.email)
            } else {
                variables.append("companyName", this.state.companyName)
                variables.append("companyEmail", this.state.email)
                variables.append("companyCode", this.state.companyId)
            }
            variables.append("password", this.state.password)
            variables.append("userType", usertype)
            return webservice(variables, "signup", 'POST')
                .then(response => {
                    this.setState({ spinnerVisible: false })
                    if (response != "error") {
                        setTimeout(() => {
                            Alert.alert(
                                'Verify Email',
                                'Thanks for signing up. Please check your email to verify your account',
                                [
                                    { text: 'I have verified', onPress: () => this.navigateToLogin() },
                                ],
                                { cancelable: false }
                            )
                        }, 300)


                    }

                })
        }


    }
    navigateToLogin() {
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Login' })],
        });
        this.props.navigation.dispatch(resetAction);
    }
    fabebook() {
        LoginManager.logInWithReadPermissions(['public_profile', 'email']).then(
            function (result) {
                if (result.isCancelled) {
                    alert('Login cancelled');
                } else {
                    // alert('Login success with permissions: '

                    AccessToken.getCurrentAccessToken().then(
                        (data) => {
                            //alert(data.accessToken.toString())
                            fetch('https://graph.facebook.com/v2.5/me?fields=email,name,picture&access_token=' + data.accessToken)
                                .then((response) => { return response.json() })
                                .then((json) => {
                                    alert(JSON.stringify(json))
                                    // Some user object has been set up somewhere, build that user here
                                    // user.name = json.name
                                    // user.id = json.id
                                    // user.user_friends = json.friends
                                    // user.email = json.email
                                    // user.username = json.name
                                    // user.loading = false
                                    // user.loggedIn = true
                                    // user.avatar = setAvatar(json.id)
                                })
                                .catch(() => {
                                    reject('ERROR GETTING DATA FROM FACEBOOK')
                                })



                        }
                    )


                }
            },
            function (error) {
                alert('Login fail with error: ' + error);
            }
        );
    }
    render() {
        return (
            <SafeAreaView
                style={styles.safeArea}>
                <Spinner visible={this.state.spinnerVisible} />


                {Platform.OS == 'ios' ? <View style={styles.iOSTopBar}>
                </View> : <StatusBar
                        backgroundColor="#21459E"
                        barStyle="light-content"
                    />}

                <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>

                    <View style={styles.container}>
                        <View style={{ height: (height / 3) - 20, backgroundColor: 'transparent', alignItems: "center", }} >
                            <Image source={require('../images/loginSignup/Simplitime.png')} style={{ marginTop: (height / 14) }} />
                        </View>
                        <View style={{ height: (Platform.OS === 'ios' && height === 812) ? (((2 * height) / 3) - 57) : (((2 * height) / 3)), backgroundColor: '#E0E0E0', justifyContent: "flex-end", alignItems: "center" }} >
                            <View style={{ flexDirection: 'row', paddingBottom: (Platform.OS === 'ios' && height === 812) ? 10 : 30, }}>
                                <Text style={[styles.titleText, { color: '#898989', textAlign: 'right', fontSize: fontSizes.font16, marginTop: 20 }]}>
                                    Already have an account?
                                    </Text>
                                <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                                    <Text style={[styles.titleText, { color: '#2A56C6', textAlign: 'right', fontSize: fontSizes.font16, marginTop: 20 }]}>
                                        {" Log in"}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View
                            //style={{ paddingBottom: this.state.keyboardOpen ? 200 : 0, backgroundColor: "red" }}
                            style={[styles.card, {
                                top: this.state.keyboardOpen ? 10 : ((Platform.OS === 'ios' && height === 812) ? (height / 5.5) : (height / 6)),
                                maxHeight: (Platform.OS === 'ios' && height === 812) ? ((2 * height) / 3) : ((2 * height) / 3) + 20,
                                paddingBottom: this.state.keyboardOpen ? 100 : 0,
                            }]}
                            keyboardShouldPersistTaps="always">
                            {/* <View
                            // style={[styles.card, {
                            //     top: this.state.keyboardOpen ? 10 : ((Platform.OS === 'ios' && height === 812) ? (height / 5.5) : (height / 6)),
                            //     height: (Platform.OS === 'ios' && height === 812) ? ((2 * height) / 3) : ((2 * height) / 3) + 20
                            // }]}
                            > */}



                            {/* <KeyboardAwareScrollView

                                keyboardShouldPersistTaps="always"> */}


                            {/* company individual tab */}
                            <View style={{ flexDirection: 'row', marginTop: 5, padding: 15 }}>
                                <TouchableOpacity style={{ flexDirection: "row" }}
                                    onPress={() => this.setState({ individual: true, company: false, password: '', confirmPassword: '' })}>
                                    <Image source={this.state.individual ? require('../images/loginSignup/radioCheck.png') : require('../images/loginSignup/radioUncheck.png')} style={{ marginTop: Platform.OS == 'ios' ? 2 : 5 }} />
                                    <Text style={[styles.titleText, { color: this.state.individual ? '#2A56C6' : '#898989', marginTop: 0, marginLeft: 15 }]}>
                                        Individual
                                        </Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ flexDirection: "row" }}
                                    onPress={() => this.setState({ individual: false, company: true, password: '', confirmPassword: '' })}>
                                    <Image source={this.state.company ? require('../images/loginSignup/radioCheck.png') : require('../images/loginSignup/radioUncheck.png')} style={{ marginLeft: 15, marginTop: Platform.OS == 'ios' ? 2 : 5 }} />
                                    <Text style={[styles.titleText, { color: this.state.company ? '#2A56C6' : '#898989', marginTop: 0, marginLeft: 15 }]}>
                                        Company Admin
                                        </Text>
                                </TouchableOpacity>
                            </View>




                            <View style={{ width: width, height: 3, backgroundColor: "#2A56C6" }} />
                            <ScrollView 
                            //contentContainerStyle={{ flexGrow: 1, alignSelf: 'stretch' }}
                                //style={{ paddingBottom: this.state.keyboardOpen ? 100 : 0 }}

                                keyboardShouldPersistTaps="always">

                            <View style={{ width: '100%' }}>
                                {
                                    this.state.individual ? this.titleNamefunction("Full Name") : null
                                }
                                {
                                    this.state.individual ? this.textField("Full Name", this.state.fullName, "Email") : null
                                }
                            </View>
                            <View style={{ width: '100%' }}>
                                {this.titleNamefunction("Email")}
                                {this.textField("Email", this.state.email, 'Company Name')}
                            </View>
                            <View style={{ width: '100%' }}>
                                {
                                    this.state.company ? this.titleNamefunction("Company Name") : null
                                }
                                {
                                    this.state.company ? this.textField("Company Name", this.state.companyName, 'Company ID') : null
                                }
                            </View>
                            <View style={{ width: '100%' }}>
                                {
                                    this.state.company ? this.titleNamefunction("Company ID") : null
                                }
                                {
                                    this.state.company ? this.textField("Company ID", this.state.companyId, "Password") : null
                                }
                            </View>
                            <View style={{ width: '100%' }}>
                                {this.titleNamefunction("Password")}
                                {this.textField("Password", this.state.password, "Confirm Password")}
                            </View>
                            <View style={{ width: '100%' }}>
                                {this.titleNamefunction("Confirm Password")}
                                {this.textField("Confirm Password", this.state.confirmPassword, null)}
                            </View>
                            {/* </ScrollView> */}
                            <TouchableWithoutFeedback onPress={() => this.onSignup()}>
                                <View style={[styles.buttonStyle, { marginBottom: this.state.marginFromBottom }]} >
                                    <Text style={[styles.titleText, { color: 'white', textAlign: 'center', fontSize: fontSizes.font16, marginTop: 0, }]}>
                                        SIGN UP
                                                </Text>
                                </View>
                            </TouchableWithoutFeedback>
                            {/* {
                                this.state.individual ?
                                    this.socialLoginView() : null
                            } */}

                            {/* </KeyboardAwareScrollView> */}


                            {/* </View> */}
                            </ScrollView>
                        </View>
                    </View>

                </TouchableWithoutFeedback>






            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center',
        //alignItems: 'flex-start',
        backgroundColor: '#2A56C6',
    },
    iOSTopBar: {
        backgroundColor: '#21459E',
    },
    safeArea: {
        flex: 1,
        backgroundColor: '#21459E'
    },
    card: {
        position: "absolute",
         width: width - 20,
        left: 10, right: 10, 
        backgroundColor: "#fff",

        elevation: 5,
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: .5,
        borderRadius: 3
    },

    titleText: { fontSize: fontSizes.font14, color: "#898989", marginTop: 15, marginLeft: 5 },
    textInput: { height: 45, width: "85%", color: '#333333', fontSize: fontSizes.font14, marginTop: 5, marginLeft: 5 },
    buttonStyle: { backgroundColor: "#2A56C6", height: 50, alignItems: "center", justifyContent: 'center', marginTop: 20, borderRadius: 3, marginHorizontal: 15, }

});
