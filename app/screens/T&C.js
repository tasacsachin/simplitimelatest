// /**
//  * Sample React Native App
//  * https://github.com/facebook/react-native
//  * @flow
//  */

// import React, { Component } from 'react';
// import {
//     Platform,
//     StyleSheet, StatusBar, Keyboard, TouchableWithoutFeedback,
//     Text, SafeAreaView, TouchableOpacity,
//     View, Image, TextInput, ScrollView
// } from 'react-native';
// import { height, width, fontSizes } from '../utils/utils'


// export default class termsConditions extends Component {
//     static navigationOptions = {
//         drawerLabel: 'T&C',
//         drawerIcon: ({ tintColor }) => (
//             <Image source={require('../images/drawerIcons/t&cIcon.png')} />
//         ),
//     };
//     onChange() {

//     }
//     render() {
//         return (
//             <SafeAreaView
//                 style={styles.safeArea}>


//                     <ScrollView keyboardShouldPersistTaps="always" scrollsToTop={false}>
//                     <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
//                         <View style={styles.container}>


//                             {Platform.OS == 'ios' ? <View style={styles.iOSTopBar}>
//                             </View> : <StatusBar
//                                     backgroundColor="#21459E"
//                                     barStyle="light-content"
//                                 />}

//                             <View style={styles.mainContainer}>

//                                 <Text style={styles.titleText}>
//                                     Email
//                                 </Text>
//                                 <TextInput
//                                     style={styles.textInput}
//                                     placeholder="Email"
//                                     placeholderTextColor="#6893FF"

//                                     underlineColorAndroid="transparent"
//                                     onChangeText={() => this.onChange()}
//                                 //value={ProjectCodeValue}
//                                 />
//                                 <Text style={styles.titleText}>
//                                     Password
//                                 </Text>
//                                 <TextInput
//                                     style={styles.textInput}
//                                     placeholder="Password"

//                                     placeholderTextColor="#6893FF"
//                                     underlineColorAndroid="transparent"
//                                     onChangeText={() => this.onChange()}
//                                 //value={ProjectCodeValue}
//                                 />
//                                 <Text style={styles.titleText}>
//                                     Company Id
//                                 <Text style={[styles.titleText, { fontSize: fontSizes.font14 }]}>
//                                         {" (If Applicable)"}
//                                     </Text></Text>
//                                 <TextInput
//                                     style={styles.textInput}
//                                     placeholder="Company Id"

//                                     placeholderTextColor="#6893FF"
//                                     underlineColorAndroid="transparent"
//                                     onChangeText={() => this.onChange()}
//                                 //value={ProjectCodeValue}
//                                 />

//                                 <TouchableOpacity style={styles.buttonStyle}>
//                                 <Text style={styles.titleText}>
//                                     Login
//                                 </Text>
//                                 </TouchableOpacity>
//                                 <View style={styles.rowView}>
//                                 <Text style={[styles.titleText,{fontSize:fontSizes.font14}]}>
//                                     {'New User?  '}
//                                 </Text>
//                                 <TouchableOpacity style={{borderBottomColor:'white',borderBottomWidth:1,}}>
//                                 <Text style={[styles.titleText,{fontSize:fontSizes.font14}]}>
//                                     SignUp 
//                                 </Text>
//                                 </TouchableOpacity>
//                                 </View>
//                             </View>

//                         </View>
//                         </TouchableWithoutFeedback>
//                     </ScrollView>

//             </SafeAreaView>
//         );
//     }
// }

// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         justifyContent: 'center',
//         alignItems: 'center',
//         backgroundColor: '#21459E',
//     },
//     iOSTopBar: {
//         backgroundColor: '#21459E',
//     },
//     safeArea: {
//         flex: 1,
//         backgroundColor: '#21459E'
//     },
//     mainContainer: { width: width - 80, backgroundColor: "transparent", justifyContent: "center",height:height-20 },
//     titleText: { fontSize: fontSizes.font18, color: "white" },
//     textInput: { height: 45, borderRadius: 8, borderColor: "white", borderWidth: 1, marginVertical: 10, paddingLeft: 10, color: '#21459E', backgroundColor: "white" },
//     buttonStyle: { height: 45,backgroundColor:"#21459E",borderRadius: 8,marginTop:20,borderColor:"white",borderWidth:2,alignItems:"center",justifyContent:"center"},
//     rowView:{flexDirection:"row",alignItems:"center",justifyContent:"center",marginTop:5}
// });





/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform, PanResponder,
    StyleSheet, Picker, ScrollView,
    Text, SafeAreaView, TouchableOpacity,
    View, Image, WebView, ActivityIndicator
} from 'react-native';
import { height, width, fontSizes } from '../utils/utils'
import { dateConverter, week_Date_Array, year_Array, dateConverterUseInWeekTab, dateConverterUseInHeader, hourMinuteConverter } from '../components/dateConverter'
import Header from '../components/commonHeader'
import Spinner from '../components/spinner'
export default class activityList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            index: 26,
            globalLeft: '',
            spinnerVisible: false
        }

    }
    componentWillMount() {
        this.setState({ spinnerVisible: true })
        setTimeout(() => {
            this.setState({ spinnerVisible: false })
        }, 5000)

    }

    render() {

        return (
            <SafeAreaView
                style={styles.safeArea}>
                <View style={styles.container}>
                    <Header HeaderLeftText="Terms & Conditions"
                        //HeaderRightText={this.state.datePassedToHeader != '' ? this.state.datePassedToHeader : today_To_day_AfterEighth_Day()}
                        //type="date"
                        headerType='back'
                        toggleDrawer={() => this.props.navigation.goBack()}
                    //headerLeftIcon={require('../images/headerIcons/downArrow.png')}
                    // headerRightIcon={require('../images/analysisExportIcons/dateIcon.png')}
                    //headerRightIconOnPress={() => this.showStartDatePicker()} 
                    />
                    {
                        this.state.spinnerVisible ?
                            <View style={{ height: height, width: width }}>
                                <ActivityIndicator animating={this.state.spinnerVisible} size="large" color="#2A56C6" style={{ marginTop: 20 }} />
                            </View>
                            : null
                    }

                    <WebView
                        source={{ uri: 'https://github.com/facebook/react-native' }}
                        onLoad={() => this.setState({ spinnerVisible: false })}
                    />


                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    safeArea: {
        flex: 1,
        backgroundColor: '#21459E'
    },
    container: { flex: 1, backgroundColor: '#fff', },
    tabStyle: { alignItems: "center", justifyContent: "center", width: width / 3, height: 35 },
    tabText: { fontSize: fontSizes.font12, color: "white", textAlign: 'center', },
});


















// <View style={{ flexDirection: 'row', marginTop: 5, padding: 15 }}>
//                                 <TouchableOpacity style={{ flexDirection: "row" }}
//                                     onPress={() => this.setState({ individual: true, company: false, password: '', confirmPassword: '' })}>
//                                     <Image source={this.state.individual ? require('../images/loginSignup/radioCheck.png') : require('../images/loginSignup/radioUncheck.png')} style={{ marginTop: Platform.OS == 'ios' ? 2 : 5 }} />
//                                     <Text style={[styles.titleText, { color: this.state.individual ? '#2A56C6' : '#898989', marginTop: 0, marginLeft: 15 }]}>
//                                         Individual
//                                         </Text>
//                                 </TouchableOpacity>
//                                 <TouchableOpacity style={{ flexDirection: "row" }}
//                                     onPress={() => this.setState({ individual: false, company: true, password: '', confirmPassword: '' })}>
//                                     <Image source={this.state.company ? require('../images/loginSignup/radioCheck.png') : require('../images/loginSignup/radioUncheck.png')} style={{ marginLeft: 15, marginTop: Platform.OS == 'ios' ? 2 : 5 }} />
//                                     <Text style={[styles.titleText, { color: this.state.company ? '#2A56C6' : '#898989', marginTop: 0, marginLeft: 15 }]}>
//                                         Company Admin
//                                         </Text>
//                                 </TouchableOpacity>
//                             </View>




//                             <View style={{ width: width, height: 3, backgroundColor: "#2A56C6" }} />
                            

//                             <View style={{ width: '100%' }}>
//                                 {
//                                     this.state.individual ? this.titleNamefunction("Full Name") : null
//                                 }
//                                 {
//                                     this.state.individual ? this.textField("Full Name", this.state.fullName, "Email") : null
//                                 }
//                             </View>
//                             <View style={{ width: '100%' }}>
//                                 {this.titleNamefunction("Email")}
//                                 {this.textField("Email", this.state.email, 'Company Name')}
//                             </View>
//                             <View style={{ width: '100%' }}>
//                                 {
//                                     this.state.company ? this.titleNamefunction("Company Name") : null
//                                 }
//                                 {
//                                     this.state.company ? this.textField("Company Name", this.state.companyName, 'Company ID') : null
//                                 }
//                             </View>
//                             <View style={{ width: '100%' }}>
//                                 {
//                                     this.state.company ? this.titleNamefunction("Company ID") : null
//                                 }
//                                 {
//                                     this.state.company ? this.textField("Company ID", this.state.companyId, "Password") : null
//                                 }
//                             </View>
//                             <View style={{ width: '100%' }}>
//                                 {this.titleNamefunction("Password")}
//                                 {this.textField("Password", this.state.password, "Confirm Password")}
//                             </View>
//                             <View style={{ width: '100%' }}>
//                                 {this.titleNamefunction("Confirm Password")}
//                                 {this.textField("Confirm Password", this.state.confirmPassword, null)}
//                             </View>
                            
//                             <TouchableWithoutFeedback onPress={() => this.onSignup()}>
//                                 <View style={[styles.buttonStyle, { marginBottom: this.state.marginFromBottom }]} >
//                                     <Text style={[styles.titleText, { color: 'white', textAlign: 'center', fontSize: fontSizes.font16, marginTop: 0, }]}>
//                                         SIGN UP
//                                                 </Text>
//                                 </View>
//                             </TouchableWithoutFeedback>
//                             {
//                                 this.state.individual ?
//                                     this.socialLoginView() : null
//                             }
