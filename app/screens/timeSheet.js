
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet, AppState, InteractionManager, PanResponder,
  Text, SafeAreaView, Keyboard, TouchableWithoutFeedback,
  View, Image, ScrollView, TouchableOpacity, FlatList,
  DeviceEventEmitter, AsyncStorage, PushNotificationIOS,
  NativeAppEventEmitter,
} from 'react-native';

var globalLeft = ''
var PushNotification = require('react-native-push-notification');


import Header from '../components/commonHeader'
import FloatingButton from '../components/floatingButton'
import LeaveModal from '../components/leaveModal'
import SelectProjectModal from '../components/selectProjectModal'
import PickerDropdownModal from '../components/pickerDropdownModal'
import BackgroundTimer from 'react-native-background-timer';
import RNAndroidBackgroundTimer from 'react-native-android-background-timer';
import TimeLeaveTab from '../components/timesheetLeaveTab'
import { height, width, fontSizes, getLog } from '../utils/utils'
import webservice from '../components/webService'
import Spinner from '../components/spinner'
const monthArr = ['', '', "January", "Feburary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December", '', '']
import { dateConverter, week_Date_Array, year_Array, dateConverterUseInWeekTab, dateConverterUseInHeader, hourMinuteConverter } from '../components/dateConverter'
//var interval
var keyboardOpen = false
//alert(JSON.stringify(week_Date_Array))
const data = [
  1, 2, 3, 4, 5, 6, 7, 8
]
//var intervalId;
var backgroundSecond = 0
//getLog(JSON.stringify(week_Date_Array))
export default class timeSheet extends Component {
  constructor(props) {

    super(props);
    this.state = {
      week_Date_Array: week_Date_Array,
      globalLeft: '',
      appState: AppState.currentState,
      second: 0,
      minute: 0,
      TabPress: true,
      projectList: [],
      selectProjectModal: false,
      modalVisible: false,
      datePickerVisible: false, leaveType: 'Leave Type',
      leaveList: [],

      totalProjectHours: 0, totalLeaveDays: 0, totalDays: 0,
      returnCallBack: { value: false, callBack: () => { } },
      leaveTabCallBack: { value: false, callBack: () => { } },
      callbackPassedToProjectBreakdown: { value: false, callBack: () => { } },
      startDate: '', endDate: '', datePickerType: '', minDate: '', startDateToShowOnMOdal: '', endDateToShowOnModal: '',
      pickerDropdownModalVisible: false,
      selectProject: "Select Project",
      selectedProjectId: "",
      week: "Week",
      pickerDropdownModalTitleText: '',
      pickerDropdownModalListData: [],
      leaveTypeOnLeaveModal: 'Leave Type',
      leaveTypeList: [],
      leaveTypeListPassToModal: [],
      playPauseImage: '',
      play: false,
      playPauseText: '',
      stopImage: '',
      //week_Date_Array: week_Date_Array,
      week_Date_Array_Middle_Index: 24,
      weekArray: [],
      leaveModalErrorText: "", reasonTextInputData: '',
      spinnerVisible: false,
      userToken: '', projectListPassedToSelectProjectDropdown: [],
      projectListContainAllProjectOfSelectProjectDropdown: [],
      filterDataPassedOnProjectBreakdown: '',
      globalLeft: '',
      selectedProjectItem: '',



      arrayPassedToTabScrollView: week_Date_Array,


    }
    PushNotification.configure({
      // (optional) Called when Token is generated (iOS and Android)
      onRegister: function (token) {
        console.log('TOKEN:', token);
      },

      // (required) Called when a remote or local notification is opened or received
      onNotification: function (notification) {
        console.log('NOTIFICATION:', notification);

        // process the notification

        // required on iOS only (see fetchCompletionHandler docs: https://facebook.github.io/react-native/docs/pushnotificationios.html)
        notification.finish(PushNotificationIOS.FetchResult.NoData);
      },

      // ANDROID ONLY: GCM or FCM Sender ID (product_number) (optional - not required for local notifications, but is need to receive remote push notifications)
      //senderID: "YOUR GCM (OR FCM) SENDER ID",

      // IOS ONLY (optional): default: all - Permissions to register.
      permissions: {
        alert: true,
        badge: true,
        sound: true
      },

      // Should the initial notification be popped automatically
      // default: true
      popInitialNotification: true,

      /**
      * (optional) default: true
      * - Specified if permissions (ios) and token (android and ios) will requested or not,
      * - if not, you must call PushNotificationsHandler.requestPermissions() later
      */
      requestPermissions: true,
      // onNotification: (notification) => {
      //   this.props.navigation.navigate('Timesheet');
      // },
    })


  }
  static navigationOptions = {
    // drawerLabel: 'Timesheet',
    // drawerIcon: ({ tintColor }) => (
    //   <Image style={{ tintColor: tintColor }}

    //     source={require('../images/drawerIcons/timesheetIcon.png')} />
    // ),
    drawerLabel: () => global.userType == 'Admin' ? null : 'Timesheet',

    drawerIcon: ({ tintColor }) => global.userType == 'Admin' ? null : (
      <Image style={{ tintColor: tintColor }}
        source={require('../images/drawerIcons/timesheetIcon.png')}
      />
    ),
  };


  // test for week scroll view






  //


  hitAllAPI() {
    this.setState({ spinnerVisible: true })
    setTimeout(() => {
      this.setState({ spinnerVisible: false })
    }, 7000)
    AsyncStorage.getItem('userToken').then((token) => {
      this.setState({ userToken: token })
      this.hitGetAllLeaveTypesAPI(token)
      //this.hitGetLeaveListAPI(token,"week")
      //getLeaveListAPI is called in makeWeekDataTabArray
      //this.makeWeekDateTabArray(token)
      this.hitGetAllProjectListApi(token)







    })

  }
  componentWillMount() {
    //this.setState({ spinnerVisible: true })
    //this.makeWeekDateTabArray(this.state.week_Date_Array_Middle_Index)
    this.updateSecondsOfTimerWhenControllerComesToThisScreen()
    this.hitAllAPI()
    // this.makeWeekDateTabArray(this.state.week_Date_Array_Middle_Index)
    getLog(this.state.appState)
    // var projectArr = [
    //   { "projectName": "ProjectXYZ asd asd asd asd asdasdasdasdsadadasdadasd ", 'projectId': 1, 'Activities': 2, 'progress': 80, 'time': 1874 },
    //   { "projectName": "ProjectXYZ", 'projectId': 2, 'Activities': 5, 'progress': 40, 'time': 200 },
    //   { "projectName": "ProjectXYZ", 'projectId': 3, 'Activities': 6, 'progress': 60, 'time': 120 },
    //   { "projectName": "ProjectXYZ", 'projectId': 4, 'Activities': 2, 'progress': 10, 'time': 140 },
    //   { "projectName": "ProjectXYZ", 'projectId': 5, 'Activities': 2, 'progress': 80, 'time': 145 },
    //   { "projectName": "ProjectXYZ", 'projectId': 6, 'Activities': 5, 'progress': 40, 'time': 330 },
    //   { "projectName": "ProjectXYZ", 'projectId': 7, 'Activities': 6, 'progress': 60, 'time': 485 },
    //   { "projectName": "ProjectXYZ", 'projectId': 8, 'Activities': 2, 'progress': 10, 'time': 55 },


    // ]
    // var leaveArr = [
    //   { "validityFrom": new Date().getTime(), "validityTo": new Date().getTime(), 'type': "Sick Leave", 'status': "Approved", 'totalDays': 1, "reason": "These are the commen adsjhas adhkjhsad asdhbkjsa asdhas" },
    //   { "validityFrom": new Date().getTime(), "validityTo": new Date().getTime(), 'type': "Planned Leave", 'status': "Pending", 'totalDays': 1, "reason": "These are the commen adsjhas adhkjhsad asdhbkjsa asdhas" },
    //   { "validityFrom": new Date().getTime(), "validityTo": new Date().getTime(), 'type': "Sick Leave", 'status': "Not Approved", 'totalDays': 1, "reason": "These are the commen adsjhas adhkjhsad asdhbkjsa asdhas" },


    // ]
    var days = 0
    var leaveTypeArr = []

    if (this.props.id) {
      this.props.id == 'leaves' ? this.setState({ TabPress: false }) : null
    }
    // this.totalHoursMinutesOfProjects()
    this.setState({ playPauseImage: this.state.second == 0 ? require('../images/timesheetIcon/playIcon.png') : require('../images/timesheetIcon/resumeIcon.png') })
    this.setState({ playPauseText: this.state.second == 0 ? 'Start' : 'Resume' })
    this.setState({ stopImage: this.state.second == 0 ? require('../images/timesheetIcon/greyStopIcon.png') : require('../images/timesheetIcon/redStopIcon.png') })

    //this.setState({ projectList: projectArr, })
    this.setState({ returnCallBack: { value: false, callBack: this.callBackCalledOnActivityAdd } })
    this.setState({ leaveTabCallBack: { value: false, callBack: this.callBackCalledOnEditDeleteLeave } })
    this.setState({ callbackPassedToProjectBreakdown: { value: false, callBack: this.callbackCalledOnChangesOnProjectBreakdown } })

    this.setState({ week_Date_Array: week_Date_Array })

    //this.updateSecondsOfTimerWhenControllerComesToThisScreen()
    this.function()






    this.wrapperPanResponder = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,

      onPanResponderEnd: (e, { vx, dx }) => {

        console.log("end" + dx)
        if (dx < 0) {
          this.setState({ globalLeft: "right" })
          if (this.state.week == 'Month') {
            if (this.state.week_Date_Array_Middle_Index < 11) {
              this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index + 1 })
              setTimeout(() => {
                this.scrollToPosition(this.state.userToken)
              }, 300)
            }
          } else {
            if (this.state.week_Date_Array_Middle_Index < 49) {
              this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index + 1 })
              setTimeout(() => {
                this.scrollToPosition(this.state.userToken)
              }, 300)
            }
          }

          //alert("right")
        } else {
          this.setState({ globalLeft: "left" })
          if (this.state.week == 'Month') {



            if (this.state.week_Date_Array_Middle_Index > 0) {
              this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index - 1 })
              setTimeout(() => {
                this.scrollToPosition(this.state.userToken)
              }, 300)

            }
            //else {
            //     //alert(this.state.week_Date_Array_Middle_Index)
            //     if (this.state.week_Date_Array_Middle_Index > -1) {
            //         this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index - 1 })
            //         this.scroller.scrollTo({ x: 0, y: 0 })
            //         setTimeout(() => {
            //             this.hitGetLeaveListAPI(this.state.userToken, "month", this.state.week_Date_Array_Middle_Index + 2)
            //             this.hitGetProjectListAPI(this.state.userToken, "month", this.state.week_Date_Array_Middle_Index + 2)
            //         })
            //     }

            // }
          } else {
            if (this.state.week_Date_Array_Middle_Index > 1) {
              this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index - 1 })
              setTimeout(() => {
                this.scrollToPosition(this.state.userToken)
              }, 300)
            }
          }
          //alert("left")

          console.log("left")
        }
        return true;
      },
      onShouldBlockNativeResponder: (evt, gestureState) => {
        return true;
      },
    });





  }
  function() {
    // Register all the valid actions for notifications here and add the action handler for each action
    //PushNotification.registerNotificationActions(['Accept', 'Reject']);
    DeviceEventEmitter.addListener('notificationActionReceived', function (action) {
      //alert ('Notification action received: ' + action);
      const info = JSON.parse(action.dataJSON);
      alert(";;;")
      this.props.navigation.navigate('Timesheet');
      // if (info.action == 'Accept') {
      //   this.props.navigation.navigate("Timesheet")
      //   // Do work pertaining to Accept action here
      // } else if (info.action == 'Reject') {
      //   //alert(JSON.stringify(action))
      //   // Do work pertaining to Reject action here
      // }
      // Add all the required actions handlers
    });
  }

  //this function is called when this component will unmount...means
  // navigate to other screen

  updateSecondsOfTimerWhenScreenChanges() {
    getLog("====when componentWillUnmount" + this.state.second + "====" + global.flag)
    if (global.flag) {
      if (Platform.OS == 'ios') {
        if (this.state.play) {
          getLog("====when componentWillUnmount this.state.play" + this.state.play)
          BackgroundTimer.clearInterval(global.intervalId);
          clearInterval(global.interval);
          //backgroundSecond = 1
          backgroundSecond = this.state.second + 1
          global.intervalId = BackgroundTimer.setInterval(() => {
            backgroundSecond = backgroundSecond + 1
            this.calling_PushNotification_Func_Passing_Second(backgroundSecond)
            getLog(backgroundSecond)
          }, 1000);

        } else {
          backgroundSecond = this.state.second
        }
      } else {
        if (this.state.play) {
          BackgroundTimer.clearInterval(global.intervalId);
          clearInterval(global.interval);
          //backgroundSecond = 1
          backgroundSecond = this.state.second + 1
          global.intervalId = RNAndroidBackgroundTimer.setInterval(() => {
            backgroundSecond = backgroundSecond + 1
            this.calling_PushNotification_Func_Passing_Second(backgroundSecond)
            getLog(backgroundSecond)
          }, 1000);
        } else {
          backgroundSecond = this.state.second
        }
      }
      getLog("====when componentWillUnmount Done" + this.state.second + "====" + global.flag)
    } else {
      getLog("+++++++")
    }
  }

  //this function is called in component will mount when comes to this screen
  updateSecondsOfTimerWhenControllerComesToThisScreen() {
    getLog("====when componentWill=mount" + backgroundSecond)
    if (global.flag) {
      if (Platform.OS == 'ios') {
        BackgroundTimer.clearInterval(global.intervalId);
        clearInterval(global.interval);
        this.setState({ second: backgroundSecond, play: !global.play ? true : false })
        this.setState({ selectProject: global.selectedProject })
        setTimeout(() => {
          this.onTimerStart()
        }, 300)

        //backgroundSecond = 0
      } else {
        RNAndroidBackgroundTimer.clearInterval(global.intervalId);
        clearInterval(global.interval);

        this.setState({ second: backgroundSecond, play: !global.play ? true : false })
        this.setState({ selectProject: global.selectedProject })
        setTimeout(() => {
          this.onTimerStart()
        }, 300)

        //backgroundSecond = 0
      }

    }
  }


  componentDidMount() {
    //this.refs.scrollView.scrollTo({ x: (((25) * width / 3)), y: 0, animated: true });
    //this.flatListRef.scrollToIndex({animated: true, index: 26});
    AppState.addEventListener('change', this._handleAppStateChange);
    AsyncStorage.getItem('userToken').then((token) => {
      this.scrollToPosition(token)
    })
    //this.scroller.scrollTo({ x: this.state.week_Date_Array_Middle_Index * (width / 3), y: 0 })
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardOpen);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardClose);
  }
  scrollToPosition(token) {
    if (this.state.week == 'Week') {
      this.scroller.scrollTo({ x: this.state.week_Date_Array_Middle_Index * (width), y: 0 })
      this.hitGetLeaveListAPI(token, "week", new Date(week_Date_Array[this.state.week_Date_Array_Middle_Index + 2].startDate).getTime(), new Date(week_Date_Array[this.state.week_Date_Array_Middle_Index + 2].endDate).getTime())

      this.hitGetProjectListAPI(token, "week", new Date(week_Date_Array[this.state.week_Date_Array_Middle_Index + 2].startDate).getTime(), new Date(week_Date_Array[this.state.week_Date_Array_Middle_Index + 2].endDate).getTime())

    } else if (this.state.week == 'Month') {

      this.scroller.scrollTo({ x: this.state.week_Date_Array_Middle_Index * (width), y: 0 })
      this.hitGetLeaveListAPI(token, "month", this.state.week_Date_Array_Middle_Index + 1)
      this.hitGetProjectListAPI(token, "month", this.state.week_Date_Array_Middle_Index + 1)
    } else {
      this.scroller.scrollTo({ x: this.state.week_Date_Array_Middle_Index * (width), y: 0 })
      this.hitGetLeaveListAPI(token, "year", year_Array[this.state.week_Date_Array_Middle_Index + 2])
      this.hitGetProjectListAPI(token, "year", year_Array[this.state.week_Date_Array_Middle_Index + 2])
    }


  }

  tabs() {
    var arr = []
    for (var i = 2; i < this.state.arrayPassedToTabScrollView.length - 2; i++) {
      var prev = this.state.arrayPassedToTabScrollView[i - 1]
      var current = this.state.arrayPassedToTabScrollView[i]
      var next = this.state.arrayPassedToTabScrollView[i + 1]


      arr.push(this.makeColumn(prev, current, next, i))
    }
    return (
      arr
    )

  }
  makeColumn(prev, item, next, i) {
    return (
      <View
        style={[styles.tabStyle, { width: width, flexDirection: 'row' }]} >
        <TouchableWithoutFeedback onPress={() => prev != '' ? this.onWeekTabPressLeft() : null}>
          <View style={{ flex: 1, alignItems: "center", justifyContent: 'center' }}>
            <Text style={[styles.tabText, { color: "#2A56C6", fontWeight: "normal" }]}>
              {this.state.week == "Week" ?

                dateConverterUseInWeekTab(new Date(prev.startDate)) + "-" + dateConverterUseInWeekTab(new Date(prev.endDate))
                :
                prev
              }
            </Text>
          </View>
        </TouchableWithoutFeedback>
        <Text style={[styles.tabText, { flex: 1, color: "#000", fontWeight: "bold" }]}>
          {this.state.week == "Week" ?
            dateConverterUseInWeekTab(new Date(item.startDate)) + "-" + dateConverterUseInWeekTab(new Date(item.endDate))
            :
            item
          }
        </Text>
        <TouchableWithoutFeedback onPress={() => next != '' ? this.onWeekTabPressRight() : null}>
          <View style={{ flex: 1, alignItems: "center", justifyContent: 'center' }}>
            <Text style={[styles.tabText, { color: "#2A56C6", fontWeight: "normal" }]}>
              {this.state.week == "Week" ?

                dateConverterUseInWeekTab(new Date(next.startDate)) + "-" + dateConverterUseInWeekTab(new Date(next.endDate))

                :
                next
              }
            </Text>
          </View>
        </TouchableWithoutFeedback>
      </View>
    )
  }
  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
    // if (this.props.id) {
    //   this.props.id == 'leaves' ? this.updateSecondsOfTimerWhenScreenChanges() : null
    // }
    this.updateSecondsOfTimerWhenScreenChanges()
  }
  calling_PushNotification_Func_Passing_Second() {
    var min = ''
    if ((backgroundSecond / 60) > 59) {
      min = backgroundSecond / 60
      while (min >= 60) {
        min = min - 60
      }
      //this.setState({ minute: min })
    } else {
      min = backgroundSecond / 60
      //this.setState({ minute: (this.state.second / 60) })
    }
    this.sendPushNotification(min, backgroundSecond)
  }
  _handleAppStateChange = (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      //alert("App has come to the foreground!")
      if (Platform.OS == 'ios') {
        BackgroundTimer.clearInterval(global.intervalId);

        clearInterval(global.interval);
        if (this.state.play) {

          this.setState({ second: backgroundSecond + 1, play: false })
          this.onTimerStart()
        }
        backgroundSecond = 0
      } else {
        RNAndroidBackgroundTimer.clearInterval(global.intervalId);
        clearInterval(global.interval);
        if (this.state.play) {

          this.setState({ second: backgroundSecond + 1, play: false })
          this.onTimerStart()
        }
        backgroundSecond = 0
      }


    } else {
      global.flag = false
      // alert('App has come to the background!')
      if (Platform.OS == 'ios') {
        if (this.state.play) {
          BackgroundTimer.clearInterval(global.intervalId);
          clearInterval(global.interval);
          //backgroundSecond = 1
          backgroundSecond = this.state.second
          global.intervalId = BackgroundTimer.setInterval(() => {
            backgroundSecond = backgroundSecond + 1

            this.calling_PushNotification_Func_Passing_Second(backgroundSecond)
            getLog(backgroundSecond)
          }, 1000);

        }
      } else {
        if (this.state.play) {
          BackgroundTimer.clearInterval(global.intervalId);
          clearInterval(global.interval);
          //backgroundSecond = 1
          backgroundSecond = this.state.second
          global.intervalId = RNAndroidBackgroundTimer.setInterval(() => {
            backgroundSecond = backgroundSecond + 1

            this.calling_PushNotification_Func_Passing_Second(backgroundSecond)
            getLog(backgroundSecond)
          }, 1000);
        }
      }

    }
    this.setState({ appState: nextAppState });
  }
  keyboardOpen() {
    keyboardOpen = true
    //this.setState({ keyboardOpen: true })
  }

  keyboardClose() {
    keyboardOpen = false
    //this.setState({ keyboardOpen: false })
  }

  hitGetAllLeaveTypesAPI(token) {
    return webservice('', "leavetype/getlist", 'Post', token)
      .then(response => {
        this.setState({ spinnerVisible: false })
        if (response != "error") {
          if (response.leaveTypeList.length != 0) {
            var arr = []

            for (var i = 0; i < response.leaveTypeList.length; i++) {
              arr.push(response.leaveTypeList[i].leaveTypeName)
            }
            this.setState({ leaveTypeList: response.leaveTypeList, leaveTypeListPassToModal: arr })

          }
          getLog("Leave typeList=====++++" + JSON.stringify(response))



        }

      })
  }

  hitGetLeaveListAPI(token, action, startDate, endDate) {
    let variables = new FormData();

    variables.append("action", action)
    variables.append("startDate", startDate)
    if (action == 'week') {
      variables.append("endDate", endDate)
    }

    //this.setState({ spinnerVisible: true })
    this.setState({ dataLoading: true })
    //dataLoading
    return webservice(variables, "leave/getlist", 'Post', token)
      .then(response => {
        this.setState({ spinnerVisible: false, dataLoading: false })
        if (response != "error") {
          if (response.leaveList.length != 0) {

            var arr = []
            for (var i = 0; i < response.leaveList.length; i++) {
              arr.push(
                {
                  "validityFrom": response.leaveList[i].startDate,
                  "validityTo": response.leaveList[i].endDate,
                  'leaveId': response.leaveList[i].leaveId,
                  'leaveTypeId': response.leaveList[i].leaveTypeId,
                  "type": response.leaveList[i].leaveTypeName,
                  "status": response.leaveList[i].status,
                  'totalDays': response.leaveList[i].days,
                  "reason": "These are the commen adsjhas adhkjhsad asdhbkjsa asdhas"
                })
            }

            this.setState({ leaveList: arr })
            //this.makeWeekDateTabArray(this.state.week_Date_Array_Middle_Index)
          } else {
            this.setState({ leaveList: [] })
          }
          this.totalNumberOfDaysLeave()
          getLog("Leave List of time sheet page=====++++" + JSON.stringify(response))



        }

      })

  }
  hitGetProjectListAPI(token, action, startDate, endDate) {
    let variables = new FormData();

    variables.append("action", action)
    variables.append("startDate", startDate)
    variables.append("page", 0)
    if (action == 'week') {
      variables.append("endDate", endDate)
    }

    //this.setState({ spinnerVisible: true })
    this.setState({ dataLoading: true })
    //dataLoading
    return webservice(variables, "timesheet/getlist", 'Post', token)
      .then(response => {
        this.setState({ spinnerVisible: false, dataLoading: false })
        if (response != "error") {
          if (response.timesheetList.length != 0) {

            var arr = []
            for (var i = 0; i < response.timesheetList.length; i++) {
              arr.push(
                {
                  "projectName": response.timesheetList[i].projectName,
                  "projectId": response.timesheetList[i].projectId,
                  'Activities': response.timesheetList[i].activityCount,
                  'userId': response.timesheetList[i].userId,
                  "time": response.timesheetList[i].totalHours,

                })
            }
            // this.totalHoursMinutesOfProjects(response.timesheetList)



            this.setState({ projectList: arr })

          } else {
            this.setState({ projectList: [] })
          }
          this.totalHoursMinutesOfProjects(response.timesheetList)
          //this.totalNumberOfDaysLeave()
          getLog("Timesheet project list List of time sheet page=====++++" + JSON.stringify(response))
          if (action == 'week') {
            this.setState({
              filterDataPassedOnProjectBreakdown: {
                "startDate": startDate,
                "action": action,
                "endDate": endDate,
                'week_Date_Array_Middle_Index': this.state.week_Date_Array_Middle_Index

              }
            })


          } else {
            this.setState({
              filterDataPassedOnProjectBreakdown: {
                "startDate": startDate,
                "action": action,
                "endDate": '',
                'week_Date_Array_Middle_Index': this.state.week_Date_Array_Middle_Index
              }
            })

          }


        } else {

        }

      })

  }
  hitGetAllProjectListApi(token) {
    let variables = new FormData();
    //this.setState({ spinnerVisible: true, projectNameMandatory: false })
    variables.append("page", 0)

    return webservice(variables, "project/getlist", 'POST', token)
      .then(response => {
        this.setState({ spinnerVisible: false })
        if (response != "error") {
          if (response.projectList.length != 0) {

            var arr = response.projectList
            for (var i = 0; i < response.projectList.length; i++) {
              arr[i].select = false
            }
            this.setState({ projectListPassedToSelectProjectDropdown: arr, projectListContainAllProjectOfSelectProjectDropdown: arr })
          }
          getLog("ProjectList=====++++" + JSON.stringify(response.projectList))
        } else {
          getLog("ProjectList err response=====++++" + JSON.stringify(response))
          this.setState({ spinnerVisible: false })
        }

      })
  }
  onTabPress() {
    this.setState({ TabPress: !this.state.TabPress })
  }

  navigateToLeaveDescription(item, index) {
    this.props.navigation.navigate('LeaveDescription', { data: item, leaveTabCallBack: this.state.leaveTabCallBack, index: index, leaveTypeListPassToModal: this.state.leaveTypeListPassToModal, leaveTypeList: this.state.leaveTypeList })
  }
  navigateToProjectBreakdown(item) {

    this.props.navigation.navigate('ProjectBreakdown', { data: item, filterData: this.state.filterDataPassedOnProjectBreakdown, callbackPassedToProjectBreakdown: this.state.callbackPassedToProjectBreakdown, })
  }
  createList(item, index) {
    // var progress = item.progress + "%"
    // var left = (100 - item.progress) + "%"
    var progress = Math.round((item.time / this.state.totalProjectHours) * 100)

    var left = Math.round(100 - progress) + "%"
    progress = progress + '%'
    //getLog(progress+"===="+left)

    return (
      <TouchableWithoutFeedback onPress={() => this.state.TabPress ? this.navigateToProjectBreakdown(item) : this.navigateToLeaveDescription(item, index)}
        key={index + "rowListProject"} style={styles.listView}>
        <View style={[styles.listView, {}]}>
          <View style={[styles.rowView, { paddingLeft: 0, paddingRight: 0 }]}>
            <View style={styles.projectColumnView}>
              <Text numberOfLines={1} style={{ fontSize: fontSizes.font14 }}>
                {this.state.TabPress ? item.projectName :
                  (dateConverterUseInWeekTab(new Date(item.validityFrom)) + "-" + dateConverterUseInWeekTab(new Date(item.validityTo)))}
              </Text>
              <Text style={{ fontSize: fontSizes.font14, color: "#BBBBBB", paddingTop: 5 }}>
                {this.state.TabPress ? item.Activities + " Activities" : item.type}
                {this.state.TabPress ? null :
                  <Text> | </Text>
                }
                {this.state.TabPress ? null :
                  <Text style={{
                    fontSize: fontSizes.font14,
                    color: item.status == 0 ? "red" : item.status == 1 ? "green" : "grey",
                    paddingTop: 5
                  }}> {item.status == 0 ? "Pending" : item.status == 1 ? "Approved" : "Rejected"}</Text>
                }
              </Text>
            </View>
            {this.state.TabPress ? <View style={[styles.projectColumnView, { flex: 2, marginHorizontal: 5, flexDirection: "row", alignItems: "center" }]}>
              <View style={{ width: progress, backgroundColor: "#2A56C6", height: 4, }} />
              <View style={{ width: left, backgroundColor: "#D8D8D8", height: 4 }} />
            </View> : null}
            <View style={[styles.projectColumnView, { flex: this.state.TabPress ? 1.5 : 0.5, flexDirection: "row", justifyContent: "flex-end", alignItems: "center" }]}>

              <Text style={{ fontSize: fontSizes.font16, color: "#6C6C6C", marginRight: 10 }}>
                {this.state.TabPress ? hourMinuteConverter(item.time) : item.totalDays > 1 ? item.totalDays + " Days" : item.totalDays + " Day"}
              </Text>
              <Image source={require('../images/rightArrow.png')} />

            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    )
  }

  // timesheet_LeavesTab() {
  //   return (
  //     <View style={styles.tabView}>
  //       {/* first tab */}
  //       <TouchableOpacity onPress={() => this.onTabPress()}
  //         style={[styles.tabStyle, { borderBottomColor: "white", borderBottomWidth: this.state.TabPress ? 2 : 0 }]} >
  //         <Text style={styles.tabText}>
  //           Timesheet
  //             </Text>
  //       </TouchableOpacity>
  //       {/* second tab */}
  //       <TouchableOpacity onPress={() => this.onTabPress()}
  //         style={[styles.tabStyle, { borderBottomColor: "white", borderBottomWidth: this.state.TabPress ? 0 : 2 }]}   >
  //         <Text style={styles.tabText}>
  //           Leaves
  //                           </Text>
  //       </TouchableOpacity>
  //       {/* third tab */}
  //       <View style={styles.tabStyle} >
  //       </View>
  //     </View>
  //   )
  // }
  onWeekTabPressLeft() {
    this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index - 1 })
    setTimeout(() => {
      this.scrollToPosition(this.state.userToken)
    }, 100)

  }
  onWeekTabPressRight() {
    this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index + 1 })
    setTimeout(() => {
      this.scrollToPosition(this.state.userToken)
    }, 100)
  }
  makeWeekDateTabArray(token) {
    var index = this.state.week_Date_Array_Middle_Index
    var weekArray = []
    weekArray.push(dateConverterUseInWeekTab(new Date(week_Date_Array[index - 1].startDate)) + "-" + dateConverterUseInWeekTab(new Date(week_Date_Array[index - 1].endDate)))
    weekArray.push(dateConverterUseInWeekTab(new Date(week_Date_Array[index].startDate)) + "-" + dateConverterUseInWeekTab(new Date(week_Date_Array[index].endDate)))
    weekArray.push(dateConverterUseInWeekTab(new Date(week_Date_Array[index + 1].startDate)) + "-" + dateConverterUseInWeekTab(new Date(week_Date_Array[index + 1].endDate)))
    this.setState({ weekArray: weekArray })

    //alert(new Date(week_Date_Array[index].startDate) + new Date(week_Date_Array[index].endDate))
    this.hitGetLeaveListAPI(token, "week", new Date(week_Date_Array[index].startDate).getTime(), new Date(week_Date_Array[index].endDate).getTime())

    this.hitGetProjectListAPI(token, "week", new Date(week_Date_Array[index].startDate).getTime(), new Date(week_Date_Array[index].endDate).getTime())


    // this.sortLeaveListByWeek(index)
  }
  // sortLeaveListByWeek(index) {
  //   var arr = []
  //   for (var i = 0; i < this.state.leaveList.length; i++) {
  //     if ((this.state.leaveList[i].validityTo >= week_Date_Array[index].startDate) && (this.state.leaveList[i].validityFrom <= week_Date_Array[index].endDate)) {
  //       arr.push(this.state.leaveList[i])
  //     }
  //   }
  //  // alert(JSON.stringify(this.state.leaveList[0]))
  //   getLog("==="+JSON.stringify(arr))
  //   this.setState({ leaveList: arr })
  // }
  pickerDropdownModalRowSelected(item) {
    if (this.state.PickerDropdownModalType == "Select Project") {
      this.setState({ selectProject: item.projectName, selectedProjectId: item.projectId })

      //this code clear the interval also reset the global variables
      //cancel the push notification and reset all the states
      clearInterval(global.interval);
      global.flag = false;
      global.selectedProject = '';
      global.play = false
      backgroundSecond = 0
      PushNotification.cancelLocalNotifications({ id: '123' });
      this.setState({
        second: 0, playPauseText: 'Play', minute: 0,
        playPauseImage: require('../images/timesheetIcon/playIcon.png'),
        stopImage: require('../images/timesheetIcon/greyStopIcon.png'),

        play: false
      })

    } else if (this.state.PickerDropdownModalType == 'Select Leave Type') {
      this.setState({ leaveTypeOnLeaveModal: item.type })
    }
    else {
      this.setState({ week: item })

      // if (item == "Week") {
      //   this.setState({ week_Date_Array_Middle_Index: 26 })
      //   setTimeout(() => {
      //     this.makeWeekDateTabArray(this.state.userToken)
      //   }, 300)

      // } else if (item == "Month") {

      //   var month = new Date().getMonth()
      //   this.setState({ week_Date_Array_Middle_Index: month })
      //   if (month > 0 && month < 11) {
      //     setTimeout(() => {
      //       this.setState({ weekArray: [monthArr[month - 1], monthArr[month], monthArr[month + 1]] })
      //     }, 300)
      //   } else {
      //     if (month == 0) {
      //       setTimeout(() => {
      //         this.setState({ weekArray: [monthArr[month], monthArr[month + 1], monthArr[month + 2]], week_Date_Array_Middle_Index: 1 })
      //       }, 300)
      //     } else {
      //       setTimeout(() => {
      //         this.setState({ weekArray: [monthArr[month - 2], monthArr[month - 1], monthArr[month]], week_Date_Array_Middle_Index: 10 })
      //       }, 300)
      //     }

      //   }
      //   setTimeout(() => {
      //     this.hitGetLeaveListAPI(this.state.userToken, "month", monthArr.indexOf(monthArr[this.state.week_Date_Array_Middle_Index + 1]))
      //     this.hitGetProjectListAPI(this.state.userToken, "month", monthArr.indexOf(monthArr[this.state.week_Date_Array_Middle_Index + 1]))
      //   }, 300)


      // } else {
      //   var year = new Date().getFullYear()
      //   this.setState({ week_Date_Array_Middle_Index: year })
      //   setTimeout(() => {
      //     this.hitGetLeaveListAPI(this.state.userToken, "year", this.state.week_Date_Array_Middle_Index)
      //     this.hitGetProjectListAPI(this.state.userToken, "year", this.state.week_Date_Array_Middle_Index)
      //     this.setState({ weekArray: [this.state.week_Date_Array_Middle_Index - 1, this.state.week_Date_Array_Middle_Index, this.state.week_Date_Array_Middle_Index + 1] })
      //   }, 300)
      // }
      if (item == "Week") {
        this.setState({ week_Date_Array_Middle_Index: 24, arrayPassedToTabScrollView: week_Date_Array, week: "Week" })
        setTimeout(() => {
          this.scrollToPosition(this.state.userToken)
        }, 300)
      } else if (item == "Month") {
        this.scroller.scrollTo({ x: 0, y: 0, animated: false })
        var month = new Date().getMonth()
        this.setState({ arrayPassedToTabScrollView: monthArr, week: "Month", week_Date_Array_Middle_Index: month })
        setTimeout(() => {
          this.scrollToPosition(this.state.userToken)

        }, 300)
      } else {
        this.scroller.scrollTo({ x: 0, y: 0, animated: false })
        var year = new Date().getFullYear()
        this.setState({ week_Date_Array_Middle_Index: year_Array.indexOf(year - 2), arrayPassedToTabScrollView: year_Array, week: "Year" })
        setTimeout(() => {
          this.scrollToPosition(this.state.userToken)
        }, 300)
      }


    }
    this.setState({ pickerDropdownModalVisible: false })
  }
  weekTabs() {
    return (
      <View style={styles.weekTabView}>
        {/* first tab */}
        <TouchableWithoutFeedback onPress={() => this.onWeekTabPressLeft()}>
          <View
            style={[styles.tabStyle]} >
            <Text style={[styles.tabText, { color: "#2A56C6" }]}>
              {this.state.weekArray[0]}
            </Text>
          </View>
        </TouchableWithoutFeedback>
        {/* second tab */}
        <TouchableWithoutFeedback >
          <View
            style={[styles.tabStyle]}   >
            <Text style={[styles.tabText, { color: "#000", fontWeight: "bold" }]}>
              {this.state.weekArray[1]}
            </Text>
          </View>
        </TouchableWithoutFeedback>
        {/* third tab */}
        <TouchableWithoutFeedback onPress={() => this.onWeekTabPressRight()}>
          <View style={[styles.tabStyle]}   >
            <Text style={[styles.tabText, { color: "#2A56C6" }]}>
              {this.state.weekArray[2]}
            </Text>
          </View>
        </TouchableWithoutFeedback>
      </View>
    )
  }
  onFloatingButtonPressed() {
    !this.state.TabPress ?
      this.setState({
        modalVisible: true, leaveModalErrorText: '',
        startDate: '',
        endDate: '',
        leaveTypeOnLeaveModal: 'Leave Type',
        totalDays: "" + 0,
        reasonTextInputData: '',
        startDateToShowOnMOdal: '',
        endDateToShowOnModal: '',
        leaveModalErrorText: ''
      })
      : this.props.navigation.navigate("CreateTimesheet", {
        data: {
          returnCallBack: this.state.returnCallBack,
          filterData: this.state.filterDataPassedOnProjectBreakdown
          //projectName: this.state.selectProject,
        }
      })
  }
  callBackCalledOnActivityAdd = (token, data) => {
    //alert(data)
    // this.state.projectList.push({ "projectName": data.projectName, 'Activities': data.activity, 'progress': 0, 'time': data.time })
    // this.setState({ projectList: this.state.projectList })
    this.hitGetProjectListAPI(token, data.action, data.startDate, data.endDate)
    this.hitGetAllProjectListApi(token)

    //this callback is called when we click on + floating icon and add a new activity that is added to this screen flatlist
  }
  callbackCalledOnChangesOnProjectBreakdown = (token, data) => {
    this.hitGetProjectListAPI(token, data.action, data.startDate, data.endDate)
    this.hitGetAllProjectListApi(token)
  }
  callBackCalledOnEditDeleteLeave = (data) => {
    if (data.type == "delete") {
      var arr = []
      for (var i = 0; i < this.state.leaveList.length; i++) {
        if (i != data.index) {
          arr.push(this.state.leaveList[i])
        }
      }
      this.setState({ leaveList: arr })
    } else {

      var arr = this.state.leaveList
      arr[data.index].validityFrom = data.editedData.validityFrom
      arr[data.index].validityTo = data.editedData.validityTo
      arr[data.index].status = data.editedData.status
      arr[data.index].totalDays = data.editedData.totalDays
      arr[data.index].reason = data.editedData.reason
      arr[data.index].type = data.editedData.type

      this.setState({ leaveList: arr })

    }
    this.totalNumberOfDaysLeave()


    //this callback is called when we click on delete button of leave description scren and edit leave description on leave description screen
  }
  getDateFromModal(date) {
    getLog("getDateFromModal" + date)
    if (this.state.datePickerType == 'start') {
      this.setState({ startDate: date, datePickerType: '', minDate: date, datePickerVisible: false, endDateToShowOnModal: "", endDate: '' })
      var d = dateConverterUseInHeader(date)
      setTimeout(() => {
        this.setState({ startDateToShowOnMOdal: d })
      }, 200)
    } if (this.state.datePickerType == 'end') {
      this.setState({ endDate: date, datePickerType: '', datePickerVisible: false, })
      var d = dateConverterUseInHeader(date)
      setTimeout(() => {
        this.setState({ endDateToShowOnModal: d })
        this.totalDaysCalculate()
      }, 200)

    }


  }
  totalDaysCalculate() {
    getLog(this.state.endDate)
    var date1 = new Date(this.state.startDate);
    var date2 = new Date(this.state.endDate);
    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)) + 1;
    this.daysTextInputChangeOnLeaveModal("" + diffDays)
    // this.setState({ totalDays: diffDays + 1 })
    // setTimeout(() => {
    //   console.log(this.state.totalDays)
    // }, 300)



  }
  startDateVisibleFunction() {
    this.setState({ datePickerVisible: true, datePickerType: 'start', minDate: '' })
  }
  endDateVisibleFunction() {
    if (this.state.startDate == '') {
      alert("Please select start date first")
    } else {
      this.setState({ datePickerVisible: true, datePickerType: 'end', minDate: this.state.minDate })
    }

  }
  totalNumberOfDaysLeave() {
    setTimeout(() => {
      var days = 0
      for (var i = 0; i < this.state.leaveList.length; i++) {
        getLog(this.state.leaveList.length + "====" + this.state.leaveList[i].totalDays)
        days = days + parseFloat(this.state.leaveList[i].totalDays)

      }

      this.setState({ totalLeaveDays: days })
    }, 300)
  }
  totalHoursMinutesOfProjects(response) {

    var minutes = 0
    this.setState({ totalProjectHours: 0 })
    for (var i = 0; i < response.length; i++) {
      //getLog(this.state.leaveList.length + "====" + this.state.leaveList[i].totalDays)
      minutes = minutes + Number(response[i].totalHours)

    }


    this.setState({ totalProjectHours: minutes })

  }

  hitAddLeaveAPI() {
    var arr = this.state.leaveList
    let variables = new FormData();
    //this.setState({ spinnerVisible: true, projectNameMandatory: false })
    variables.append("leaveTypeId", this.state.leaveTypeId)
    variables.append("days", this.state.totalDays)
    variables.append("startDate", new Date(this.state.startDate).getTime())
    variables.append("endDate", new Date(this.state.endDate).getTime())
    variables.append("reason", this.state.reasonTextInputData)

    this.setState({ spinnerVisible: true })
    return webservice(variables, "leave/add", 'POST', this.state.userToken)
      .then(response => {
        this.setState({ spinnerVisible: false })
        if (response != "error") {
          // var arr = this.state.leaveList
          arr.push(
            {
              "validityFrom": this.state.startDate,
              "validityTo": this.state.endDate,
              'type': this.state.leaveTypeOnLeaveModal,
              'status': 0,
              'totalDays': this.state.totalDays,
              "reason": this.state.reasonTextInputData,
              "leaveTypeId": this.state.leaveTypeId
            }
          )
          this.setState({ leaveList: arr })
          //this.totalNumberOfDaysLeave()
          setTimeout(() => {
            this.setState({ modalVisible: false })
            this.setState({
              startDate: '',
              endDate: '',
              leaveTypeOnLeaveModal: 'Leave Type',
              //totalDays: "" + 0,
              reasonTextInputData: '',
              startDateToShowOnMOdal: '',
              endDateToShowOnModal: '',
              leaveModalErrorText: ''


            })
          }, 300)
        } else {
          getLog("Leave  add response=====++++" + JSON.stringify(response))

        }

      })
  }

  saveButtonClicked() {

    if (this.state.leaveTypeOnLeaveModal == "Leave Type") {
      this.setState({ leaveModalErrorText: "*Please select leave type." })
      return
    }
    if (this.state.startDate == "") {
      this.setState({ leaveModalErrorText: "*Please select start date." })
      return
    }
    if (this.state.endDate == "") {
      this.setState({ leaveModalErrorText: "*Please select end date." })
      return
    }
    this.hitAddLeaveAPI()
    // var arr = this.state.leaveList
    // arr.push(
    //   {
    //     "validityFrom": this.state.startDate,
    //     "validityTo": this.state.endDate,
    //     'type': this.state.leaveTypeOnLeaveModal,
    //     'status': "Pending",
    //     'totalDays': this.state.totalDays,
    //     "reason": this.state.reasonTextInputData
    //   }
    // )
    // this.setState({ leaveList: arr })
    // this.totalNumberOfDaysLeave()
    // setTimeout(() => {
    //   this.setState({ modalVisible: false })
    //   this.setState({
    //     startDate: '',
    //     endDate: '',
    //     leaveTypeOnLeaveModal: 'Leave Type',
    //     totalDays: "" + 0,
    //     reasonTextInputData: '',
    //     startDateToShowOnMOdal: '',
    //     endDateToShowOnModal: '',
    //     leaveModalErrorText: ''


    //   })
    // }, 300)

    //alert(this.state.reasonTextInputData)
  }
  modalClose() {
    if (keyboardOpen) {
      Keyboard.dismiss()
    } else {
      this.setState({ modalVisible: false })
    }

  }

  sendPushNotification(minute, second) {
    // if (Platform.OS == 'ios') {
    //   getLog("push")
    //   var details = {
    //     alertBody:" pppppppppppp",
    //     fireDate:new Date()

    //   }
    //   //PushNotificationIOS.scheduleLocalNotification(details);
    // } else {

    PushNotification.localNotification({
      //... You can use all the options from localNotifications
      //message: "My Notification Message", // (required)
      title: "Timer for project is running", // (optional)
      //message: " "+second, // (required)
      message: " " + (Math.floor(second / 3600) > 9 ? Math.floor(second / 3600) : "0" + Math.floor(second / 3600)) + ":" +
        (Math.floor(minute) > 9 ? Math.floor(minute) : "0" + Math.floor(minute)) +
        ':' +
        ('0' + second % 60).slice(-2),
      id: '123',
      userInfo: { id: '123' },
      repeatType: 'time',
      playSound: false,
      vibrate: false,
      repeatTime: 1000,
      autoCancel: false, // (optional) default: true
      //actions: '["Accept", "Reject"]',
      //date: new Date(Date.now() + (60 * 1000)) // in 60 secs
    });
    



//    }



  }
  onTimerStart() {
    if (this.state.selectProject != "Select Project") {
      if (this.state.play) {
        global.flag = true
        global.selectedProject = this.state.selectProject
        global.play = false

        //calculate minute when timer pause and go to another screen and comes back to this screen

        if ((this.state.second / 60) > 59) {
          var min = this.state.second / 60
          while (min >= 60) {
            min = min - 60
          }
          this.setState({ minute: min })
        } else {
          this.setState({ minute: (this.state.second / 60) })
        }

        //
        this.sendPushNotification(this.state.minute, this.state.second)
        clearInterval(global.interval);
        this.setState({ play: false, playPauseImage: require('../images/timesheetIcon/resumeIcon.png'), playPauseText: 'Play' })
      } else {
        global.flag = true
        global.selectedProject = this.state.selectProject
        global.play = true
        backgroundSecond = 0
        BackgroundTimer.clearInterval(global.intervalId);
        RNAndroidBackgroundTimer.clearInterval(global.intervalId);
        this.setState({ play: true, playPauseImage: require('../images/timesheetIcon/pauseIcon.png'), playPauseText: 'Pause', stopImage: require('../images/timesheetIcon/redStopIcon.png'), })
        global.interval = setInterval(() => {
          //seconds++
          getLog(Math.floor(this.state.second / 60))
          if (this.state.minute % 5 == 0) {
            if (this.state.second == 0) {
              this.setState({ second: this.state.second + 1 })
            } else {
              this.setState({ second: this.state.second + 2 })
            }

          }
          else if (this.state.minute % 15 == 0) {
            this.setState({ second: this.state.second + 2 })
          }
          else if (this.state.minute % 20 == 0) {
            this.setState({ second: this.state.second + 2 })
          }
          else if (this.state.minute % 25 == 0) {
            this.setState({ second: this.state.second + 2 })
          } else {
            this.setState({ second: this.state.second + 1 })
          }


          if ((this.state.second / 60) > 59) {
            var min = this.state.second / 60
            while (min >= 60) {
              min = min - 60
            }
            this.setState({ minute: min })
          } else {
            this.setState({ minute: (this.state.second / 60) })
          }

          backgroundSecond = this.state.second + 1
          this.sendPushNotification(this.state.minute, this.state.second)


        }, 1000)
      }
    } else {
      alert("Please select project from dropdown.")
    }

  }

  onTimerStop() {
    if (this.state.stopImage != require('../images/timesheetIcon/greyStopIcon.png')) {
      clearInterval(global.interval);
      global.flag = false;
      global.selectedProject = '';
      global.play = false
      backgroundSecond = 0
      PushNotification.cancelLocalNotifications({ id: '123' });
      var hours = Math.round(this.state.second / (60 * 60));

      var divisor_for_minutes = this.state.second % (60 * 60);
      var minutes = Math.round(divisor_for_minutes / 60);

      //calculate start and end time to pass on timesheet page
      //here end time is when we stop the timer and start time is calculated when we subtract the endtime in seconds to total seconds
      var endTime = new Date().getTime()
      var startTime = new Date().getTime() - (this.state.second * 1000)
      //startTime=new Date(startTime*1000).getTime()
      //alert(new Date(endTime) + "---------"+new Date(startTime))
      this.props.navigation.navigate("CreateTimesheet", {
        data: {
          projectName: this.state.selectProject,
          projectId: this.state.selectedProjectId,
          timerStop: true,
          hours: hours * 60 + minutes == 0 ? 1 : hours * 60 + minutes,
          endTime: endTime,
          startTime: startTime,
          date: new Date().getTime(),
          returnCallBack: this.state.returnCallBack,
          filterData: this.state.filterDataPassedOnProjectBreakdown
        }
      })
      this.setState({
        second: 0, playPauseText: 'Play', minute: 0,
        playPauseImage: require('../images/timesheetIcon/playIcon.png'),
        stopImage: require('../images/timesheetIcon/greyStopIcon.png'),
        selectProject: "Select Project",
        play: false
      })
    }

  }
  onProjectSelectDropdown() {
    this.setState({ pickerDropdownModalVisible: true, PickerDropdownModalType: "Select Project", pickerDropdownModalListData: this.state.projectListPassedToSelectProjectDropdown })
  }

  onWeekSelectDropdown() {
    this.setState({ pickerDropdownModalVisible: true, PickerDropdownModalType: "Week", pickerDropdownModalListData: ["Week", "Month", "Year"] })
  }
  onLeaveModalselectLeaveTypeClicked(index, value) {

    this.setState({ leaveTypeOnLeaveModal: value, leaveTypeId: this.state.leaveTypeList[index].leaveTypeId })
  }
  daysTextInputChangeOnLeaveModal(text) {
    this.setState({ totalDays: text })
  }
  navigateToAnalysis() {
    this.props.navigation.navigate("AnalysisExport")
  }
  customProjectListForModalRowSelected(item, index) {
    // var arr = this.state.projectListPassedToSelectProjectDropdown
    // for (var i = 0; i < arr.length; i++) {
    //   if (i == index) {
    //     arr[i].select = true
    //   } else {
    //     arr[i].select = false

    //   }

    // }
    // this.setState({ projectListPassedToSelectProjectDropdown: arr, selectedProjectItem: item })
    this.setState({ selectProject: item.projectName, selectedProjectId: item.projectId })

    //this code clear the interval also reset the global variables
    //cancel the push notification and reset all the states
    clearInterval(global.interval);
    global.flag = false;
    global.selectedProject = '';
    global.play = false
    backgroundSecond = 0
    PushNotification.cancelLocalNotifications({ id: '123' });
    this.setState({
      second: 0, playPauseText: 'Play', minute: 0,
      playPauseImage: require('../images/timesheetIcon/playIcon.png'),
      stopImage: require('../images/timesheetIcon/greyStopIcon.png'),
      selectProjectModal: false,
      play: false, selectedProjectItem: '',
      projectListPassedToSelectProjectDropdown: this.state.projectListContainAllProjectOfSelectProjectDropdown
    })
  }
  pickerDropdownSelectClickedOfSelectProject(item) {
    if (item != '') {
      this.setState({ selectProject: item.projectName, selectedProjectId: item.projectId })

      //this code clear the interval also reset the global variables
      //cancel the push notification and reset all the states
      clearInterval(global.interval);
      global.flag = false;
      global.selectedProject = '';
      global.play = false
      backgroundSecond = 0
      PushNotification.cancelLocalNotifications({ id: '123' });
      this.setState({
        second: 0, playPauseText: 'Play', minute: 0,
        playPauseImage: require('../images/timesheetIcon/playIcon.png'),
        stopImage: require('../images/timesheetIcon/greyStopIcon.png'),
        selectProjectModal: false,
        play: false, selectedProjectItem: ''
      })
      var arr = this.state.projectListPassedToSelectProjectDropdown
      for (var i = 0; i < arr.length; i++) {
        arr[i].select = false

      }
    } else {
      alert("Please select project.")
    }


  }
  pickerDropdownCloseClickedOfSelectProject() {

    var arr = this.state.projectListPassedToSelectProjectDropdown
    // for (var i = 0; i < arr.length; i++) {
    //   arr[i].select = false

    // }
    this.setState({ selectProjectModal: false, selectedProjectItem: '', projectListPassedToSelectProjectDropdown: this.state.projectListContainAllProjectOfSelectProjectDropdown })
  }
  onChangeOfSearchTextInputOfSelectProjectModal(text) {
    if (text != '') {
      var arr = []
      var typeArr = this.state.projectListContainAllProjectOfSelectProjectDropdown
      for (var i = 0; i < typeArr.length; i++) {

        if (typeArr[i].projectName.toLowerCase().indexOf(text.toLowerCase()) != -1) {
          arr.push(typeArr[i])
        }
      }
      this.setState({ projectListPassedToSelectProjectDropdown: arr })
    } else {
      this.setState({ projectListPassedToSelectProjectDropdown: this.state.projectListContainAllProjectOfSelectProjectDropdown })
    }



  }

  render() {
    return (
      <SafeAreaView style={styles.safeArea}>
        <View style={styles.container}>
          <Spinner visible={this.state.spinnerVisible} />
          <Header HeaderLeftText={this.state.week}
            HeaderRightText="Analysis"
            toggleDrawer={() => this.props.navigation.openDrawer()}
            headerLeftIcon={require('../images/headerIcons/downArrow.png')}
            headerRightIcon={require('../images/headerIcons/analysis.png')}
            headerLeftIconOnPress={() => this.onWeekSelectDropdown()}
            headerRightIconOnPress={() => this.navigateToAnalysis()} />
          <FloatingButton onButtonPressed={() => this.onFloatingButtonPressed()} icon={require('../images/floatPlusIcon.png')} />


          <LeaveModal
            modalVisible={this.state.modalVisible}
            datePickerVisible={this.state.datePickerVisible}
            datePickerVisibleFunction={() => this.setState({ datePickerVisible: true })}
            startDatePickerVisibleFunction={() => this.startDateVisibleFunction()}
            endDatePickerVisibleFunction={() => this.endDateVisibleFunction()}
            dateSelected={(date) => this.getDateFromModal(date)}
            startDate={this.state.startDateToShowOnMOdal}
            endDate={this.state.endDateToShowOnModal}
            daysTextInputData={this.state.totalDays}
            daysTextInputChange={(text) => this.daysTextInputChangeOnLeaveModal(text)}
            datePickerCloseFunction={() => this.setState({ datePickerVisible: false })}
            minDate={this.state.minDate == '' ? new Date('01/01/1990') : this.state.minDate}
            saveButtonClicked={() => this.saveButtonClicked()}
            reasonTextInputDataChange={(text) => this.setState({ reasonTextInputData: text })}
            reasonTextInputData={this.state.reasonTextInputData}
            modalClose={() => this.modalClose()}
            TitleText="Add Leave" TypePlaceHolder={this.state.leaveTypeOnLeaveModal}
            leavesOptionArrayOnLeaveModal={this.state.leaveTypeListPassToModal}
            selectLeaveTypeClicked={(index, value) => this.onLeaveModalselectLeaveTypeClicked(index, value)}
            errorText={this.state.leaveModalErrorText}
          />
          <SelectProjectModal
            pickerDropdownModalVisible={this.state.selectProjectModal}
            pickerDropdownModalClose={() => this.pickerDropdownCloseClickedOfSelectProject()}
            titleText="Select Project"
            type='singleSelect'
            pickerDropdownModalListData={this.state.projectListPassedToSelectProjectDropdown}
            extraData={this.state}
            //totalSelectedCount={this.state.totalSelectedCount}
            selectedCountShow={false}
            onChangeOfSearchTextInput={(text) => this.onChangeOfSearchTextInputOfSelectProjectModal(text)}
            //pickerDropdownSelectClicked={() => this.pickerDropdownSelectClickedOfSelectProject(this.state.selectedProjectItem)}
            pickerDropdownModalRow={({ item, index }) =>
              <TouchableWithoutFeedback key={"abc" + index}
                onPress={() => this.customProjectListForModalRowSelected(item, index)}>
                <View style={{ padding: 12, paddingLeft: 25, alignItems: "center", justifyContent: "flex-start", borderBottomColor: "#E5E5E5", borderBottomWidth: 1, flexDirection: "row" }}>
                  {/* <Image source={item.select == true ? require('../images/createTimesheet/tickIcon.png') : require('../images/createTimesheet/uncheckIcon.png')} /> */}
                  <Text style={{ fontSize: fontSizes.font16, color: "#333333", }}>
                    {item.projectName}
                  </Text>
                </View>
              </TouchableWithoutFeedback>
            }
          />
          <PickerDropdownModal
            pickerDropdownModalVisible={this.state.pickerDropdownModalVisible}
            pickerDropdownModalClose={() => this.setState({ pickerDropdownModalVisible: false })}
            titleText="Select Type"
            //titleText={this.state.PickerDropdownModalType == "Week" ? this.state.week : this.state.PickerDropdownModalType}
            pickerDropdownModalListData={this.state.pickerDropdownModalListData}
            pickerDropdownModalRow={({ item, index }) =>
              <TouchableWithoutFeedback key={"pickerDropdown" + index} onPress={() => this.pickerDropdownModalRowSelected(item)}>
                <View style={{ padding: 15, paddingLeft: 25, alignItems: "flex-start", justifyContent: "flex-start", borderBottomColor: "#E5E5E5", borderBottomWidth: 1 }}>
                  <Text style={{ fontSize: fontSizes.font18, color: "#333333" }}>
                    {item.projectName ? item.projectName : item.type ? item.type : item}
                  </Text>
                </View>
              </TouchableWithoutFeedback>
            }
          />




          <View style={styles.mainContainer}>
            {/* {this.timesheet_LeavesTab()} */}
            <TimeLeaveTab onTabPress={() => this.onTabPress()} tabPress={this.state.TabPress} />
            {/* {this.state.weekArray ? this.weekTabs() : null} */}
            <View style={{ height: 35 }}>
              <ScrollView
                pagingEnabled

                style={{ height: 35, backgroundColor: "#DADADA" }}
                showsHorizontalScrollIndicator={false}
                onChangeVisibleColumns={(visibleRows) => alert(visibleRows)}
                //onScroll={(event)=>this.scrollToPosition()}
                //scrollEventThrottle={16}
                //onScrollEndDrag={() => this.scrollToPosition()}
                ref={(scroller) => { this.scroller = scroller }}
                horizontal
                {...this.wrapperPanResponder.panHandlers}
              >
                {this.state.week_Date_Array_Middle_Index > -3 ? this.tabs() : null}
              </ScrollView>
            </View>

            <ScrollView
              key="scrollview" keyboardShouldPersistTaps="always" >
              <View style={[styles.projectListContainer, { paddingBottom: 50 }]}>
                {this.state.TabPress ?
                  // <CardView
                  //   style={{ backgroundColor: 'transparent' }}
                  //   cardElevation={2}

                  //   cardMaxElevation={2}
                  //   cornerRadius={2}>
                  <View style={styles.TimerContainer}>
                    <View style={styles.rowView}>
                      <Text style={[styles.ProjectHeaderText, { flex: 1, textAlign: "left" }]}>
                        Quick Log
                                    </Text>
                      <TouchableOpacity onPress={() => this.setState({ selectProjectModal: true })}
                        style={{ flex: 1, borderBottomColor: "#D8D8D8", borderBottomWidth: 1, flexDirection: "row", justifyContent: "space-between" }}>
                        <Text style={[styles.ProjectHeaderText, { color: 'black' }]}>
                          {this.state.selectProject}
                        </Text>
                        <Image style={{ marginTop: 4 }} source={require('../images/downArrow.png')} />
                      </TouchableOpacity>
                    </View>
                    <View style={[styles.rowView, { paddingTop: 10, marginTop: 10 }]}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <TouchableOpacity onPress={() => this.onTimerStart()}
                          style={{ flex: 1, alignItems: "center" }}>
                          <Image source={this.state.playPauseImage} style={{ marginTop: this.state.playPauseText == "Pause" ? 3 : this.state.playPauseText == "Play" ? 1 : null }} />
                          <Text style={[styles.ProjectHeaderText, { color: 'black', marginTop: 5 }]}>
                            {this.state.playPauseText}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.onTimerStop()}
                          style={{ flex: 1, alignItems: "center", marginTop: 2 }}>
                          <Image source={this.state.stopImage} />
                          <Text style={[styles.ProjectHeaderText, { color: 'black', marginTop: 5 }]}>
                            Stop
                                            </Text>
                        </TouchableOpacity>
                      </View>


                      <View style={{ flex: 1, alignItems: "flex-end", justifyContent: "center" }}>
                        <Text style={[styles.ProjectHeaderText, { color: 'black', fontSize: 32 }]}>
                          {/* {(Math.floor(this.state.second / 3600) > 9 ? Math.floor(this.state.second / 3600) : "0" + Math.floor(this.state.second / 3600)) + ":" +
                            (Math.floor(this.state.second / 60) > 9 ? Math.floor(this.state.second / 60) : "0" + Math.floor(this.state.second / 60)) +
                            ':' +
                            ('0' + this.state.second % 60).slice(-2)} */}
                          {(Math.floor(this.state.second / 3600) > 9 ? Math.floor(this.state.second / 3600) : "0" + Math.floor(this.state.second / 3600)) + ":" +
                            (Math.floor(this.state.minute) > 9 ? Math.floor(this.state.minute) : "0" + Math.floor(this.state.minute)) +
                            ':' +
                            ('0' + this.state.second % 60).slice(-2)}
                          {/* {(Math.floor(this.state.second / 3600) > 9 ? Math.floor(this.state.second / 3600) : "0" + Math.floor(this.state.second / 3600)) + ":" +
                            (('0' + this.state.second % 3600).slice(-2) > 9 ? ('0' + this.state.second % 3600).slice(-2) :  ('0' + this.state.second % 3600).slice(-2)) +
                            ':' +
                            ('0' + this.state.second % 60).slice(-2)} */}
                        </Text>
                      </View>
                    </View>
                  </View>
                  // </CardView>
                  : null}
                {!this.state.TabPress && this.state.leaveList.length == 0 ? null :
                  <View style={styles.headerView}>
                    <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                      <Text style={styles.ProjectHeaderText}>
                        {this.state.TabPress ? "Projects" : "On"}
                      </Text>
                      <Text style={styles.ProjectHeaderTime}>

                        {this.state.TabPress ? hourMinuteConverter(this.state.totalProjectHours) : this.state.totalLeaveDays > 1 ? this.state.totalLeaveDays + " Days" : this.state.totalLeaveDays + " Day"}

                      </Text>
                    </View>
                  </View>
                }
                {this.state.dataLoading ?
                  <Text style={styles.ProjectHeaderText}>
                    Loading
                    </Text>
                  :
                  <FlatList
                    data={this.state.TabPress ? this.state.projectList : this.state.leaveList}
                    renderItem={({ item, index }) => this.createList(item, index)}
                    keyExtractor={key => key.index}
                    extraData={this.state}
                  />
                }
                {!this.state.dataLoading ?
                  this.state.TabPress ? this.state.projectList.length == 0 ?
                    <Text style={[styles.ProjectHeaderText, { fontSize: fontSizes.font14, marginTop: 15, color: "#898989" }]}>
                      No project activity found for this time period
                    </Text>
                    : null
                    : this.state.leaveList.length == 0 ?
                      <Text style={[styles.ProjectHeaderText, { fontSize: fontSizes.font14, marginTop: 15, color: "#898989" }]}>
                        No leaves found for this time period
                  </Text> : null
                  : null
                }
              </View>
            </ScrollView>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#fff', justifyContent: "flex-start", },
  safeArea: {
    flex: 1,
    backgroundColor: '#21459E'
  },
  tabText: { fontSize: fontSizes.font12, color: "white", textAlign: 'center', },
  instructions: { textAlign: 'center', color: '#333333', marginBottom: 5, },
  mainContainer: { flex: 1, justifyContent: "center", alignItems: 'center', },
  tabView: { height: 40, width: width, flexDirection: 'row', backgroundColor: "#2A56C6" },
  weekTabView: { height: 35, width: width, flexDirection: 'row', backgroundColor: "#DADADA" },
  tabStyle: { alignItems: "center", justifyContent: "center" },
  projectListContainer: { margin: 10, width: width - 20, },
  ProjectHeaderText: { fontSize: fontSizes.font16, color: "#2A56C6", textAlign: 'center' },
  ProjectHeaderTime: { fontSize: fontSizes.font14, color: "#FF9800", textAlign: 'center' },
  headerView: { borderBottomWidth: .5, padding: 10, borderBottomColor: '#979797' },
  listView: { borderBottomWidth: .5, padding: 0, paddingTop: 5, paddingBottom: 5, borderBottomColor: '#BBBBBB' },
  rowView: { flexDirection: "row", justifyContent: "space-between", padding: 5 },
  projectColumnView: { flex: 1.5, justifyContent: "center", },
  //TimerContainer: { height: 100, width: width - 20, borderRadius: 6, borderWidth: .5, borderColor: "white", shadowOffset: { width: 0, height: 3, }, shadowColor: '#000', shadowOpacity: 0.8, shadowRadius: 5 }
  TimerContainer: { width: width - 20, borderRadius: 6, borderWidth: 0, paddingRight: 10, paddingBottom: 10, borderColor: "#E5E5E588", borderRadius: 5, padding: 5 }

});
