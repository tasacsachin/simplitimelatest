



/**
* Sample React Native App
* https://github.com/facebook/react-native
* @flow
*/

import React, { Component } from 'react';
import {

    StyleSheet, SafeAreaView,
    Text, FlatList,
    View, Image, TouchableOpacity, AsyncStorage,
} from 'react-native';
import Header from '../components/commonHeader'
import { dateConverterUseInHeader, hourMinuteConverter } from '../components/dateConverter'
import FloatingButton from '../components/floatingButton'
import { height, width, fontSizes, getLog } from '../utils/utils'
import webservice from '../components/webService'
import Spinner from '../components/spinner'
export default class projectDevelopment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            projectName: '', data: '', listArr: [],
            returnCallBack: { value: false, callBack: () => { } },
            userToken: '',
            spinnerVisible: false,
            totalSubActivitiesHours: 0,
            filterData: this.props.navigation.state.params.filterDataPassedOnProjectDevelopment
        }
    }
    componentWillMount() {
        AsyncStorage.getItem('userToken').then((token) => {
            this.setState({ userToken: token })
            this.hitGetProjectSubActivityListAPI(token, this.state.filterData.action, this.state.filterData.startDate, this.state.filterData.endDate)

        })

        var list = [
            { date: new Date().getTime(), hours: 120, timeTo: new Date().getTime(), timeFrom: new Date(), location: "Location", notes: "asgfdj asdgjsa asgd jsahasgd agd asgd a gdksgd asd gks gdks gdkas gdka gd" },

            { date: new Date().getTime(), hours: 76, timeTo: new Date().getTime(), timeFrom: new Date(), location: "Location", notes: "asgfdj asdgjsa asgd jsahasgd agd asgd a gdksgd asd gks gdks gdkas gdka gd" },

            { date: new Date().getTime(), hours: 18, timeTo: new Date().getTime(), timeFrom: new Date(), location: "Location", notes: "asgfdj asdgjsa asgd jsahasgd agd asgd a gdksgd asd gks gdks gdkas gdka gd" },
        ]
        // setTimeout(() => {
        //     alert(JSON.stringify(this.props.navigation.state.params))
        // }, 500)
        this.setState({ returnCallBack: { value: false, callBack: this.callBackCalledOnActivityAdd } })
        this.setState({ projectName: this.props.navigation.state.params.projectName, data: this.props.navigation.state.params.data })
    }

    hitGetProjectSubActivityListAPI(token, action, startDate, endDate) {
        let variables = new FormData();

        variables.append("action", action)
        variables.append("startDate", startDate)
        variables.append("projectId", this.state.data.projectId)
        variables.append("activityId", this.state.data.activityId)
        variables.append("page", 2)
        if (action == 'week') {
            variables.append("endDate", endDate)
        }

        this.setState({ spinnerVisible: true })
        //this.setState({ dataLoading: true })
        //dataLoading
        return webservice(variables, "timesheet/getsubactiviteslist", 'Post', token)
            .then(response => {
                this.setState({ spinnerVisible: false })
                if (response != "error") {
                    if (response.subAcvitiesList.length != 0) {

                        var arr = []

                        for (var i = 0; i < response.subAcvitiesList.length; i++) {
                            arr.push(
                                {
                                    "timesheetId": response.subAcvitiesList[i].timesheetId,
                                    "userId": response.subAcvitiesList[i].userId,
                                    'date': response.subAcvitiesList[i].addedDate,
                                    'timeFrom': Number(response.subAcvitiesList[i].startTime),
                                    'timeTo': Number(response.subAcvitiesList[i].endTime),
                                    'hours': Number(response.subAcvitiesList[i].totalHours),
                                    "activityId": response.subAcvitiesList[i].activityId,
                                    'billable': response.subAcvitiesList[i].billable,
                                    "location": response.subAcvitiesList[i].location,
                                    'notes': response.subAcvitiesList[i].notes,
                                    "latitude": response.subAcvitiesList[i].latitude,
                                    "longitude": response.subAcvitiesList[i].longitude,

                                })
                        }

                        this.totalHoursMinutesOfActivities(response.subAcvitiesList)

                        this.setState({ listArr: arr })

                    } else {
                        this.setState({ listArr: [] })
                    }
                    //this.totalNumberOfDaysLeave()
                    getLog("Sub activity list of activity of  project=====++++" + JSON.stringify(response))



                }

            })
    }
    totalHoursMinutesOfActivities(response) {

        var minutes = 0
        this.setState({ totalSubActivitiesHours: 0 })

        for (var i = 0; i < response.length; i++) {
            //getLog("==========++++++++++++=============")
            minutes = minutes + Number(response[i].totalHours)

        }


        this.setState({ totalSubActivitiesHours: minutes })

    }
    callBackCalledOnActivityAdd = (token, data) => {
        //alert(JSON.stringify(data))
        this.hitGetProjectSubActivityListAPI(token, data.action, data.startDate, data.endDate)
        // if (data.index != null && !data.edit) {
        //     alert("delete")
        //     var arr = this.state.listArr
        //     var newArr = []
        //     for (var i = 0; i < arr.length; i++) {
        //         if (i != data.index) {
        //             newArr.push(arr[i])
        //         }
        //     }

        //     this.setState({ listArr: newArr })
        // } else {
        //     if (data.edit) {
        //         var arr = this.state.listArr
        //         arr[data.index].date = new Date().getTime(),
        //             arr[data.index].hours = data.time,
        //             arr[data.index].timeTo = data.startTime,
        //             arr[data.index].timeFrom = data.endTime,
        //             arr[data.index].location = "Location",
        //             arr[data.index].notes = data.notes,

        //             setTimeout(() => {
        //                 this.setState({ listArr: arr })
        //             }, 300)


        //         alert("edit" + JSON.stringify(arr))
        //     } else {

        //         var arr = this.state.listArr
        //         arr.push({ date: new Date().getTime(), hours: data.time, timeTo: data.startTime, timeFrom: data.endTime, location: "Location", notes: data.notes })
        //         this.setState({ listArr: arr })
        //         alert("new" + JSON.stringify(data))

        //     }

        // }
        // this.state.ActivityList.push({ "activityName": data.activity, "Billable": data.billable, "time": data.time })
        // this.setState({ ActivityList: this.state.ActivityList })

        //this callback is called when we click on + floating icon and add a new activity that is added to this screen flatlist
    }

    onEditPress(item, index) {
        this.props.navigation.navigate("CreateTimesheet", {
            data: {
                projectName: this.props.navigation.state.params.projectName,
                projectId: this.props.navigation.state.params.projectId,
                otherData: item, activityName: this.state.data.activityName,
                activityId: this.state.data.activityId,
                index: index,
                edit: true,
                returnCallBack: this.state.returnCallBack,
                filterData: this.state.filterData

            }
        })
    }
    createList(item, index) {
        return (
            <View key={"view" + index}
                style={styles.listContainer}>
                <View style={[styles.rowView, { marginBottom: 5 }]}>
                    <View style={{ flex: 2 }}>
                        <Text style={styles.headerTextStyle}>{dateConverterUseInHeader(new Date(item.date))}</Text>
                        <Text style={[styles.headerTextStyle, { color: "#979797" }]}>{item.timeTo == '0' ? " " : ((new Date(item.timeTo).getHours() + ":" + new Date(item.timeTo).getMinutes()) + "-" + (new Date(item.timeFrom).getHours() + ":" + new Date(item.timeFrom).getMinutes()))}</Text>
                    </View>
                    <TouchableOpacity onPress={() => global.userType == 'Admin' ? null : this.onEditPress(item, index)}
                        style={styles.editView}>
                        <Text style={[styles.headerTextStyle, { color: "#979797", fontSize: fontSizes.font16 }]}>{hourMinuteConverter(item.hours)}</Text>
                        {global.userType == 'Admin' ? null :
                            <Image source={require('../images/projectDevelopmentIcons/editIcon.png')} />
                        }
                    </TouchableOpacity>
                </View>
                <View style={{ justifyContent: "flex-start", flexDirection: "row", marginHorizontal: 15 }}>

                    <Image source={require('../images/projectDevelopmentIcons/locationIcon.png')} />
                    <Text style={[styles.headerTextStyle, { color: "#979797", marginLeft: 10 }]}>{item.location}</Text>
                </View>
                <Text style={[styles.headerTextStyle, { margin: 10, marginHorizontal: 15 }]}>Notes</Text>
                <Text style={[styles.headerTextStyle, { marginHorizontal: 15, color: "#979797" }]}>{item.notes}</Text>
            </View>
        )
    }
    onFloatingButtonPressed() {
        this.props.navigation.navigate("CreateTimesheet", {
            data: {
                projectName: this.props.navigation.state.params.projectName,
                projectId: this.props.navigation.state.params.projectId,
                activityName: this.state.data.activityName,
                activityId: this.state.data.activityId,
                edit: false,
                returnCallBack: this.state.returnCallBack,
                filterData: this.state.filterData
            }
        })
    }
    render() {
        var projectName = this.state.projectName.length > 15 ? (this.state.projectName.slice(0, 15) + "..") : this.state.projectName

        return (
            <SafeAreaView style={styles.safeArea}>
                <Spinner visible={this.state.spinnerVisible} />
                <View style={styles.container}>
                    <Header
                        HeaderLeftText={projectName + " | " + this.state.data.activityName}
                        headerType='back'
                        HeaderRightText=''

                        toggleDrawer={() => this.props.navigation.goBack()}
                        //headerLeftIcon={require('../images/headerIcons/downArrow.png')}
                        headerRightIcon=''
                    //headerRightIconOnPress={} 
                    />
                    {global.userType == 'Admin' ? null :
                        <FloatingButton onButtonPressed={() => this.onFloatingButtonPressed()} icon={require('../images/floatPlusIcon.png')} />
                    }
                    <View style={styles.mainContainer}>
                        <View style={[styles.rowView, { borderBottomColor: "#E5E5E5", borderBottomWidth: 1, marginTop: 15, }]}>
                            <Text style={[styles.headerTextStyle, { fontSize: fontSizes.font18, color: "#2A56C6" }]}>On</Text>
                            <Text style={[styles.headerTextStyle, { fontSize: fontSizes.font18, color: "#FF9800", marginRight: 15 }]}>{hourMinuteConverter(this.state.totalSubActivitiesHours)}</Text>
                        </View>
                        <View style={{ flex: 1 }}>
                            <FlatList
                                data={this.state.listArr}
                                renderItem={({ item, index }) => this.createList(item, index)}
                                keyExtractor={key => key.index}
                            // extraData={this.state}
                            // onEndReachedThreshold={0.5}
                            //         onEndReached={({ distanceFromEnd }) => {
                            //              alert('on end reached ', distanceFromEnd);
                            //             // this.loadMore();
                            //          }}
                            />
                        </View>
                        {/* <View style={{ height: 80 }} /> */}
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, backgroundColor: '#fff'
    },
    safeArea: {
        flex: 1,
        backgroundColor: '#21459E'
    },
    mainContainer: { flex: 1 },
    listContainer: { borderBottomColor: "#E5E5E5", borderBottomWidth: 1, marginHorizontal: 10, marginBottom: 10, paddingBottom: 10 },
    editView: { flex: 1, justifyContent: "flex-end", flexDirection: "row" },
    rowView: { flexDirection: "row", justifyContent: "space-between", paddingBottom: 5, padding: 3, margin: 15, marginTop: 0, marginBottom: 10, },
    headerTextStyle: { fontSize: fontSizes.font14, color: "#333333", textAlign: 'left', marginRight: 10, marginBottom: 10 },
    listView: { borderBottomWidth: .5, marginHorizontal: 5, marginTop: 0, paddingBottom: 5, borderBottomColor: '#E5E5E5', marginRight: 10 },
    listRowView: { flexDirection: "row", justifyContent: "space-between", padding: 5 },
    projectColumnView: { flex: 4, justifyContent: "center", },
});
