/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  SafeAreaView,
  PanResponder,
  TextInput,
  Text,
  Keyboard,
  AsyncStorage,
  TouchableWithoutFeedback,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  FlatList
} from "react-native";

import Header from "../components/commonHeader";
import FloatingButton from "../components/floatingButton";
import ProjectModal from "../components/projectModal";
import Spinner from "../components/spinner";
import LeaveModal from "../components/leaveModal";
import { height, width, fontSizes, getLog } from "../utils/utils";
import AddEmployeeModal from "../components/addEmployeeModal";
import {
  dateConverter,
  dateConverterUseInHeader
} from "../components/dateConverter";
import webservice from "../components/webService";
var keyboardOpen = false;
var projectListArr = [];

export default class analysisExport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tabId: "",
      flatListdata: [],
      projectList: [],
      activitiList: [],
      leaveTypeList: [],
      projectModalVisible: false,
      projectNameModalText: "",
      projectCodeModalText: "",
      projectBillingModalText: 0,
      projectModalDatePickerVisible: false,
      datePickerType: "",
      startDateToShowOnMOdal: "",
      endDateToShowOnModal: "",
      startDate: "",
      endDate: "",
      projectModalTitleText: "New Project",
      leaveModalTitleText: "Add Leave",
      editedIndex: null,
      projectCurrencyModalText: "Currency",
      projectNameMandatory: false,
      projectNameMandatoryText: "",
      projectId: "",
      //leave modal states

      leaveModalVisible: false,
      totalDays: 0,
      reasonTextInputData: "",
      leaveTypeOnLeaveModal: "Leave Type",
      leaveTypeListPassOnModal: [],
      leaveBalance: "",
      leaveTypeTextInputDataOnModal: "",
      minDate: "",
      userToken: "",
      spinnerVisible: false,
      leaveModalErrorText: "",
      leaveTypeId: "",

      //employyee tab states
      addEmployeeModalVisible: false,
      employeeNameMandatory: false,
      employeeNameMandatoryText: "",
      fullNameValue: "",
      emailValue: "",
      addEmployeeModalType: "",
      activityValue: "",
      activityIdToEdit: "",
      addEmployeeModalTitle: "",
      updateListOnSomeEvent: false,

      ActiveStatusPassedOnModal: "Active",
      searchTextInputValue: "",
      //isActiveOfModal: '0',
      employeeDetailCallBack: { value: false, callBack: () => {} }
    };
  }
  hitAllAPI() {
    setTimeout(() => {
      this.setState({ spinnerVisible: false });
    }, 7000);
    AsyncStorage.getItem("userToken").then(token => {
      this.setState({ userToken: token });
      this.hitGetAllProjectListApi(token);
      this.hitGetAllActivitiesAPI(token);
      this.hitGetAllLeaveTypesAPI(token);
      if (global.userType == "Admin") {
        this.hitGetAllEmployeeAPI(token);
      }
    });
  }
  hitGetAllEmployeeAPI(token, searchText) {
    // var employeeArr = [
    //     { name: "Sachin", id: "abs@mail.com", status: 'Active' },
    //     { name: "asdas", id: "erteter@mail.com", status: 'Disabled' },
    //     { name: "Sacsadsadhin", id: "ertdfgdfg@mail.com", status: 'Active' },
    //     { name: "Sacdrfsdgsgdfghin", id: "hjhjhjhjh@mail.com", status: 'Active' },
    //     // { name: "Project XYZ", id: "AAFGT#$%^", costPerHour: 14, currency: "$", validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
    //     // { name: "Project XYZ", id: "", costPerHour: 14, currency: "$", validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
    //     // { name: "Project XYZ", costPerHour: 14, currency: "$", validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
    //     // { name: "Project XYZ", id: "", costPerHour: '', currency: "", validityFrom: "", validityTo: "" },
    //     // { name: "Project XYZ", id: "AAFGT#$%^", costPerHour: 14, currency: "$", validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
    //     // { name: "Project XYZ", id: "", costPerHour: 14, currency: "$", validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
    //     // { name: "Project XYZ", costPerHour: 14, currency: "$", validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
    //     // { name: "Project XYZ", id: "", costPerHour: '', currency: "", validityFrom: "", validityTo: "" },
    // ]
    // this.setState({ employeeList: employeeArr })

    let variables = new FormData();
    //this.setState({ spinnerVisible: true, projectNameMandatory: false })
    variables.append("page", 0);
    if (searchText) {
      if (searchText != "" && searchText != null) {
        variables.append("search", searchText);
      }
    }

    return webservice(variables, "employee/getlist", "POST", token).then(
      response => {
        this.setState({ spinnerVisible: false });
        if (response != "error") {
          if (response.employeeList.length != 0) {
            this.setState({ spinnerVisible: true });
            var arr = [];
            for (var i = 0; i < response.employeeList.length; i++) {
              arr.push({
                userId: response.employeeList[i].userId,
                name: response.employeeList[i].fullName,
                isActive:
                  response.employeeList[i].isActive == "0"
                    ? "Disabled"
                    : "Active",
                id: response.employeeList[i].email
              });
            }

            this.setState({ spinnerVisible: false, employeeList: arr });
            if (
              this.props.id == "employee" ||
              this.state.updateListOnSomeEvent
            ) {
              getLog("00000000`~~~~~~");
              this.setState({ flatListdata: arr, tabId: "Employee" });
            }

            // projectList
            // projectListArr=arr
          }
        } else {
          this.setState({ spinnerVisible: false });
        }
        getLog("Get All employee response=====++++" + JSON.stringify(response));
      }
    );
  }
  hitGetAllProjectListApi(token, searchText) {
    let variables = new FormData();
    //this.setState({ spinnerVisible: true, projectNameMandatory: false })
    variables.append("page", 0);
    if (searchText) {
      if (searchText != "" && searchText != null) {
        variables.append("search", searchText);
      }
    }
    return webservice(variables, "project/getlist", "POST", token).then(
      response => {
        this.setState({ spinnerVisible: false });
        if (response != "error") {
          if (response.projectList.length != 0) {
            this.setState({ spinnerVisible: true });
            var arr = [];
            for (var i = 0; i < response.projectList.length; i++) {
              arr.push({
                name: response.projectList[i].projectName,
                id: response.projectList[i].projectCode,
                costPerHour:
                  response.projectList[i].billing == 0
                    ? ""
                    : response.projectList[i].billing,
                currency: response.projectList[i].currency,
                validityFrom: response.projectList[i].startDate,
                validityTo: response.projectList[i].endDate,
                projectId: response.projectList[i].projectId,
                isActive:
                  response.projectList[i].isActive == "0"
                    ? "Disabled"
                    : "Active"
              });
            }

            this.setState({ spinnerVisible: false, projectList: arr });
            if (
              this.props.id == "manageProject" ||
              this.state.updateListOnSomeEvent
            ) {
              this.setState({ flatListdata: arr, tabId: "Projects" });
            }

            // projectList
            // projectListArr=arr
          }
          getLog("ProjectList=====++++" + JSON.stringify(arr));
        } else {
          getLog(
            "ProjectList err response=====++++" + JSON.stringify(response)
          );
          this.setState({ spinnerVisible: false });
        }
      }
    );
  }
  hitGetAllActivitiesAPI(token, searchText) {
    let variables = "";
    if (searchText) {
      if (searchText != "" && searchText != null) {
        variables = new FormData();
        variables.append("search", searchText);
      }
    }
    return webservice(variables, "activity/getlist", "Post", token).then(
      response => {
        this.setState({ spinnerVisible: false });
        if (response != "error") {
          if (response.activityList.length != 0) {
            var arr = [];
            for (var i = 0; i < response.activityList.length; i++) {
              arr.push({
                name: response.activityList[i].activityName,
                activityId: response.activityList[i].activityId,
                isActive:
                  response.activityList[i].isActive == "0"
                    ? "Disabled"
                    : "Active"
              });
            }
            this.setState({ spinnerVisible: false, activitiList: arr });
            if (
              this.props.id == "activityList" ||
              this.state.updateListOnSomeEvent
            ) {
              getLog("00000000`~~~~~~");
              this.setState({ flatListdata: arr, tabId: "Activities" });
            }
          }
          getLog("ActivityList=====++++" + JSON.stringify(response));
        }
      }
    );
  }
  hitGetAllLeaveTypesAPI(token, searchText) {
    let variables = "";
    if (searchText) {
      if (searchText != "" && searchText != null) {
        variables = new FormData();
        variables.append("search", searchText);
      }
    }
    return webservice(variables, "leavetype/getlist", "Post", token).then(
      response => {
        this.setState({ spinnerVisible: false });
        if (response != "error") {
          if (response.leaveTypeList.length != 0) {
            var arr = [];
            var leaveTypeArr = [];
            for (var i = 0; i < response.leaveTypeList.length; i++) {
              arr.push({
                name: response.leaveTypeList[i].leaveTypeName,
                leaveCount: response.leaveTypeList[i].balance,
                validityFrom: response.leaveTypeList[i].startDate,
                validityTo: response.leaveTypeList[i].endDate,
                leaveTypeId: response.leaveTypeList[i].leaveTypeId,
                isActive:
                  response.leaveTypeList[i].isActive == "0"
                    ? "Disabled"
                    : "Active"
              });
              leaveTypeArr.push(response.leaveTypeList[i].leaveTypeName);
            }

            this.setState({
              spinnerVisible: false,
              leaveTypeList: arr,
              leaveTypeListPassOnModal: leaveTypeArr
            });
            if (
              this.props.id == "manageLeaveTypes" ||
              this.state.updateListOnSomeEvent
            ) {
              this.setState({ flatListdata: arr, tabId: "Leave Types" });
            }
          }
          getLog("Leave typeList=====++++" + JSON.stringify(response));
        }
      }
    );
  }
  componentWillMount() {
    this.setState({ spinnerVisible: true });
    this.setState({
      employeeDetailCallBack: {
        value: false,
        callBack: this
          .callBackCalledWhenEmployeeDetailChangedOnEmployeeDetailPage
      }
    });
    this.hitAllAPI();

    // var activityListArr = [
    //     { name: "Metting", type: "activity" },
    //     { name: "Development", type: "activity" },
    //     { name: "Testing", type: "activity" },
    //     { name: "Deployment", type: "activity" },

    //     { name: "Metting", type: "activity" },
    //     { name: "Development", type: "activity" },
    //     { name: "Testing", type: "activity" },
    //     { name: "Deployment", type: "activity" },
    //     { name: "Metting", type: "activity" },
    //     { name: "Development", type: "activity" },
    //     { name: "Testing", type: "activity" },
    //     { name: "Deployment", type: "activity" },
    // ]

    // var leaveListArr = [
    //     { name: "Planned(PL)", leaveCount: 12, validityFrom: new Date(), validityTo: new Date() },
    //     { name: "Sick(PL)", leaveCount: 12, validityFrom: new Date(), validityTo: new Date() },
    //     { name: "Casual(CL)", leaveCount: 12, validityFrom: new Date(), validityTo: new Date() },
    //     { name: "Project XYZ", leaveCount: 12, validityFrom: new Date(), validityTo: new Date() },

    // ]

    // setTimeout(() => {
    //     this.setState({ flatListdata: this.state.projectList })
    // }, 300)
    if (this.props.id) {
      //alert(this.props.id)
      if (this.props.id == "activityList") {
        this.setState({
          flatListdata: this.state.activitiList,
          tabId: "Activities"
        });
      }
      if (this.props.id == "manageLeaveTypes") {
        this.setState({
          flatListdata: this.state.leaveTypeList,
          tabId: "Leave Types"
        });
      }
      if (this.props.id == "manageProject") {
        this.setState({
          flatListdata: this.state.projectList,
          tabId: "Projects"
        });
      }
      // if (this.props.id == 'employee') {
      //     this.setState({ flatListdata: this.state.employeeList })
      //     this.setState({ flatListdata: this.state.employeeList, tabId: "Employee" })
      // }
    }

    AsyncStorage.getItem("userToken").then(token => {
      this.setState({ userToken: token });
    });

    this.wrapperPanResponder = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,

      onPanResponderEnd: (e, { vx, dx, dy, x0 }) => {
        console.log("dx" + dx + "dy" + dy);
        if (dy > -30 && dy < 30) {
          if (dx < -30) {
            this.setState({ globalLeft: "right" });
            if (this.state.tabId == "Employee") {
              this.setState({ tabId: "Projects" });
              this.setState({ flatListdata: this.state.projectList });
            } else if (this.state.tabId == "Projects") {
              this.setState({ tabId: "Activities" });
              this.setState({ flatListdata: this.state.activitiList });
            } else if (this.state.tabId == "Activities") {
              this.setState({ tabId: "Leave Types" });
              this.setState({ flatListdata: this.state.leaveTypeList });
            } else {
            }
            console.log("right");
          }

          if (dx > 30) {
            this.setState({ globalLeft: "left" });
            if (this.state.tabId == "Leave Types") {
              this.setState({ tabId: "Activities" });
              this.setState({ flatListdata: this.state.activitiList });
            }
            if (this.state.tabId == "Activities") {
              this.setState({ tabId: "Projects" });
              this.setState({ flatListdata: this.state.projectList });
            }
            if (this.state.tabId == "Projects") {
              if (global.userType == "Admin") {
                this.setState({ tabId: "Employee" });
                this.setState({ flatListdata: this.state.employeeList });
              }
            }

            console.log("left");
          }
        }
        return true;
      },
      onShouldBlockNativeResponder: (evt, gestureState) => {
        return true;
      }
    });
  }

  callBackCalledWhenEmployeeDetailChangedOnEmployeeDetailPage = searchData => {
    if (searchData != "") {
      this.hitGetAllEmployeeAPI(this.state.userToken, searchData);
    }
  };
  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this.keyboardOpen
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      this.keyboardClose
    );
  }
  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }
  static navigationOptions = {
    drawerLabel: global.userType == "Admin" ? "Manage" : "Manage Projects",
    drawerIcon: ({ tintColor }) => (
      <Image source={require("../images/drawerIcons/manageProjectIcon.png")} />
    )
  };
  keyboardOpen() {
    keyboardOpen = true;
    //this.setState({ keyboardOpen: true })
  }

  keyboardClose() {
    keyboardOpen = false;
    //this.setState({ keyboardOpen: false })
  }

  filterFunction() {}
  onFloatingButtonPressed() {
    getLog("tab id on floating button pressed" + this.state.tabId);
    if (this.state.tabId == "Projects") {
      this.setState({
        projectModalTitleText: "New Project",
        projectNameModalText: "",
        projectCodeModalText: "",
        startDateToShowOnMOdal: "",
        endDateToShowOnModal: "",
        projectBillingModalText: "",
        projectCurrencyModalText: "Currency",
        projectNameMandatory: false,
        projectNameMandatoryText: "",
        ActiveStatusPassedOnModal: "Active"
      });
      setTimeout(() => {
        this.setState({ projectModalVisible: true });
      }, 300);
    }

    if (this.state.tabId == "Leave Types") {
      this.setState({
        leaveModalTitleText: "Add Leave Type",
        leaveTypeOnLeaveModal: "Leave Type",
        leaveTypeTextInputDataOnModal: "",
        leaveBalance: "",
        startDateToShowOnMOdal: "",
        endDateToShowOnModal: "",
        leaveModalErrorText: "",
        ActiveStatusPassedOnModal: "Active"
      });
      setTimeout(() => {
        this.setState({ leaveModalVisible: true });
      }, 300);
    }
    if (this.state.tabId == "Employee") {
      this.setState({
        fullNameValue: "",
        emailValue: "",
        activityValue: "",
        employeeNameMandatoryText: "",
        addEmployeeModalTitle: "Add Employee",
        ActiveStatusPassedOnModal: "Active"
      });
      setTimeout(() => {
        this.setState({
          addEmployeeModalVisible: true,
          addEmployeeModalType: "Add Employee"
        });
      }, 300);
    }
    if (this.state.tabId == "Activities") {
      this.setState({
        fullNameValue: "",
        emailValue: "",
        activityValue: "",
        activityNameOnAddActivityModal: "",
        employeeNameMandatoryText: "",
        addEmployeeModalTitle: "Add Activity",
        ActiveStatusPassedOnModal: "Active"
      });
      setTimeout(() => {
        this.setState({
          addEmployeeModalVisible: true,
          addEmployeeModalType: "Add Activity"
        });
      }, 300);
    }
  }
  onTabPress(tabId) {
    this.setState({ tabId: tabId });
    
      
     
     
    if (tabId == "Projects") {
      this.hitGetAllProjectListApi(this.state.userToken);
      this.setState({ flatListdata: this.state.projectList });
    }
    if (tabId == "Activities") {
      this.hitGetAllActivitiesAPI(this.state.userToken);
      this.setState({ flatListdata: this.state.activitiList });
    }
    if (tabId == "Leave Types") {
      this.hitGetAllLeaveTypesAPI(this.state.userToken);
      this.setState({ flatListdata: this.state.leaveTypeList });
    }
    if (tabId == "Employee") {
      this.hitGetAllEmployeeAPI(this.state.userToken);
      this.setState({ flatListdata: this.state.employeeList });
    }
  }
  makeTabs() {
    return (
      <View style={styles.tabView}>
        {/* tab for company admin */}

        {global.userType == "Admin" ? (
          <TouchableOpacity
            onPress={() => this.onTabPress("Employee")}
            style={[
              styles.tabStyle,
              {
                borderBottomColor: "white",
                borderBottomWidth: this.state.tabId == "Employee" ? 2 : 0
              }
            ]}
          >
            <Text
              style={[
                styles.tabText,
                {
                  fontSize:
                    global.userType == "admin"
                      ? fontSizes.font12
                      : fontSizes.font14
                }
              ]}
            >
              Employees
            </Text>
          </TouchableOpacity>
        ) : null}
        {/* first tab */}
        <TouchableOpacity
          onPress={() => this.onTabPress("Projects")}
          style={[
            styles.tabStyle,
            {
              borderBottomColor: "white",
              borderBottomWidth: this.state.tabId == "Projects" ? 2 : 0
            }
          ]}
        >
          <Text
            style={[
              styles.tabText,
              {
                fontSize:
                  global.userType == "admin"
                    ? fontSizes.font12
                    : fontSizes.font14
              }
            ]}
          >
            Projects
          </Text>
        </TouchableOpacity>
        {/* second tab */}
        <TouchableOpacity
          onPress={() => this.onTabPress("Activities")}
          style={[
            styles.tabStyle,
            {
              borderBottomColor: "white",
              borderBottomWidth: this.state.tabId == "Activities" ? 2 : 0
            }
          ]}
        >
          <Text
            style={[
              styles.tabText,
              {
                fontSize:
                  global.userType == "admin"
                    ? fontSizes.font12
                    : fontSizes.font14
              }
            ]}
          >
            Activities
          </Text>
        </TouchableOpacity>
        {/* third tab */}
        <TouchableOpacity
          onPress={() => this.onTabPress("Leave Types")}
          style={[
            styles.tabStyle,
            {
              borderBottomColor: "white",
              borderBottomWidth: this.state.tabId == "Leave Types" ? 2 : 0
            }
          ]}
        >
          <Text
            style={[
              styles.tabText,
              {
                fontSize:
                  global.userType == "admin"
                    ? fontSizes.font12
                    : fontSizes.font14
              }
            ]}
          >
            Leave Types
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
  //
  createList(item, index) {
    return (
      <TouchableWithoutFeedback
        onPress={() =>
          this.state.tabId == "Employee"
            ? this.props.navigation.navigate("EmployeeDetail", {
                data: item,
                employeeDetailCallBack: this.state.employeeDetailCallBack,
                searchText:
                  this.state.searchTextInputValue.length >= 3
                    ? this.state.searchTextInputValue
                    : ""
              })
            : null
        }
      >
        <View key={index + "View"} style={styles.listRowView}>
          <View style={{ flex: 6, marginLeft: 10, }}>
            <View style={{ flexDirection: "row" }}>
              <Text style={[styles.projectNameText, { flex: 3 }]}>
                {item.name}
              </Text>
              {/* {global.userType == "Admin" && item.isActive ? (
                <Text
                  style={[
                    styles.projectNameText,
                    {
                      color: item.isActive == "Active" ? "#06B15A" : "red",
                      flex: 1
                    }
                  ]}
                >
                  {item.isActive}
                </Text>
              ) : null} */}
            </View>
            {item.id ? (
              item.id != "" ? (
                <Text style={styles.otherText}>{item.id}</Text>
              ) : null
            ) : null}
            {item.leaveCount ? (
              item.leaveCount != "" ? (
                <Text style={styles.otherText}>
                  {item.leaveCount + " Leaves"}
                </Text>
              ) : null
            ) : null}
            {item.costPerHour ? (
              item.costPerHour != "" ? (
                <Text style={styles.otherText}>
                  {item.costPerHour + item.currency + " per hour"}
                </Text>
              ) : null
            ) : null}
            {item.validityFrom ? (
              item.validityFrom != "" && item.validityTo != "" ? (
                <Text style={styles.otherText}>
                  {"validity: " +
                    (item.validityFrom != ""
                      ? dateConverterUseInHeader(new Date(item.validityFrom))
                      : "") +
                    " - " +
                    (item.validityTo != ""
                      ? dateConverterUseInHeader(new Date(item.validityTo))
                      : "")}
                </Text>
              ) : null
            ) : null}
          </View>
          {global.userType == "Admin" && item.isActive ? (
            <View style={{ flex: 2.5, alignItems:'center',justifyContent:'center' }}>
             
              <Text
                style={[
                  
                  {
                    color: item.isActive == "Active" ? "#06B15A" : "red",
                    
                    fontSize:fontSizes.font14
                  }
                ]}
              >
                {item.isActive}
              </Text>
              </View>
           
          ) : null}
          {item.activityId && global.userType != "Admin" ? null : (
            <TouchableOpacity
              onPress={() =>
                this.state.tabId == "Employee"
                  ? this.props.navigation.navigate("EmployeeDetail", {
                      data: item,
                      employeeDetailCallBack: this.state.employeeDetailCallBack,
                      searchText:
                        this.state.searchTextInputValue.length >= 3
                          ? this.state.searchTextInputValue
                          : ""
                    })
                  : this.onEditPress(item, index)
              }
              style={{
                flex: 0.7,
                alignItems: "center",
                justifyContent:
                  this.state.tabId == "Employee" ? "center" : "center"
              }}
            >
              <Image
                source={
                  this.state.tabId == "Employee"
                    ? require("../images/rightArrow.png")
                    : require("../images/settingIcons/editIcon.png")
                }
                //style={{ marginTop: 5 }}
              />
            </TouchableOpacity>
          )}
        </View>
      </TouchableWithoutFeedback>
    );
  }

  startDateVisibleFunction() {
    this.setState({
      projectModalDatePickerVisible: true,
      datePickerType: "start",
      startDate: ""
    });
  }
  endDateVisibleFunction() {
    if (this.state.startDate == "") {
      alert("Please select start date first");
    } else {
      this.setState({
        projectModalDatePickerVisible: true,
        datePickerType: "end",
        minDate: this.state.startDate
      });
    }
  }
  getDateFromModal(date) {
    //alert(date)startDateToShowOnMOdal
    //this.setState({ minDate: date })
    if (this.state.datePickerType == "start") {
      this.setState({
        startDate: date,
        datePickerType: "",
        projectModalDatePickerVisible: false,
        endDateToShowOnModal: "",
        endDate: ""
      });
      var d = dateConverter(date);
      setTimeout(() => {
        this.setState({ startDateToShowOnMOdal: d });
      }, 200);
    }
    if (this.state.datePickerType == "end") {
      if (date < new Date(this.state.startDate)) {
        alert("End date should be greater than start sate");
      } else {
        this.setState({
          endDate: date,
          datePickerType: "",
          projectModalDatePickerVisible: false,
          minDate: ""
        });
        var d = dateConverter(date);
        setTimeout(() => {
          this.setState({ endDateToShowOnModal: d });
        }, 200);
      }
    }
  }
  onEditPress(item, index) {
    getLog("on edit pressed" + index + this.state.tabId);
    getLog("on edit pressed item" + JSON.stringify(item));
    if (this.state.tabId == "Projects") {
      this.setState({
        projectModalTitleText: "Edit Project",
        projectNameModalText: item.name,
        projectCodeModalText: item.id,
        startDateToShowOnMOdal:
          item.validityFrom != ""
            ? dateConverterUseInHeader(new Date(item.validityFrom))
            : "Start Date",
        endDateToShowOnModal:
          item.validityTo != ""
            ? dateConverterUseInHeader(new Date(item.validityTo))
            : "End Date",
        startDate: item.validityFrom,
        editedIndex: index,
        endDate: item.validityTo,
        projectBillingModalText: item.costPerHour,
        projectCurrencyModalText:
          item.currency == "" ? "Currency" : item.currency,
        projectId: item.projectId,
        projectNameMandatory: false,
        projectNameMandatoryText: "",
        ActiveStatusPassedOnModal: item.isActive
      });
      setTimeout(() => {
        this.setState({ projectModalVisible: true });
      }, 300);
    }

    if (this.state.tabId == "Leave Types") {
      this.setState({
        leaveModalTitleText: "Edit Leave Type",
        //leaveTypeOnLeaveModal: item.name,
        leaveTypeTextInputDataOnModal: item.name,
        leaveBalance: "" + item.leaveCount,
        editedIndex: index,
        leaveTypeId: item.leaveTypeId,
        startDateToShowOnMOdal:
          item.validityFrom != ""
            ? dateConverterUseInHeader(new Date(item.validityFrom))
            : "Start Date",
        startDate: item.validityFrom,
        endDate: item.validityTo,
        endDateToShowOnModal:
          item.validityTo != ""
            ? dateConverterUseInHeader(new Date(item.validityTo))
            : "End Date",
        leaveModalErrorText: ""
      });
      setTimeout(() => {
        this.setState({ leaveModalVisible: true });
      }, 300);
    }

    if (this.state.tabId == "Activities") {
      this.setState({
        fullNameValue: "",
        emailValue: "",
        activityValue: item.name,
        activityIdToEdit: item.activityId,
        employeeNameMandatoryText: "",
        addEmployeeModalTitle: "Edit Activity",
        ActiveStatusPassedOnModal: item.isActive
      });
      setTimeout(() => {
        this.setState({
          addEmployeeModalVisible: true,
          addEmployeeModalType: "Add Activity"
        });
      }, 300);
    }
  }
  closeProjectModal() {
    if (keyboardOpen) {
      Keyboard.dismiss();
    } else {
      this.setState({ projectModalVisible: false, minDate: "", startDate: "" });
    }
    //
  }
  closeLeaveModal() {
    if (keyboardOpen) {
      Keyboard.dismiss();
    } else {
      this.setState({ leaveModalVisible: false, minDate: "", startDate: "" });
    }
    //
  }

  onLeaveModalselectLeaveTypeClicked(index, value) {
    this.setState({ leaveTypeOnLeaveModal: value });
  }

  editProjectAPI() {
    var arr = this.state.projectList;
    let variables = new FormData();
    this.setState({
      spinnerVisible: true,
      projectNameMandatory: false,
      projectNameMandatoryText: ""
    });
    variables.append("projectName", this.state.projectNameModalText);
    variables.append("projectCode", this.state.projectCodeModalText);
    variables.append("projectId", this.state.projectId);
    variables.append(
      "isActive",
      this.state.ActiveStatusPassedOnModal == "Active" ? "1" : "0"
    );
    if (this.state.startDate != "") {
      variables.append("startDate", new Date(this.state.startDate).getTime());
    } else {
      variables.append("startDate", "");
    }
    if (this.state.endDate != "") {
      variables.append("endDate", new Date(this.state.endDate).getTime());
    } else {
      variables.append("endDate", "");
    }

    variables.append("billing", this.state.projectBillingModalText);
    variables.append(
      "currency",
      this.state.projectCurrencyModalText == "Currency"
        ? ""
        : this.state.projectCurrencyModalText
    );
    getLog("=====" + this.state.userToken);
    return webservice(
      variables,
      "project/update",
      "POST",
      this.state.userToken
    ).then(response => {
      this.setState({ spinnerVisible: false });
      if (response != "error") {
        arr[this.state.editedIndex].name = this.state.projectNameModalText;
        arr[this.state.editedIndex].id = this.state.projectCodeModalText;
        arr[this.state.editedIndex].costPerHour =
          "" + this.state.projectBillingModalText;
        arr[
          this.state.editedIndex
        ].currency = this.state.projectCurrencyModalText;
        arr[this.state.editedIndex].validityFrom = this.state.startDate;
        arr[this.state.editedIndex].validityTo = this.state.endDate;
        arr[
          this.state.editedIndex
        ].isActive = this.state.ActiveStatusPassedOnModal;

        this.setState({ projectList: arr });
        setTimeout(() => {
          this.setState({ projectModalVisible: false });
          this.setState({
            projectNameModalText: "",
            projectCodeModalText: "",
            spinnerVisible: false,
            projectBillingModalText: "",
            projectCurrencyModalText: "",
            startDate: "",
            endDate: "",
            projectNameMandatory: false,
            projectNameMandatoryText: "",
            ActiveStatusPassedOnModal: "Active"
          });
        }, 300);
      }
    });
  }
  addProjectAPI() {
    var arr = this.state.projectList;
    let variables = new FormData();
    this.setState({
      spinnerVisible: true,
      projectNameMandatory: false,
      projectNameMandatoryText: ""
    });
    variables.append("projectName", this.state.projectNameModalText);
    variables.append("projectCode", this.state.projectCodeModalText);
    variables.append(
      "isActive",
      this.state.ActiveStatusPassedOnModal == "Active" ? "1" : "0"
    );
    if (this.state.startDate != "") {
      variables.append("startDate", new Date(this.state.startDate).getTime());
    } else {
      variables.append("startDate", "");
    }
    if (this.state.endDate != "") {
      variables.append("endDate", new Date(this.state.endDate).getTime());
    } else {
      variables.append("endDate", "");
    }

    variables.append("billing", this.state.projectBillingModalText);
    variables.append(
      "currency",
      this.state.projectCurrencyModalText == "Currency"
        ? ""
        : this.state.projectCurrencyModalText
    );
    getLog("=====" + this.state.userToken);
    return webservice(
      variables,
      "project/add",
      "POST",
      this.state.userToken
    ).then(response => {
      this.setState({ spinnerVisible: false });
      if (response != "error") {
        arr.unshift({
          name: this.state.projectNameModalText,
          id: this.state.projectCodeModalText,
          costPerHour: "" + this.state.projectBillingModalText,
          currency: this.state.projectCurrencyModalText,
          validityFrom: this.state.startDate,
          validityTo: this.state.endDate,
          projectId: response.projectData.projectId,
          isActive: this.state.ActiveStatusPassedOnModal
        });
        this.setState({ projectList: arr });
        setTimeout(() => {
          this.setState({ projectModalVisible: false });

          this.setState({
            projectNameModalText: "",
            projectCodeModalText: "",
            spinnerVisible: false,
            projectBillingModalText: "",
            projectCurrencyModalText: "",
            startDate: "",
            endDate: "",
            projectNameMandatory: false,
            projectNameMandatoryText: "",
            ActiveStatusPassedOnModal: "Active"
          });
        }, 300);
      }
    });
  }
  projectModalSaveButtonClicked() {
    this.setState({ minDate: "" });
    var arr = this.state.projectList;

    if (this.state.projectModalTitleText == "Edit Project") {
      if (this.state.projectNameModalText != "") {
        if (
          this.state.projectBillingModalText == "" &&
          this.state.projectCurrencyModalText == "Currency"
        ) {
          this.editProjectAPI();
        } else if (
          this.state.projectBillingModalText != "" &&
          this.state.projectCurrencyModalText != "Currency"
        ) {
          this.editProjectAPI();
        } else {
          //alert(this.state.projectCurrencyModalText)
          this.state.projectBillingModalText == ""
            ? this.setState({
                projectNameMandatory: true,
                projectNameMandatoryText:
                  "*Please enter Billing rate in per hour."
              })
            : this.setState({
                projectNameMandatory: true,
                projectNameMandatoryText: "*Please enter currency."
              });
        }

        //alert(this.state.projectId)
      } else {
        this.setState({
          projectNameMandatory: true,
          projectNameMandatoryText: "*Please enter project name"
        });
      }
    } else {
      if (this.state.projectNameModalText != "") {
        if (
          this.state.projectBillingModalText == "" &&
          this.state.projectCurrencyModalText == "Currency"
        ) {
          this.addProjectAPI();
        } else if (
          this.state.projectBillingModalText != "" &&
          this.state.projectCurrencyModalText != "Currency"
        ) {
          this.addProjectAPI();
        } else {
          this.state.projectBillingModalText == ""
            ? this.setState({
                projectNameMandatory: true,
                projectNameMandatoryText:
                  "*Please enter Billing rate in per hour."
              })
            : this.setState({
                projectNameMandatory: true,
                projectNameMandatoryText: "*Please enter currency."
              });
        }
      } else {
        this.setState({
          projectNameMandatory: true,
          projectNameMandatoryText: "*Please enter project name"
        });
      }
    }

    // this.setState({ projectList: arr })
    // setTimeout(() => {
    //     this.setState({ projectModalVisible: false })
    //     this.setState({
    //         projectNameModalText: '',
    //         projectCodeModalText: '',
    //         spinnerVisible: false,
    //         projectBillingModalText: '',
    //         projectCurrencyModalText: '',
    //         startDate: '',
    //         endDate: ''
    //     })
    // }, 300)
  }
  hitAddLeaveTypeAPI() {
    var arr = this.state.leaveTypeList;
    let variables = new FormData();
    //this.setState({ spinnerVisible: true, projectNameMandatory: false })
    variables.append("leaveTypeName", this.state.leaveTypeTextInputDataOnModal);
    variables.append("balance", this.state.leaveBalance);
    variables.append("startDate", new Date(this.state.startDate).getTime());
    variables.append("endDate", new Date(this.state.endDate).getTime());
    variables.append(
      "isActive",
      this.state.ActiveStatusPassedOnModal == "Active" ? "1" : "0"
    );

    return webservice(
      variables,
      "leavetype/add",
      "POST",
      this.state.userToken
    ).then(response => {
      this.setState({ spinnerVisible: false });
      if (response != "error") {
        arr.unshift({
          name: this.state.leaveTypeTextInputDataOnModal,
          leaveCount: this.state.leaveBalance,
          validityFrom: this.state.startDate,
          validityTo: this.state.endDate,
          isActive: this.state.ActiveStatusPassedOnModal,
          leaveTypeId: response.leaveTypeData.leaveTypeId
        });

        this.setState({ leaveTypeList: arr });
        getLog("Leave type add response=====++++" + JSON.stringify(arr));
        setTimeout(() => {
          this.setState({ leaveModalVisible: false });
          this.setState({
            leaveTypeTextInputDataOnModal: "",
            leaveBalance: "",
            startDate: "",
            endDate: "",
            ActiveStatusPassedOnModal: "Active"
          });
        }, 300);
      } else {
        getLog("Leave type add response=====++++" + JSON.stringify(response));
      }
    });
  }
  hitEditLeaveTypeAPI() {
    var arr = this.state.leaveTypeList;
    let variables = new FormData();
    //this.setState({ spinnerVisible: true, projectNameMandatory: false })
    variables.append("leaveTypeName", this.state.leaveTypeTextInputDataOnModal);
    variables.append("balance", this.state.leaveBalance);
    variables.append("startDate", new Date(this.state.startDate).getTime());
    variables.append("endDate", new Date(this.state.endDate).getTime());
    variables.append("leaveTypeId", this.state.leaveTypeId);
    variables.append(
      "isActive",
      this.state.ActiveStatusPassedOnModal == "Active" ? "1" : "0"
    );

    return webservice(
      variables,
      "leavetype/edit",
      "POST",
      this.state.userToken
    ).then(response => {
      this.setState({ spinnerVisible: false });
      if (response != "error") {
        arr[
          this.state.editedIndex
        ].name = this.state.leaveTypeTextInputDataOnModal;
        arr[this.state.editedIndex].leaveCount = this.state.leaveBalance;
        arr[this.state.editedIndex].validityFrom = this.state.startDate;
        arr[this.state.editedIndex].validityTo = this.state.endDate;
        arr[
          this.state.editedIndex
        ].isActive = this.state.ActiveStatusPassedOnModal;

        this.setState({ leaveTypeList: arr });
        getLog("Leave type edit response=====++++" + JSON.stringify(arr));
        setTimeout(() => {
          this.setState({ leaveModalVisible: false });
          this.setState({
            leaveTypeTextInputDataOnModal: "",
            leaveBalance: "",
            startDate: "",
            endDate: "",
            ActiveStatusPassedOnModal: "Active"
          });
        }, 300);
      } else {
        getLog("Leave type edit response=====++++" + JSON.stringify(response));
      }
    });
  }
  hitAddEmployeeAPI() {
    let variables = new FormData();
    //this.setState({ spinnerVisible: true, projectNameMandatory: false })
    variables.append("employeeName", this.state.fullNameValue);
    variables.append("employeeEmail", this.state.emailValue);
    variables.append(
      "isActive",
      this.state.ActiveStatusPassedOnModal == "Active" ? "1" : "0"
    );

    this.setState({ spinnerVisible: true });
    return webservice(
      variables,
      "employee/add",
      "POST",
      this.state.userToken
    ).then(response => {
      this.setState({ spinnerVisible: false });

      if (response != "error") {
        this.hitGetAllEmployeeAPI(this.state.userToken);
        setTimeout(() => {
          this.setState({
            addEmployeeModalVisible: false,
            ActiveStatusPassedOnModal: "Active"
          });
        }, 300);
      } else {
        getLog("Add employee response=====++++" + JSON.stringify(response));
      }
    });
  }
  hitAddActivityAPI() {
    let variables = new FormData();
    variables.append("activityName", this.state.activityValue);
    variables.append(
      "isActive",
      this.state.ActiveStatusPassedOnModal == "Active" ? "1" : "0"
    );

    return webservice(
      variables,
      "activity/add",
      "POST",
      this.state.userToken
    ).then(response => {
      if (response != "error") {
        getLog("Activity added" + JSON.stringify(response));
        //this.hitGetAllActivitiesAPI(this.state.userToken)
        var arr = this.state.activitiList;

        arr.unshift({
          name: this.state.activityValue,
          activityId: response.activityData.activityId,
          isActive: this.state.ActiveStatusPassedOnModal
        });

        this.setState({ spinnerVisible: false, activitiList: arr });

        this.setState({ flatListdata: arr, tabId: "Activities" });

        setTimeout(() => {
          this.setState({
            addEmployeeModalVisible: false,
            ActiveStatusPassedOnModal: "Active"
          });
        }, 300);
      }
    });
  }
  hitEditActivityAPI() {
    let variables = new FormData();
    variables.append("activityName", this.state.activityValue);
    variables.append(
      "isActive",
      this.state.ActiveStatusPassedOnModal == "Active" ? "1" : "0"
    );
    variables.append("activityId", this.state.activityIdToEdit);
    return webservice(
      variables,
      "activity/edit",
      "POST",
      this.state.userToken
    ).then(response => {
      if (response != "error") {
        getLog("Activity added" + JSON.stringify(response));
        //this.hitGetAllActivitiesAPI(this.state.userToken)
        this.setState({ updateListOnSomeEvent: true });
        setTimeout(() => {
          this.hitGetAllActivitiesAPI(this.state.userToken);
        }, 100);

        // var arr = this.state.activitiList

        // arr.unshift({
        //     name: this.state.activityValue,
        //     activityId: response.activityData.activityId,
        //     isActive: this.state.ActiveStatusPassedOnModal
        // })

        // this.setState({ spinnerVisible: false, activitiList: arr })

        // this.setState({ flatListdata: arr, tabId: "Activities" })

        setTimeout(() => {
          this.setState({
            addEmployeeModalVisible: false,
            ActiveStatusPassedOnModal: "Active"
          });
        }, 300);
      }
    });
  }
  leaveModalSaveButtonClicked() {
    if (this.state.leaveTypeTextInputDataOnModal == "") {
      this.setState({ leaveModalErrorText: "*Please enter leave type" });
      return;
    } else if (this.state.leaveBalance == "") {
      this.setState({ leaveModalErrorText: "*Please enter leave balance" });
      return;
    } else if (this.state.startDate == "") {
      this.setState({ leaveModalErrorText: "*Please enter start date" });
      return;
    } else if (this.state.endDate == "") {
      this.setState({ leaveModalErrorText: "*Please enter end date" });
      return;
    } else {
      if (this.state.leaveModalTitleText == "Edit Leave Type") {
        this.hitEditLeaveTypeAPI();
      } else {
        this.hitAddLeaveTypeAPI();
      }

      //this.setState({ leaveTypeList: arr })
    }
  }
  onEmployeeDetailSavePress() {
    var emailRegex = /^[A-Z0-9_]+([\.][A-Z0-9_]+)*@[A-Z0-9-]+(\.[a-zA-Z]{2,4})+$/i;

    if (this.state.fullNameValue == "") {
      this.setState({
        employeeNameMandatory: true,
        employeeNameMandatoryText: "*Please enter employee name."
      });
      return;
    } else if (this.state.emailValue == "") {
      this.setState({
        employeeNameMandatory: true,
        employeeNameMandatoryText: "*Please enter employee email id."
      });
      return;
    } else if (!emailRegex.test(this.state.emailValue)) {
      this.setState({
        employeeNameMandatory: true,
        employeeNameMandatoryText: "*Please enter valid employee email id."
      });
      return;
    } else {
      this.hitAddEmployeeAPI();
    }
  }
  onActivityDetailSavePress() {
    if (this.state.activityValue == "") {
      this.setState({
        employeeNameMandatory: true,
        employeeNameMandatoryText: "*Please enter activity name."
      });
      return;
    } else {
      if (this.state.addEmployeeModalTitle == "Edit Activity") {
        this.hitEditActivityAPI();
      } else {
        this.hitAddActivityAPI();
      }
    }
  }
  onSwitchClickOfModal() {
    if (this.state.ActiveStatusPassedOnModal == "Active") {
      this.setState({ ActiveStatusPassedOnModal: "Disabled" });
    } else {
      this.setState({ ActiveStatusPassedOnModal: "Active" });
    }
  }
  makeSearchView() {
    return (
      <View
        style={{
          flexDirection: "row",
          borderBottomColor: "#E1E2E4",
          marginTop: 5,
          borderBottomWidth: 1.5,
          marginHorizontal: 20
        }}
      >
        <Image
          source={require("../images/searchIcon.png")}
          style={{ alignSelf: "center", marginRight: 5 }}
        />
        <TextInput
          style={{
            height: 40,
            flex: 1,
            color: "#000",
            alignSelf: "center",
            paddingLeft: 5
          }}
          placeholder="Search"
          value={this.state.searchTextInputValue}
          placeholderTextColor="#757575"
          underlineColorAndroid="transparent"
          autoCapitalize={false}
          autoCorrect={false}
          returnKeyType="search"
          //maxLength={placeholder == "Company ID" ? 5 : 100}
          //secureTextEntry={placeholder == "Password" ? true : false}
          onChangeText={text => this.onChangeSearchTextInputText(text)}
          //value={ProjectCodeValue}
        />
      </View>
    );
  }
  onChangeSearchTextInputText(text) {
    this.setState({ searchTextInputValue: text });
    getLog(text);
    if (text.length >= 3) {
      this.hitSearchApiWithText(text);
    }
    if (text.length == 0) {
      this.hitSearchApiWithoutAnyText();
    }
  }
  hitSearchApiWithText(searchText) {
    getLog(searchText);
    if (this.state.tabId == "Projects") {
      this.setState({ updateListOnSomeEvent: true });
      setTimeout(() => {
        this.hitGetAllProjectListApi(this.state.userToken, searchText);
      }, 100);
    } else if (this.state.tabId == "Activities") {
      this.setState({ updateListOnSomeEvent: true });
      setTimeout(() => {
        this.hitGetAllActivitiesAPI(this.state.userToken, searchText);
      }, 100);
    } else if (this.state.tabId == "Leave Types") {
      this.setState({ updateListOnSomeEvent: true });
      setTimeout(() => {
        this.hitGetAllLeaveTypesAPI(this.state.userToken, searchText);
      }, 100);
    } else if (this.state.tabId == "Employee") {
      this.setState({ updateListOnSomeEvent: true });
      setTimeout(() => {
        this.hitGetAllEmployeeAPI(this.state.userToken, searchText);
      }, 100);
    }
  }
  hitSearchApiWithoutAnyText() {
    if (this.state.tabId == "Projects") {
      this.setState({ updateListOnSomeEvent: true });
      setTimeout(() => {
        this.hitGetAllProjectListApi(this.state.userToken);
      }, 100);
    } else if (this.state.tabId == "Activities") {
      this.setState({ updateListOnSomeEvent: true });
      setTimeout(() => {
        this.hitGetAllActivitiesAPI(this.state.userToken);
      }, 100);
    } else if (this.state.tabId == "Leave Types") {
      this.setState({ updateListOnSomeEvent: true });
      setTimeout(() => {
        this.hitGetAllLeaveTypesAPI(this.state.userToken);
      }, 100);
    } else if (this.state.tabId == "Employee") {
      this.setState({ updateListOnSomeEvent: true });
      setTimeout(() => {
        this.hitGetAllEmployeeAPI(this.state.userToken);
      }, 100);
    }
  }
  makeCompanyDataView() {
    return (
      <View
        style={{
          paddingHorizontal: 20,
          paddingTop: 10,
          paddingBottom: 5,
          borderBottomColor: "#2A56C6",
          borderBottomWidth: 2
        }}
      >
        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
          <Text style={[styles.projectNameText]}>
            Active Employees :
            <Text
              style={[
                styles.projectNameText,
                {
                  color: "#06B15A",
                  fontWeight: "600"
                }
              ]}
            >
              {" " + "10"}
            </Text>
          </Text>
          <Text style={[styles.projectNameText]}>
            Disabled Employees :
            <Text
              style={[
                styles.projectNameText,
                { color: "red", fontWeight: "600" }
              ]}
            >
              {" " + "2"}
            </Text>
          </Text>
        </View>
        <Text style={[styles.projectNameText]}>
          Total Employees Quota :
          <Text
            style={[
              styles.projectNameText,
              {
                color: "#06B15A",
                fontWeight: "600"
              }
            ]}
          >
            {" " + "15"}
          </Text>
        </Text>
      </View>
    );
  }
  render() {
    return (
      <SafeAreaView style={styles.safeArea}>
        <View style={styles.container}>
          <Header
            HeaderLeftText={global.userType == "Admin" ? "Manage" : "Settings"}
            toggleDrawer={() => this.props.navigation.openDrawer()}
            //headerLeftIcon={require('../images/headerIcons/downArrow.png')}
            //headerRightIcon={require('../images/settingIcons/filterIcon.png')}

            HeaderRightText={global.userType == "Admin" ? "Report" : ""}
            headerRightIcon={
              global.userType == "Admin"
                ? require("../images/headerIcons/analysis.png")
                : ""
            }
            headerRightIconOnPress={() =>
              this.props.navigation.navigate("AnalysisExport")
            }
          />
          <Spinner visible={this.state.spinnerVisible} />
          <ProjectModal
            modalVisible={this.state.projectModalVisible}
            modalClose={() => this.closeProjectModal()}
            TitleText={this.state.projectModalTitleText}
            ProjectNameValue={this.state.projectNameModalText}
            projectNameChangeText={text =>
              this.setState({
                projectNameModalText: text,
                projectNameMandatory: false,
                projectNameMandatoryText: ""
              })
            }
            ProjectCodeValue={this.state.projectCodeModalText}
            projectCodeChangeText={text =>
              this.setState({ projectCodeModalText: text })
            }
            BillingValue={this.state.projectBillingModalText}
            billingTextInputData={text =>
              this.setState({ projectBillingModalText: text })
            }
            Currency={this.state.projectCurrencyModalText}
            saveButtonClicked={() => this.projectModalSaveButtonClicked()}
            datePickerVisible={this.state.projectModalDatePickerVisible}
            startDatePickerVisibleFunction={() =>
              this.startDateVisibleFunction()
            }
            datePickerCloseFunction={() =>
              this.setState({ projectModalDatePickerVisible: false })
            }
            endDatePickerVisibleFunction={() => this.endDateVisibleFunction()}
            dateSelected={date => this.getDateFromModal(date)}
            startDate={this.state.startDateToShowOnMOdal}
            endDate={this.state.endDateToShowOnModal}
            minDate={
              this.state.minDate == ""
                ? new Date("01/01/1990")
                : this.state.minDate
            }
            CurrencyOptionArrayOnProjectModal={["None", "$", "€", "£", "₹"]}
            selectCurrencyClicked={(index, value) =>
              this.setState({
                projectCurrencyModalText: value == "None" ? "Currency" : value
              })
            }
            projectNameMandatory={this.state.projectNameMandatory}
            projectNameMandatoryText={this.state.projectNameMandatoryText}
            onSwitchClickOfModal={() => this.onSwitchClickOfModal()}
            ActiveStatusPassedOnModal={this.state.ActiveStatusPassedOnModal}
          />
          <AddEmployeeModal
            modalVisible={this.state.addEmployeeModalVisible}
            modalType={this.state.addEmployeeModalType}
            modalClose={() => this.setState({ addEmployeeModalVisible: false })}
            employeeNameMandatory={this.state.employeeNameMandatory}
            employeeNameMandatoryText={this.state.employeeNameMandatoryText}
            activityChangeText={text => this.setState({ activityValue: text })}
            activityValue={this.state.activityValue}
            emailChangeText={text => this.setState({ emailValue: text })}
            emailValue={this.state.emailValue}
            fullNameChangeText={text => this.setState({ fullNameValue: text })}
            fullNameValue={this.state.fullNameValue}
            addEmployeeModalTitle={this.state.addEmployeeModalTitle}
            onEmployeeDetailSavePress={() =>
              this.state.addEmployeeModalType == "Add Employee"
                ? this.onEmployeeDetailSavePress()
                : this.onActivityDetailSavePress()
            }
            onSwitchClickOfModal={() => this.onSwitchClickOfModal()}
            ActiveStatusPassedOnModal={this.state.ActiveStatusPassedOnModal}
          />

          <LeaveModal
            modalVisible={this.state.leaveModalVisible}
            datePickerVisible={this.state.projectModalDatePickerVisible}
            datePickerVisibleFunction={() =>
              this.setState({ projectModalDatePickerVisible: true })
            }
            startDatePickerVisibleFunction={() =>
              this.startDateVisibleFunction()
            }
            endDatePickerVisibleFunction={() => this.endDateVisibleFunction()}
            dateSelected={date => this.getDateFromModal(date)}
            startDate={this.state.startDateToShowOnMOdal}
            endDate={this.state.endDateToShowOnModal}
            totalDays={this.state.totalDays}
            datePickerCloseFunction={() =>
              this.setState({ projectModalDatePickerVisible: false })
            }
            minDate={
              this.state.minDate == ""
                ? new Date("01/01/1990")
                : this.state.minDate
            }
            saveButtonClicked={() => this.leaveModalSaveButtonClicked()}
            reasonTextInputData={text =>
              this.setState({ reasonTextInputData: text })
            }
            modalClose={() => this.closeLeaveModal()}
            TitleText={this.state.leaveModalTitleText}
            TypePlaceHolder={this.state.leaveTypeOnLeaveModal}
            leavesOptionArrayOnLeaveModal={this.state.leaveTypeListPassOnModal}
            selectLeaveTypeClicked={(index, value) =>
              this.onLeaveModalselectLeaveTypeClicked(index, value)
            }
            balanceTextInputData={this.state.leaveBalance}
            balanceTextInputChange={text =>
              this.setState({ leaveBalance: text })
            }
            leaveTypeTextInputData={this.state.leaveTypeTextInputDataOnModal}
            leaveTypeTextInputChange={text =>
              this.setState({ leaveTypeTextInputDataOnModal: text })
            }
            errorText={this.state.leaveModalErrorText}
            onSwitchClickOfModal={() => this.onSwitchClickOfModal()}
            ActiveStatusPassedOnModal={this.state.ActiveStatusPassedOnModal}
          />
          {this.state.flatListdata == this.state.activitiList &&
          global.userType != "Admin" ? null : (
            <FloatingButton
              onButtonPressed={() => this.onFloatingButtonPressed()}
              icon={require("../images/floatPlusIcon.png")}
            />
          )}

          <View style={styles.mainContainer}>
            {this.makeTabs()}

            {global.userType == "Admin" ? this.makeSearchView() : null}
            {this.state.tabId == "Employee" ? this.makeCompanyDataView() : null}

            <ScrollView
              showsVerticalScrollIndicator={false}
              keyboardShouldPersistTaps="always"
              {...this.wrapperPanResponder.panHandlers}
            >
              <FlatList
                data={this.state.flatListdata}
                //data={this.state.tabId == "Projects" ? this.state.projectList : this.state.tabId == "Leave Types" ? this.state.leaveTypeList : this.state.activitiList}
                renderItem={({ item, index }) => this.createList(item, index)}
                keyExtractor={key => key.index}
                extraData={this.state}
                ref={ref => (this.flatList = ref)}
                keyboardShouldPersistTaps="always"
                showsVerticalScrollIndicator={false}
                // onEndReachedThreshold={10}
                // onEndReached={({ distanceFromEnd }) => {
                //     alert('on end reached ', distanceFromEnd)
                // }}
              />
              {this.state.tabId == "Activities" ? null : (
                <View style={{ height: 80 }} />
              )}
            </ScrollView>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: "#21459E"
  },
  container: { flex: 1, backgroundColor: "#fff" },
  mainContainer: { flex: 1 },
  tabView: {
    height: 40,
    width: width,
    flexDirection: "row",
    backgroundColor: "#2A56C6"
  },
  tabStyle: { flex: 1, alignItems: "center", justifyContent: "center" },
  weekTabView: {
    height: 20,
    width: width,
    flexDirection: "row",
    backgroundColor: "#979797"
  },
  tabText: { color: "white", textAlign: "center", fontWeight: "bold" },
  listRowView: {
    flex: 1,
    flexDirection: "row",
    padding: 10,
    borderBottomColor: "#E5E5E5",
    borderBottomWidth: 1
  },
  projectNameText: {
    fontSize: fontSizes.font14,
    color: "#333333",
    textAlign: "left",
    margin: 2
  },
  otherText: {
    fontSize: fontSizes.font12,
    color: "#979797",
    textAlign: "left",
    margin: 2
  }
});
