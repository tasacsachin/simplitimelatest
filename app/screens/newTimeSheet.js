
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet, AppState,
  Text, SafeAreaView, Keyboard, TouchableWithoutFeedback,
  View, Image, ScrollView, TouchableOpacity, FlatList,
  DeviceEventEmitter, AsyncStorage,
  NativeAppEventEmitter,
} from 'react-native';




import Header from '../components/commonHeader'
import FloatingButton from '../components/floatingButton'
import LeaveModal from '../components/leaveModal'
import PickerDropdownModal from '../components/pickerDropdownModal'
import BackgroundTimer from 'react-native-background-timer';
import RNAndroidBackgroundTimer from 'react-native-android-background-timer';
import TimeLeaveTab from '../components/timesheetLeaveTab'
import { height, width, fontSizes, getLog } from '../utils/utils'
import webservice from '../components/webService'
import Spinner from '../components/spinner'
const monthArr = ["January", "Feburary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
import { dateConverter, week_Date_Array, dateConverterUseInWeekTab, dateConverterUseInHeader, hourMinuteConverter } from '../components/dateConverter'
var interval
var keyboardOpen = false

const data = [
  1, 2, 3, 4, 5, 6, 7, 8
]
var intervalId;
var backgroundSecond = 0
export default class timeSheet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      appState: AppState.currentState,
      second: 0,
      minute: 0,
      TabPress: true,
      projectList: [],
      modalVisible: false,
      datePickerVisible: false, leaveType: 'Leave Type',
      leaveList: [],

      totalProjectHours: 0, totalLeaveDays: 0, totalDays: 0,
      returnCallBack: { value: false, callBack: () => { } },
      leaveTabCallBack: { value: false, callBack: () => { } },
      startDate: '', endDate: '', datePickerType: '', minDate: '', startDateToShowOnMOdal: '', endDateToShowOnModal: '',
      pickerDropdownModalVisible: false,
      selectProject: "Select Project",
      selectedProjectId: "",
      week: "Week",
      pickerDropdownModalTitleText: '',
      pickerDropdownModalListData: [],
      leaveTypeOnLeaveModal: 'Leave Type',
      leaveTypeList: [],
      leaveTypeListPassToModal: [],
      playPauseImage: '',
      play: false,
      playPauseText: '',
      stopImage: '',
      //week_Date_Array: week_Date_Array,
      week_Date_Array_Middle_Index: 26,
      weekArray: [],
      leaveModalErrorText: "", reasonTextInputData: '',
      spinnerVisible: false,
      userToken: '', projectListPassedToSelectProjectDropdown: []

    }
  }
  static navigationOptions = {
    drawerLabel: 'TimeSheet',
    drawerIcon: ({ tintColor }) => (
      <Image source={require('../images/drawerIcons/timesheetIcon.png')} />
    ),
  };


  hitAllAPI() {
    setTimeout(() => {
      this.setState({ spinnerVisible: false })
    }, 7000)
    AsyncStorage.getItem('userToken').then((token) => {
      this.setState({ userToken: token })
      this.hitGetAllLeaveTypesAPI(token)
      //this.hitGetLeaveListAPI(token,"week")
      //getLeaveListAPI is called in makeWeekDataTabArray
      this.makeWeekDateTabArray(token)
      this.hitGetAllProjectListApi(token)







    })

  }
  componentWillMount() {
    this.setState({ spinnerVisible: true })
    //this.makeWeekDateTabArray(this.state.week_Date_Array_Middle_Index)

    this.hitAllAPI()
    // this.makeWeekDateTabArray(this.state.week_Date_Array_Middle_Index)
    getLog(this.state.appState)
    var projectArr = [
      { "projectName": "ProjectXYZ asd asd asd asd asdasdasdasdsadadasdadasd ", 'projectId': 1, 'Activities': 2, 'progress': 80, 'time': 1874 },
      { "projectName": "ProjectXYZ", 'projectId': 2, 'Activities': 5, 'progress': 40, 'time': 200 },
      { "projectName": "ProjectXYZ", 'projectId': 3, 'Activities': 6, 'progress': 60, 'time': 120 },
      { "projectName": "ProjectXYZ", 'projectId': 4, 'Activities': 2, 'progress': 10, 'time': 140 },
      { "projectName": "ProjectXYZ", 'projectId': 5, 'Activities': 2, 'progress': 80, 'time': 145 },
      { "projectName": "ProjectXYZ", 'projectId': 6, 'Activities': 5, 'progress': 40, 'time': 330 },
      { "projectName": "ProjectXYZ", 'projectId': 7, 'Activities': 6, 'progress': 60, 'time': 485 },
      { "projectName": "ProjectXYZ", 'projectId': 8, 'Activities': 2, 'progress': 10, 'time': 55 },


    ]
    // var leaveArr = [
    //   { "validityFrom": new Date().getTime(), "validityTo": new Date().getTime(), 'type': "Sick Leave", 'status': "Approved", 'totalDays': 1, "reason": "These are the commen adsjhas adhkjhsad asdhbkjsa asdhas" },
    //   { "validityFrom": new Date().getTime(), "validityTo": new Date().getTime(), 'type': "Planned Leave", 'status': "Pending", 'totalDays': 1, "reason": "These are the commen adsjhas adhkjhsad asdhbkjsa asdhas" },
    //   { "validityFrom": new Date().getTime(), "validityTo": new Date().getTime(), 'type': "Sick Leave", 'status': "Not Approved", 'totalDays': 1, "reason": "These are the commen adsjhas adhkjhsad asdhbkjsa asdhas" },


    // ]
    var days = 0
    var leaveTypeArr = []

    if (this.props.id) {
      this.props.id == 'leaves' ? this.setState({ TabPress: false }) : null
    }
    this.totalHoursMinutesOfProjects()
    this.setState({ playPauseImage: this.state.second == 0 ? require('../images/timesheetIcon/playIcon.png') : require('../images/timesheetIcon/resumeIcon.png') })
    this.setState({ playPauseText: this.state.second == 0 ? 'Start' : 'Resume' })
    this.setState({ stopImage: this.state.second == 0 ? require('../images/timesheetIcon/greyStopIcon.png') : require('../images/timesheetIcon/redStopIcon.png') })

    this.setState({ projectList: projectArr, })
    this.setState({ returnCallBack: { value: false, callBack: this.callBackCalledOnActivityAdd } })
    this.setState({ leaveTabCallBack: { value: false, callBack: this.callBackCalledOnEditDeleteLeave } })
    this.setState({ week_Date_Array: week_Date_Array })

    this.updateSecondsOfTimerWhenControllerComesToThisScreen()
  }

  //this function is called when this component will unmount...means
  // navigate to other screen

  updateSecondsOfTimerWhenScreenChanges() {
    if (global.flag) {
      if (Platform.OS == 'ios') {
        if (this.state.play) {
          BackgroundTimer.clearInterval(intervalId);
          clearInterval(interval);
          backgroundSecond = 1
          intervalId = BackgroundTimer.setInterval(() => {
            backgroundSecond = backgroundSecond + 1
            //getLog(backgroundSecond)
          }, 1000);

        }
      } else {
        if (this.state.play) {
          BackgroundTimer.clearInterval(intervalId);
          clearInterval(interval);
          backgroundSecond = 1
          intervalId = RNAndroidBackgroundTimer.setInterval(() => {
            backgroundSecond = backgroundSecond + 1
            getLog(backgroundSecond)
          }, 1000);
        }
      }
    }
  }
  //this function is called in component will mount when comes to this screen
  updateSecondsOfTimerWhenControllerComesToThisScreen() {
    if (global.flag) {
      if (Platform.OS == 'ios') {
        BackgroundTimer.clearInterval(intervalId);
        
        this.setState({ second: global.backgroundSeconds, play: !global.play ? true : false })
        this.setState({ selectProject: global.selectedProject })
        setTimeout(() => {
          this.onTimerStart()
        }, 300)
        
        //backgroundSecond = 0
      } else {
        RNAndroidBackgroundTimer.clearInterval(intervalId);
        

        this.setState({ second: global.backgroundSeconds, play: !global.play ? true : false })
        this.setState({ selectProject: global.selectedProject })
        setTimeout(() => {
          this.onTimerStart()
        }, 300)
        
        //backgroundSecond = 0
      }
    }
  }


  componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange);
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardOpen);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardClose);
  }
  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();

    this.updateSecondsOfTimerWhenScreenChanges()
  }
  _handleAppStateChange = (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      //alert("App has come to the foreground!")
      if (Platform.OS == 'ios') {
        BackgroundTimer.clearInterval(intervalId);
        if (this.state.play) {

          this.setState({ second: this.state.second + backgroundSecond, play: false })
          this.onTimerStart()
        }
        backgroundSecond = 0
      } else {
        RNAndroidBackgroundTimer.clearInterval(intervalId);
        if (this.state.play) {

          this.setState({ second: this.state.second + backgroundSecond, play: false })
          this.onTimerStart()
        }
        backgroundSecond = 0
      }


    } else {
      global.flag=false
      // alert('App has come to the background!')
      if (Platform.OS == 'ios') {
        if (this.state.play) {
          BackgroundTimer.clearInterval(intervalId);
          clearInterval(interval);
          backgroundSecond = 1
          intervalId = BackgroundTimer.setInterval(() => {
            backgroundSecond = backgroundSecond + 1
            getLog(backgroundSecond)
          }, 1000);

        }
      } else {
        if (this.state.play) {
          BackgroundTimer.clearInterval(intervalId);
          clearInterval(interval);
          backgroundSecond = 1
          intervalId = RNAndroidBackgroundTimer.setInterval(() => {
            backgroundSecond = backgroundSecond + 1
            getLog(backgroundSecond)
          }, 1000);
        }
      }

    }
    this.setState({ appState: nextAppState });
  }
  keyboardOpen() {
    keyboardOpen = true
    //this.setState({ keyboardOpen: true })
  }

  keyboardClose() {
    keyboardOpen = false
    //this.setState({ keyboardOpen: false })
  }

  hitGetAllLeaveTypesAPI(token) {
    return webservice('', "leavetype/getlist", 'Post', token)
      .then(response => {
        this.setState({ spinnerVisible: false })
        if (response != "error") {
          if (response.leaveTypeList.length != 0) {
            var arr = []

            for (var i = 0; i < response.leaveTypeList.length; i++) {
              arr.push(response.leaveTypeList[i].leaveTypeName)
            }
            this.setState({ leaveTypeList: response.leaveTypeList, leaveTypeListPassToModal: arr })

          }
          getLog("Leave typeList=====++++" + JSON.stringify(response))



        }

      })
  }

  hitGetLeaveListAPI(token, action, startDate, endDate) {
    let variables = new FormData();

    variables.append("action", action)
    variables.append("startDate", startDate)
    if (action == 'week') {
      variables.append("endDate", endDate)
    }

    //this.setState({ spinnerVisible: true })
    this.setState({ dataLoading: true })
    //dataLoading
    return webservice(variables, "leave/getlist", 'Post', token)
      .then(response => {
        this.setState({ spinnerVisible: false, dataLoading: false })
        if (response != "error") {
          if (response.leaveList.length != 0) {

            var arr = []
            for (var i = 0; i < response.leaveList.length; i++) {
              arr.push(
                {
                  "validityFrom": response.leaveList[i].startDate,
                  "validityTo": response.leaveList[i].endDate,
                  'leaveId': response.leaveList[i].leaveId,
                  'leaveTypeId': response.leaveList[i].leaveTypeId,
                  "type": response.leaveList[i].leaveTypeName,
                  "status": response.leaveList[i].status,
                  'totalDays': response.leaveList[i].days,
                  "reason": "These are the commen adsjhas adhkjhsad asdhbkjsa asdhas"
                })
            }

            this.setState({ leaveList: arr })
            //this.makeWeekDateTabArray(this.state.week_Date_Array_Middle_Index)
          } else {
            this.setState({ leaveList: [] })
          }
          this.totalNumberOfDaysLeave()
          getLog("Leave List of time sheet page=====++++" + JSON.stringify(response))



        }

      })

  }
  hitGetAllProjectListApi(token) {
    let variables = new FormData();
    //this.setState({ spinnerVisible: true, projectNameMandatory: false })
    variables.append("page", 0)

    return webservice(variables, "project/getlist", 'POST', token)
      .then(response => {
        this.setState({ spinnerVisible: false })
        if (response != "error") {
          if (response.projectList.length != 0) {
            this.setState({ projectListPassedToSelectProjectDropdown: response.projectList })

          }
          getLog("ProjectList=====++++" + JSON.stringify(response.projectList))
        } else {
          getLog("ProjectList err response=====++++" + JSON.stringify(response))
          this.setState({ spinnerVisible: false })
        }

      })
  }
  onTabPress() {
    this.setState({ TabPress: !this.state.TabPress })
  }

  navigateToLeaveDescription(item, index) {
    this.props.navigation.navigate('LeaveDescription', { data: item, leaveTabCallBack: this.state.leaveTabCallBack, index: index, leaveTypeListPassToModal: this.state.leaveTypeListPassToModal, leaveTypeList: this.state.leaveTypeList })
  }
  navigateToProjectBreakdown(item) {
    this.props.navigation.navigate('ProjectBreakdown', { data: item })
  }
  createList(item, index) {
    // var progress = item.progress + "%"
    // var left = (100 - item.progress) + "%"
    var progress = Math.round((item.time / this.state.totalProjectHours) * 100)

    var left = Math.round(100 - progress) + "%"
    progress = progress + '%'
    //getLog(progress+"===="+left)

    return (
      <TouchableWithoutFeedback onPress={() => this.state.TabPress ? this.navigateToProjectBreakdown(item) : this.navigateToLeaveDescription(item, index)}
        key={index + "rowListProject"} style={styles.listView}>
        <View style={[styles.listView, {}]}>
          <View style={[styles.rowView, { paddingLeft: 0, paddingRight: 0 }]}>
            <View style={styles.projectColumnView}>
              <Text numberOfLines={1} style={{ fontSize: fontSizes.font14 }}>
                {this.state.TabPress ? item.projectName :
                  (dateConverterUseInWeekTab(new Date(item.validityFrom)) + "-" + dateConverterUseInWeekTab(new Date(item.validityTo)))}
              </Text>
              <Text style={{ fontSize: fontSizes.font14, color: "#BBBBBB", paddingTop: 5 }}>
                {this.state.TabPress ? item.Activities + " Activities" : item.type}
                {this.state.TabPress ? null :
                  <Text> | </Text>
                }
                {this.state.TabPress ? null :
                  <Text style={{
                    fontSize: fontSizes.font14,
                    color: item.status == 0 ? "red" : item.status == 1 ? "green" : "grey",
                    paddingTop: 5
                  }}> {item.status == 0 ? "Pending" : item.status == 1 ? "Approved" : "Rejected"}</Text>
                }
              </Text>
            </View>
            {this.state.TabPress ? <View style={[styles.projectColumnView, { flex: 2, marginHorizontal: 5, flexDirection: "row", alignItems: "center" }]}>
              <View style={{ width: progress, backgroundColor: "#2A56C6", height: 4, }} />
              <View style={{ width: left, backgroundColor: "#D8D8D8", height: 4 }} />
            </View> : null}
            <View style={[styles.projectColumnView, { flex: this.state.TabPress ? 1.5 : 0.5, flexDirection: "row", justifyContent: "flex-end", alignItems: "center" }]}>

              <Text style={{ fontSize: fontSizes.font16, color: "#6C6C6C", marginRight: 10 }}>
                {this.state.TabPress ? hourMinuteConverter(item.time) : item.totalDays > 1 ? item.totalDays + " Days" : item.totalDays + " Day"}
              </Text>
              <Image source={require('../images/rightArrow.png')} />

            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    )
  }

  // timesheet_LeavesTab() {
  //   return (
  //     <View style={styles.tabView}>
  //       {/* first tab */}
  //       <TouchableOpacity onPress={() => this.onTabPress()}
  //         style={[styles.tabStyle, { borderBottomColor: "white", borderBottomWidth: this.state.TabPress ? 2 : 0 }]} >
  //         <Text style={styles.tabText}>
  //           Timesheet
  //             </Text>
  //       </TouchableOpacity>
  //       {/* second tab */}
  //       <TouchableOpacity onPress={() => this.onTabPress()}
  //         style={[styles.tabStyle, { borderBottomColor: "white", borderBottomWidth: this.state.TabPress ? 0 : 2 }]}   >
  //         <Text style={styles.tabText}>
  //           Leaves
  //                           </Text>
  //       </TouchableOpacity>
  //       {/* third tab */}
  //       <View style={styles.tabStyle} >
  //       </View>
  //     </View>
  //   )
  // }
  onWeekTabPressLeft() {
    if (this.state.week == "Week") {
      if (this.state.week_Date_Array_Middle_Index > 1) {

        this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index - 1 })
        setTimeout(() => {
          this.makeWeekDateTabArray(this.state.userToken)
        }, 300)
      }
    } else if (this.state.week == "Month") {
      if (this.state.week_Date_Array_Middle_Index > 1) {
        console.log("month=======" + this.state.week_Date_Array_Middle_Index)
        this.hitGetLeaveListAPI(this.state.userToken, "month", monthArr.indexOf(monthArr[this.state.week_Date_Array_Middle_Index + 1]) - 1)

        this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index - 1 })
        setTimeout(() => {
          this.setState({ weekArray: [monthArr[this.state.week_Date_Array_Middle_Index - 1], monthArr[this.state.week_Date_Array_Middle_Index], monthArr[this.state.week_Date_Array_Middle_Index + 1]] })
        }, 300)
      }
    } else {
      this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index - 1 })

      setTimeout(() => {
        this.hitGetLeaveListAPI(this.state.userToken, "year", this.state.week_Date_Array_Middle_Index)
        this.setState({ weekArray: [this.state.week_Date_Array_Middle_Index - 1, this.state.week_Date_Array_Middle_Index, this.state.week_Date_Array_Middle_Index + 1] })
      }, 300)
    }



  }
  onWeekTabPressRight() {
    if (this.state.week == "Week") {
      if (this.state.week_Date_Array_Middle_Index < 51) {
        this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index + 1 })
        setTimeout(() => {
          this.makeWeekDateTabArray(this.state.userToken)
        }, 300)
      }
    } else if (this.state.week == "Month") {
      if (this.state.week_Date_Array_Middle_Index < 10) {
        console.log("month=======" + this.state.week_Date_Array_Middle_Index)
        this.hitGetLeaveListAPI(this.state.userToken, "month", monthArr.indexOf(monthArr[this.state.week_Date_Array_Middle_Index + 1]) + 1)
        //alert(monthArr[this.state.week_Date_Array_Middle_Index + 1])
        //alert(monthArr.indexOf(monthArr[this.state.week_Date_Array_Middle_Index + 1]))
        this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index + 1 })
        setTimeout(() => {
          this.setState({ weekArray: [monthArr[this.state.week_Date_Array_Middle_Index - 1], monthArr[this.state.week_Date_Array_Middle_Index], monthArr[this.state.week_Date_Array_Middle_Index + 1]] })
        }, 300)
      }
    } else {
      this.setState({ week_Date_Array_Middle_Index: this.state.week_Date_Array_Middle_Index + 1 })
      setTimeout(() => {
        this.hitGetLeaveListAPI(this.state.userToken, "year", this.state.week_Date_Array_Middle_Index)

        this.setState({ weekArray: [this.state.week_Date_Array_Middle_Index - 1, this.state.week_Date_Array_Middle_Index, this.state.week_Date_Array_Middle_Index + 1] })
      }, 300)
    }
  }
  makeWeekDateTabArray(token) {
    var index = this.state.week_Date_Array_Middle_Index
    var weekArray = []
    weekArray.push(dateConverterUseInWeekTab(new Date(week_Date_Array[index - 1].startDate)) + "-" + dateConverterUseInWeekTab(new Date(week_Date_Array[index - 1].endDate)))
    weekArray.push(dateConverterUseInWeekTab(new Date(week_Date_Array[index].startDate)) + "-" + dateConverterUseInWeekTab(new Date(week_Date_Array[index].endDate)))
    weekArray.push(dateConverterUseInWeekTab(new Date(week_Date_Array[index + 1].startDate)) + "-" + dateConverterUseInWeekTab(new Date(week_Date_Array[index + 1].endDate)))
    this.setState({ weekArray: weekArray })

    //alert(new Date(week_Date_Array[index].startDate) + new Date(week_Date_Array[index].endDate))
    this.hitGetLeaveListAPI(token, "week", new Date(week_Date_Array[index].startDate).getTime(), new Date(week_Date_Array[index].endDate).getTime())


    // this.sortLeaveListByWeek(index)
  }
  // sortLeaveListByWeek(index) {
  //   var arr = []
  //   for (var i = 0; i < this.state.leaveList.length; i++) {
  //     if ((this.state.leaveList[i].validityTo >= week_Date_Array[index].startDate) && (this.state.leaveList[i].validityFrom <= week_Date_Array[index].endDate)) {
  //       arr.push(this.state.leaveList[i])
  //     }
  //   }
  //  // alert(JSON.stringify(this.state.leaveList[0]))
  //   getLog("==="+JSON.stringify(arr))
  //   this.setState({ leaveList: arr })
  // }
  pickerDropdownModalRowSelected(item) {
    if (this.state.PickerDropdownModalType == "Select Project") {
      this.setState({ selectProject: item.projectName, selectedProjectId: item.projectId })

    } else if (this.state.PickerDropdownModalType == 'Select Leave Type') {
      this.setState({ leaveTypeOnLeaveModal: item.type })
    }
    else {
      this.setState({ week: item })

      if (item == "Week") {
        this.setState({ week_Date_Array_Middle_Index: 26 })
        setTimeout(() => {
          this.makeWeekDateTabArray(this.state.userToken)
        }, 300)

      } else if (item == "Month") {

        var month = new Date().getMonth()
        this.setState({ week_Date_Array_Middle_Index: month })
        if (month > 0 && month < 11) {
          setTimeout(() => {
            this.setState({ weekArray: [monthArr[month - 1], monthArr[month], monthArr[month + 1]] })
          }, 300)
        } else {
          if (month == 0) {
            setTimeout(() => {
              this.setState({ weekArray: [monthArr[month], monthArr[month + 1], monthArr[month + 2]], week_Date_Array_Middle_Index: 1 })
            }, 300)
          } else {
            setTimeout(() => {
              this.setState({ weekArray: [monthArr[month - 2], monthArr[month - 1], monthArr[month]], week_Date_Array_Middle_Index: 10 })
            }, 300)
          }

        }
        setTimeout(() => {
          this.hitGetLeaveListAPI(this.state.userToken, "month", monthArr.indexOf(monthArr[this.state.week_Date_Array_Middle_Index + 1]))
        }, 300)


      } else {
        var year = new Date().getFullYear()
        this.setState({ week_Date_Array_Middle_Index: year })
        setTimeout(() => {
          this.hitGetLeaveListAPI(this.state.userToken, "year", this.state.week_Date_Array_Middle_Index)
          this.setState({ weekArray: [this.state.week_Date_Array_Middle_Index - 1, this.state.week_Date_Array_Middle_Index, this.state.week_Date_Array_Middle_Index + 1] })
        }, 300)
      }

    }
    this.setState({ pickerDropdownModalVisible: false })
  }
  weekTabs() {
    return (
      <View style={styles.weekTabView}>
        {/* first tab */}
        <TouchableWithoutFeedback onPress={() => this.onWeekTabPressLeft()}>
          <View
            style={[styles.tabStyle]} >
            <Text style={[styles.tabText, { color: "#2A56C6" }]}>
              {this.state.weekArray[0]}
            </Text>
          </View>
        </TouchableWithoutFeedback>
        {/* second tab */}
        <TouchableWithoutFeedback >
          <View
            style={[styles.tabStyle]}   >
            <Text style={[styles.tabText, { color: "#000", fontWeight: "bold" }]}>
              {this.state.weekArray[1]}
            </Text>
          </View>
        </TouchableWithoutFeedback>
        {/* third tab */}
        <TouchableWithoutFeedback onPress={() => this.onWeekTabPressRight()}>
          <View style={[styles.tabStyle]}   >
            <Text style={[styles.tabText, { color: "#2A56C6" }]}>
              {this.state.weekArray[2]}
            </Text>
          </View>
        </TouchableWithoutFeedback>
      </View>
    )
  }
  onFloatingButtonPressed() {
    !this.state.TabPress ?
      this.setState({
        modalVisible: true, leaveModalErrorText: '',
        startDate: '',
        endDate: '',
        leaveTypeOnLeaveModal: 'Leave Type',
        totalDays: "" + 0,
        reasonTextInputData: '',
        startDateToShowOnMOdal: '',
        endDateToShowOnModal: '',
        leaveModalErrorText: ''
      })
      : this.props.navigation.navigate("CreateTimesheet", {
        data: {
          returnCallBack: this.state.returnCallBack,
          //projectName: this.state.selectProject,
        }
      })
  }
  callBackCalledOnActivityAdd = (data) => {
    //alert(data)
    this.state.projectList.push({ "projectName": data.projectName, 'Activities': data.activity, 'progress': 0, 'time': data.time })
    this.setState({ projectList: this.state.projectList })

    //this callback is called when we click on + floating icon and add a new activity that is added to this screen flatlist
  }
  callBackCalledOnEditDeleteLeave = (data) => {
    if (data.type == "delete") {
      var arr = []
      for (var i = 0; i < this.state.leaveList.length; i++) {
        if (i != data.index) {
          arr.push(this.state.leaveList[i])
        }
      }
      this.setState({ leaveList: arr })
    } else {

      var arr = this.state.leaveList
      arr[data.index].validityFrom = data.editedData.validityFrom
      arr[data.index].validityTo = data.editedData.validityTo
      arr[data.index].status = data.editedData.status
      arr[data.index].totalDays = data.editedData.totalDays
      arr[data.index].reason = data.editedData.reason
      arr[data.index].type = data.editedData.type

      this.setState({ leaveList: arr })

    }
    this.totalNumberOfDaysLeave()


    //this callback is called when we click on delete button of leave description scren and edit leave description on leave description screen
  }
  getDateFromModal(date) {
    getLog("getDateFromModal" + date)
    if (this.state.datePickerType == 'start') {
      this.setState({ startDate: date, datePickerType: '', minDate: date, datePickerVisible: false, endDateToShowOnModal: "", endDate: '' })
      var d = dateConverterUseInHeader(date)
      setTimeout(() => {
        this.setState({ startDateToShowOnMOdal: d })
      }, 200)
    } if (this.state.datePickerType == 'end') {
      this.setState({ endDate: date, datePickerType: '', datePickerVisible: false, })
      var d = dateConverterUseInHeader(date)
      setTimeout(() => {
        this.setState({ endDateToShowOnModal: d })
        this.totalDaysCalculate()
      }, 200)

    }


  }
  totalDaysCalculate() {
    getLog(this.state.endDate)
    var date1 = new Date(this.state.startDate);
    var date2 = new Date(this.state.endDate);
    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)) + 1;
    this.daysTextInputChangeOnLeaveModal("" + diffDays)
    // this.setState({ totalDays: diffDays + 1 })
    // setTimeout(() => {
    //   console.log(this.state.totalDays)
    // }, 300)



  }
  startDateVisibleFunction() {
    this.setState({ datePickerVisible: true, datePickerType: 'start', minDate: '' })
  }
  endDateVisibleFunction() {
    if (this.state.startDate == '') {
      alert("Please select start date first")
    } else {
      this.setState({ datePickerVisible: true, datePickerType: 'end', minDate: this.state.minDate })
    }

  }
  totalNumberOfDaysLeave() {
    setTimeout(() => {
      var days = 0
      for (var i = 0; i < this.state.leaveList.length; i++) {
        getLog(this.state.leaveList.length + "====" + this.state.leaveList[i].totalDays)
        days = days + parseFloat(this.state.leaveList[i].totalDays)

      }

      this.setState({ totalLeaveDays: days })
    }, 300)
  }
  totalHoursMinutesOfProjects() {
    setTimeout(() => {
      var minutes = 0
      for (var i = 0; i < this.state.projectList.length; i++) {
        //getLog(this.state.leaveList.length + "====" + this.state.leaveList[i].totalDays)
        minutes = minutes + this.state.projectList[i].time

      }
      // alert(minutes2349)
      this.setState({ totalProjectHours: minutes })
    }, 300)
  }

  hitAddLeaveAPI() {
    var arr = this.state.leaveList
    let variables = new FormData();
    //this.setState({ spinnerVisible: true, projectNameMandatory: false })
    variables.append("leaveTypeId", this.state.leaveTypeId)
    variables.append("days", this.state.totalDays)
    variables.append("startDate", new Date(this.state.startDate).getTime())
    variables.append("endDate", new Date(this.state.endDate).getTime())
    variables.append("reason", this.state.reasonTextInputData)

    this.setState({ spinnerVisible: true })
    return webservice(variables, "leave/add", 'POST', this.state.userToken)
      .then(response => {
        this.setState({ spinnerVisible: false })
        if (response != "error") {
          // var arr = this.state.leaveList
          arr.push(
            {
              "validityFrom": this.state.startDate,
              "validityTo": this.state.endDate,
              'type': this.state.leaveTypeOnLeaveModal,
              'status': 0,
              'totalDays': this.state.totalDays,
              "reason": this.state.reasonTextInputData,
              "leaveTypeId": this.state.leaveTypeId
            }
          )
          this.setState({ leaveList: arr })
          //this.totalNumberOfDaysLeave()
          setTimeout(() => {
            this.setState({ modalVisible: false })
            this.setState({
              startDate: '',
              endDate: '',
              leaveTypeOnLeaveModal: 'Leave Type',
              //totalDays: "" + 0,
              reasonTextInputData: '',
              startDateToShowOnMOdal: '',
              endDateToShowOnModal: '',
              leaveModalErrorText: ''


            })
          }, 300)
        } else {
          getLog("Leave  add response=====++++" + JSON.stringify(response))

        }

      })
  }

  saveButtonClicked() {

    if (this.state.leaveTypeOnLeaveModal == "Leave Type") {
      this.setState({ leaveModalErrorText: "*Please select leave type." })
      return
    }
    if (this.state.startDate == "") {
      this.setState({ leaveModalErrorText: "*Please select start date." })
      return
    }
    if (this.state.endDate == "") {
      this.setState({ leaveModalErrorText: "*Please select end date." })
      return
    }
    this.hitAddLeaveAPI()
    // var arr = this.state.leaveList
    // arr.push(
    //   {
    //     "validityFrom": this.state.startDate,
    //     "validityTo": this.state.endDate,
    //     'type': this.state.leaveTypeOnLeaveModal,
    //     'status': "Pending",
    //     'totalDays': this.state.totalDays,
    //     "reason": this.state.reasonTextInputData
    //   }
    // )
    // this.setState({ leaveList: arr })
    // this.totalNumberOfDaysLeave()
    // setTimeout(() => {
    //   this.setState({ modalVisible: false })
    //   this.setState({
    //     startDate: '',
    //     endDate: '',
    //     leaveTypeOnLeaveModal: 'Leave Type',
    //     totalDays: "" + 0,
    //     reasonTextInputData: '',
    //     startDateToShowOnMOdal: '',
    //     endDateToShowOnModal: '',
    //     leaveModalErrorText: ''


    //   })
    // }, 300)

    //alert(this.state.reasonTextInputData)
  }
  modalClose() {
    if (keyboardOpen) {
      Keyboard.dismiss()
    } else {
      this.setState({ modalVisible: false })
    }

  }
  onTimerStart() {
    if (this.state.selectProject != "Select Project") {
      if (this.state.play) {
        global.flag = true
        global.selectedProject = this.state.selectProject
        global.play = false
        clearInterval(interval);
        this.setState({ play: false, playPauseImage: require('../images/timesheetIcon/resumeIcon.png'), playPauseText: 'Play' })
      } else {
        global.flag = true
        global.selectedProject = this.state.selectProject
        global.play = false
        this.setState({ play: true, playPauseImage: require('../images/timesheetIcon/pauseIcon.png'), playPauseText: 'Pause', stopImage: require('../images/timesheetIcon/redStopIcon.png'), })
        interval = setInterval(() => {
          //seconds++
          getLog(Math.floor(this.state.second / 60))
          if (this.state.minute % 5 == 0) {
            if (this.state.second == 0) {
              this.setState({ second: this.state.second + 1 })
            } else {
              this.setState({ second: this.state.second + 2 })
            }

          }
          else if (this.state.minute % 15 == 0) {
            this.setState({ second: this.state.second + 2 })
          }
          else if (this.state.minute % 20 == 0) {
            this.setState({ second: this.state.second + 2 })
          }
          else if (this.state.minute % 25 == 0) {
            this.setState({ second: this.state.second + 2 })
          } else {
            this.setState({ second: this.state.second + 1 })
          }


          if ((this.state.second / 60) > 59) {
            var min = this.state.second / 60
            while (min >= 60) {
              min = min - 60
            }
            this.setState({ minute: min })
          } else {
            this.setState({ minute: (this.state.second / 60) })
          }
        }, 1000)
      }
    } else {
      alert("Please select project from dropdown.")
    }

  }

  onTimerStop() {
    if (this.state.stopImage != require('../images/timesheetIcon/greyStopIcon.png')) {
      clearInterval(interval);
      global.flag = false;
      global.selectedProject = '';
      global.play = false
      var hours = Math.round(this.state.second / (60 * 60));

      var divisor_for_minutes = this.state.second % (60 * 60);
      var minutes = Math.round(divisor_for_minutes / 60);

      //calculate start and end time to pass on timesheet page
      //here end time is when we stop the timer and start time is calculated when we subtract the endtime in seconds to total seconds
      var endTime = new Date().getTime()
      var startTime = new Date().getTime() - (this.state.second * 1000)
      //startTime=new Date(startTime*1000).getTime()
      //alert(new Date(endTime) + "---------"+new Date(startTime))
      this.props.navigation.navigate("CreateTimesheet", {
        data: {
          projectName: this.state.selectProject,
          projectId: this.state.selectedProjectId,
          timerStop: true,
          hours: hours * 60 + minutes == 0 ? 1 : hours * 60 + minutes,
          endTime: endTime,
          startTime: startTime,
          date: new Date(),
          returnCallBack: this.state.returnCallBack,
        }
      })
      this.setState({
        second: 0, playPauseText: 'Play', minute: 0,
        playPauseImage: require('../images/timesheetIcon/playIcon.png'),
        stopImage: require('../images/timesheetIcon/greyStopIcon.png'),
        selectProject: "Select Project",
        play: false
      })
    }

  }
  onProjectSelectDropdown() {
    this.setState({ pickerDropdownModalVisible: true, PickerDropdownModalType: "Select Project", pickerDropdownModalListData: this.state.projectListPassedToSelectProjectDropdown })
  }

  onWeekSelectDropdown() {
    this.setState({ pickerDropdownModalVisible: true, PickerDropdownModalType: "Week", pickerDropdownModalListData: ["Week", "Month", "Year"] })
  }
  onLeaveModalselectLeaveTypeClicked(index, value) {

    this.setState({ leaveTypeOnLeaveModal: value, leaveTypeId: this.state.leaveTypeList[index].leaveTypeId })
  }
  daysTextInputChangeOnLeaveModal(text) {
    this.setState({ totalDays: text })
  }

  render() {
    return (
      <SafeAreaView style={styles.safeArea}>
        <View style={styles.container}>
          <Spinner visible={this.state.spinnerVisible} />
          <Header HeaderLeftText={this.state.week}
            HeaderRightText="Analysis"
            toggleDrawer={() => this.props.navigation.openDrawer()}
            headerLeftIcon={require('../images/headerIcons/downArrow.png')}
            headerRightIcon={require('../images/headerIcons/analysis.png')}
            headerLeftIconOnPress={() => this.onWeekSelectDropdown()} />
          <FloatingButton onButtonPressed={() => this.onFloatingButtonPressed()} icon={require('../images/floatPlusIcon.png')} />


          <LeaveModal
            modalVisible={this.state.modalVisible}
            datePickerVisible={this.state.datePickerVisible}
            datePickerVisibleFunction={() => this.setState({ datePickerVisible: true })}
            startDatePickerVisibleFunction={() => this.startDateVisibleFunction()}
            endDatePickerVisibleFunction={() => this.endDateVisibleFunction()}
            dateSelected={(date) => this.getDateFromModal(date)}
            startDate={this.state.startDateToShowOnMOdal}
            endDate={this.state.endDateToShowOnModal}
            daysTextInputData={this.state.totalDays}
            daysTextInputChange={(text) => this.daysTextInputChangeOnLeaveModal(text)}
            datePickerCloseFunction={() => this.setState({ datePickerVisible: false })}
            minDate={this.state.minDate == '' ? new Date('01/01/1990') : this.state.minDate}
            saveButtonClicked={() => this.saveButtonClicked()}
            reasonTextInputDataChange={(text) => this.setState({ reasonTextInputData: text })}
            reasonTextInputData={this.state.reasonTextInputData}
            modalClose={() => this.modalClose()}
            TitleText="Add Leave" TypePlaceHolder={this.state.leaveTypeOnLeaveModal}
            leavesOptionArrayOnLeaveModal={this.state.leaveTypeListPassToModal}
            selectLeaveTypeClicked={(index, value) => this.onLeaveModalselectLeaveTypeClicked(index, value)}
            errorText={this.state.leaveModalErrorText}
          />

          <PickerDropdownModal
            pickerDropdownModalVisible={this.state.pickerDropdownModalVisible}
            pickerDropdownModalClose={() => this.setState({ pickerDropdownModalVisible: false })}
            titleText="Select Type"
            //titleText={this.state.PickerDropdownModalType == "Week" ? this.state.week : this.state.PickerDropdownModalType}
            pickerDropdownModalListData={this.state.pickerDropdownModalListData}
            pickerDropdownModalRow={({ item, index }) =>
              <TouchableWithoutFeedback key={"pickerDropdown" + index} onPress={() => this.pickerDropdownModalRowSelected(item)}>
                <View style={{ padding: 15, paddingLeft: 25, alignItems: "flex-start", justifyContent: "flex-start", borderBottomColor: "#E5E5E5", borderBottomWidth: 1 }}>
                  <Text style={{ fontSize: fontSizes.font18, color: "#333333" }}>
                    {item.projectName ? item.projectName : item.type ? item.type : item}
                  </Text>
                </View>
              </TouchableWithoutFeedback>
            }
          />




          <View style={styles.mainContainer}>
            {/* {this.timesheet_LeavesTab()} */}
            <TimeLeaveTab onTabPress={() => this.onTabPress()} tabPress={this.state.TabPress} />
            {this.state.weekArray ? this.weekTabs() : null}
            <ScrollView key="scrollview" keyboardShouldPersistTaps="always">
              <View style={styles.projectListContainer}>
                {this.state.TabPress ?
                  // <CardView
                  //   style={{ backgroundColor: 'transparent' }}
                  //   cardElevation={2}

                  //   cardMaxElevation={2}
                  //   cornerRadius={2}>
                  <View style={styles.TimerContainer}>
                    <View style={styles.rowView}>
                      <Text style={[styles.ProjectHeaderText, { flex: 1, textAlign: "left" }]}>
                        Quick Log
                                    </Text>
                      <TouchableOpacity onPress={() => this.onProjectSelectDropdown()}
                        style={{ flex: 1, borderBottomColor: "#D8D8D8", borderBottomWidth: 1, flexDirection: "row", justifyContent: "space-between" }}>
                        <Text style={[styles.ProjectHeaderText, { color: 'black' }]}>
                          {this.state.selectProject}
                        </Text>
                        <Image style={{ marginTop: 4 }} source={require('../images/downArrow.png')} />
                      </TouchableOpacity>
                    </View>
                    <View style={[styles.rowView, { paddingTop: 10, marginTop: 10 }]}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <TouchableOpacity onPress={() => this.onTimerStart()}
                          style={{ flex: 1, alignItems: "center" }}>
                          <Image source={this.state.playPauseImage} style={{ marginTop: this.state.playPauseText == "Pause" ? 3 : this.state.playPauseText == "Play" ? 1 : null }} />
                          <Text style={[styles.ProjectHeaderText, { color: 'black', marginTop: 5 }]}>
                            {this.state.playPauseText}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.onTimerStop()}
                          style={{ flex: 1, alignItems: "center", marginTop: 2 }}>
                          <Image source={this.state.stopImage} />
                          <Text style={[styles.ProjectHeaderText, { color: 'black', marginTop: 5 }]}>
                            Stop
                                            </Text>
                        </TouchableOpacity>
                      </View>


                      <View style={{ flex: 1, alignItems: "flex-end", justifyContent: "center" }}>
                        <Text style={[styles.ProjectHeaderText, { color: 'black', fontSize: 32 }]}>
                          {/* {(Math.floor(this.state.second / 3600) > 9 ? Math.floor(this.state.second / 3600) : "0" + Math.floor(this.state.second / 3600)) + ":" +
                            (Math.floor(this.state.second / 60) > 9 ? Math.floor(this.state.second / 60) : "0" + Math.floor(this.state.second / 60)) +
                            ':' +
                            ('0' + this.state.second % 60).slice(-2)} */}
                          {(Math.floor(this.state.second / 3600) > 9 ? Math.floor(this.state.second / 3600) : "0" + Math.floor(this.state.second / 3600)) + ":" +
                            (Math.floor(this.state.minute) > 9 ? Math.floor(this.state.minute) : "0" + Math.floor(this.state.minute)) +
                            ':' +
                            ('0' + this.state.second % 60).slice(-2)}
                          {/* {(Math.floor(this.state.second / 3600) > 9 ? Math.floor(this.state.second / 3600) : "0" + Math.floor(this.state.second / 3600)) + ":" +
                            (('0' + this.state.second % 3600).slice(-2) > 9 ? ('0' + this.state.second % 3600).slice(-2) :  ('0' + this.state.second % 3600).slice(-2)) +
                            ':' +
                            ('0' + this.state.second % 60).slice(-2)} */}
                        </Text>
                      </View>
                    </View>
                  </View>
                  // </CardView>
                  : null}
                <View style={styles.headerView}>
                  <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                    <Text style={styles.ProjectHeaderText}>
                      {this.state.TabPress ? "Projects" : "On"}
                    </Text>
                    <Text style={styles.ProjectHeaderTime}>

                      {this.state.TabPress ? hourMinuteConverter(this.state.totalProjectHours) : this.state.totalLeaveDays > 1 ? this.state.totalLeaveDays + " Days" : this.state.totalLeaveDays + " Day"}

                    </Text>
                  </View>
                </View>
                {this.state.dataLoading ?
                  <Text style={styles.ProjectHeaderText}>
                    Loading
                    </Text>
                  :
                  <FlatList
                    data={this.state.TabPress ? this.state.projectList : this.state.leaveList}
                    renderItem={({ item, index }) => this.createList(item, index)}
                    keyExtractor={key => key.index}
                    extraData={this.state}
                  />
                }
              </View>
            </ScrollView>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#fff', justifyContent: "flex-start", },
  safeArea: {
    flex: 1,
    backgroundColor: '#21459E'
  },
  tabText: { fontSize: fontSizes.font12, color: "white", textAlign: 'center', },
  instructions: { textAlign: 'center', color: '#333333', marginBottom: 5, },
  mainContainer: { flex: 1, justifyContent: "center", alignItems: 'center', },
  tabView: { height: 40, width: width, flexDirection: 'row', backgroundColor: "#2A56C6" },
  weekTabView: { height: 35, width: width, flexDirection: 'row', backgroundColor: "#DADADA" },
  tabStyle: { flex: 1, alignItems: "center", justifyContent: "center" },
  projectListContainer: { margin: 10, width: width - 20, },
  ProjectHeaderText: { fontSize: fontSizes.font16, color: "#2A56C6", textAlign: 'center' },
  ProjectHeaderTime: { fontSize: fontSizes.font14, color: "#FF9800", textAlign: 'center' },
  headerView: { borderBottomWidth: .5, padding: 10, borderBottomColor: '#979797' },
  listView: { borderBottomWidth: .5, padding: 0, paddingTop: 5, paddingBottom: 5, borderBottomColor: '#BBBBBB' },
  rowView: { flexDirection: "row", justifyContent: "space-between", padding: 5 },
  projectColumnView: { flex: 1.5, justifyContent: "center", },
  //TimerContainer: { height: 100, width: width - 20, borderRadius: 6, borderWidth: .5, borderColor: "white", shadowOffset: { width: 0, height: 3, }, shadowColor: '#000', shadowOpacity: 0.8, shadowRadius: 5 }
  TimerContainer: { width: width - 20, borderRadius: 6, borderWidth: 0, paddingRight: 10, paddingBottom: 10, borderColor: "#E5E5E588", borderRadius: 5, padding: 5 }

});
